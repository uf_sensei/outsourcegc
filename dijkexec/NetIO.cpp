#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include "NetIO.h"
#include "Env.h"
#include <netinet/tcp.h>
#include <sys/time.h>

unsigned long getTime()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    unsigned long time_in_micros = 1000 * tv.tv_sec + tv.tv_usec/1000;
    
    return time_in_micros;
    
}

void addLatency(int ms)
{
    //time_t start, current;
    
    //time(&start);
    
    unsigned long start,current;
    
    start = getTime();
    
    while(1)
    {
        //time(&current);
        current = getTime();
        //printf("%lu\n",(current-start));
        
        if((current - start)>=ms)
            break;
        
    }
    //while ();
}

const int CHUNK_SIZE = 1024;

Socket::Socket() : m_socket(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))
{

	int one = 1; setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));

    struct timeval timeout;      
    timeout.tv_sec = 20000;
    timeout.tv_usec = 0;
int syncnt = 100;
int syncnt_sz = sizeof(syncnt);
setsockopt(m_socket, IPPROTO_TCP, TCP_SYNCNT, &syncnt, syncnt_sz);
	setsockopt (m_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout));
        //error("setsockopt failed\n");

   // setsockopt (m_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,sizeof(timeout));
        //error("setsockopt failed\n");

}

Socket::~Socket()
{
	shutdown(m_socket, SHUT_RDWR);
    close(m_socket);
}

void Socket::write_bytes(const Bytes &bytes)
{
	uint32_t sz = htonl(bytes.size());

	send(m_socket, &sz, sizeof(sz), 0);
	sz = bytes.size();
	/*if(sz > 1500)
	{
		std::cout <<"[sz: "<<sz<<" ]";
	}*/
	size_t ix = 0;
	for (; sz > CHUNK_SIZE; sz -= CHUNK_SIZE, ix += CHUNK_SIZE)
    {
//        addLatency(latency);
        send(m_socket, &bytes[ix], CHUNK_SIZE, 0);
    }
//    addLatency(latency);
	send(m_socket, &bytes[ix], bytes.size()-ix, 0);
}

inline void my_read(int socket, void *data, size_t n)
{
	size_t temp;
	for (size_t ix = 0; ix != n; )
	{
		temp = recv(socket, reinterpret_cast<char*>(data)+ix, n-ix, 0);
		ix +=temp;
		if(temp < 0)
		{
			return;
		} 
	}
}

Bytes Socket::read_bytes()
{
        uint32_t sz, ix;
        my_read(m_socket, &sz, 4);
        sz = ntohl(sz);

        Bytes bytes(sz);
        for (ix = 0; sz > CHUNK_SIZE; sz -= CHUNK_SIZE, ix += CHUNK_SIZE)
        	my_read(m_socket, &bytes[0]+ix, CHUNK_SIZE);

        my_read(m_socket, &bytes[0]+ix, bytes.size()-ix);

        return bytes;
}

void Socket::write_string(const std::string &str)
{
	byte *ptr = (byte *)(str.c_str());
	write_bytes(Bytes(ptr, ptr+str.size()));
}

std::string Socket::read_string()
{
	Bytes bytes = read_bytes();
	char *ptr = reinterpret_cast<char*>(&bytes[0]);
	return std::string(ptr, bytes.size());
}

ClientSocket::ClientSocket(const char *host_ip, size_t port, int delay)
{
	//std::cout << "jump...\n";
    
	struct timeval timeout;      
	timeout.tv_sec = 20000;
	timeout.tv_usec = 0;


	const int MAX_SLEEP = 160000;

	struct sockaddr_in addr;
	int res;

	if (-1 == m_socket)
	{
		perror("cannot create socket");
		exit(EXIT_FAILURE);
	}

	//std::cout << "memset...\n";
	memset(&addr, 0, sizeof(addr));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	res = inet_pton(AF_INET, host_ip, &addr.sin_addr);
	//std::cout << "ient_pton...\n";

	if (0 > res)
	{
		perror("error: first parameter is not a valid address family");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
	else if (0 == res)
	{
		perror("error: second parameter does not contain valid IP address");
		close(m_socket);
		exit(EXIT_FAILURE);
	}

	// exponential backoff algorithm
	for (int sec = 1; sec < MAX_SLEEP; sec++ )
	{
		int ret;
		//sleep(sec+5*Env::group_rank());
		//setsockopt (m_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout));
		//setsockopt (m_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,sizeof(timeout));
   
                //std::cout << "try connecting...\n";
		//sleep(delay);
		if (0 == (ret = connect(m_socket, (struct sockaddr *)&addr, sizeof(addr))))
		{
			timeout.tv_sec = 20000;
			
			setsockopt (m_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout));
			setsockopt (m_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,sizeof(timeout));
  

			return;
		}

		//std::cout <<"ret: "<<errno<<" "<<ETIMEDOUT<<" " <<ECONNREFUSED<<" "<<ENETUNREACH<<"\n";
		sleep(1);
		//std::cout <<"aftersleep\n";

		// state of m_socket is unspecified after failure. need to recreate
		

		
  		close(m_socket);
		m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		
	}

	perror("connect failed");
	close(m_socket);
	exit(EXIT_FAILURE);
}

ServerSocket::ServerSocket()
{
}

ServerSocket::ServerSocket(size_t port)
{
	struct sockaddr_in addr;

	if(-1 == m_socket)
	{
		perror("can not create socket");
		exit(EXIT_FAILURE);
	}

	memset(&addr, 0, sizeof addr);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;

	if(-1 == bind(m_socket,(struct sockaddr *)&addr, sizeof addr))
	{
		std::cerr<<"Port: "<<port<<std::endl;
		perror("error bind failed");
		close(m_socket);
		exit(EXIT_FAILURE);
	}

	if(-1 == listen(m_socket, 10))
	{
		perror("error listen failed");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
}

Socket *ServerSocket::accept()
{

	struct timeval timeout;      
	timeout.tv_sec = 20000;
	timeout.tv_usec = 0;


	int socket = ::accept(m_socket, NULL, NULL);

	if(socket < 0)
	{
		fprintf(stderr, "accept() failed: %s\n", strerror(errno));
		close(m_socket);
		exit(EXIT_FAILURE);
	}

 			
	m_sockets.push_back(socket);

	setsockopt (m_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout));
	setsockopt (m_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,sizeof(timeout));
  


	return new Socket(socket);
}

ServerSocket::~ServerSocket() {}
