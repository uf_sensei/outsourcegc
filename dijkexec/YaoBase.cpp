#include <iomanip>

#include <netdb.h>
#include <arpa/inet.h>
#include <mpi.h>
#include <cstring>

#include "YaoBase.h"
#include <limits.h>
//#include <log4cxx/logger.h>
//static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("YaoBase.cpp"));


inline std::string get_IP()
{
	// get local IP and display
	char hostname[1024];
	gethostname(hostname, 1024);
	struct hostent *host = gethostbyname(hostname);
	const std::string local_IP = inet_ntoa(*((struct in_addr *)host->h_addr_list[0]));
	return local_IP;
}


YaoBase::YaoBase(EnvParams &params)
{
	init_cluster(params);
#if defined EVL_CODE || defined GEN_CODE || defined TRD_CODE
	init_network(params); // no need in simulation mode
#endif
	MPI_Barrier(MPI_COMM_WORLD);	


	GEN_BEGIN
		std::cout <<"gen Succeed\n";	
	GEN_END	
	EVL_BEGIN
		std::cout <<"evl succeed\n";
	EVL_END

	gettimeofday(&startt, NULL);

	init_environ(params);
	init_private(params);

	// [WARNING] no access to params after this point. use Env instead.

	// display local IP
	/*EVL_BEGIN
		LOG4CXX_INFO(logger, "EVL (" << Env::group_rank() << ") is at " << get_IP());
	EVL_END

	GEN_BEGIN
		LOG4CXX_INFO(logger, "GEN (" << Env::group_rank() << ") is at " << get_IP());
	GEN_END

	if (!Env::is_root())
		return;

	LOG4CXX_INFO(logger, "========================================================");
	LOG4CXX_INFO(logger, "Starting Yao protocol");
	LOG4CXX_INFO(logger, "========================================================");*/
}

void YaoBase::init_cluster(EnvParams &params)
{
	// inquire world info
	MPI_Comm_rank(MPI_COMM_WORLD, &params.wrld_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &params.node_amnt);

#if defined GEN_CODE || defined EVL_CODE || defined TRD_CODE
	params.node_rank = params.wrld_rank;
	m_mpi_comm = MPI_COMM_WORLD;
#else
	if (params.node_amnt % 2 != 0)
	{
		LOG4CXX_FATAL(logger, "statistical parameter s needs to be an even number in the Simulation mode");
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}

	// divide the world into two groups of the same size (Simulation mode)
	MPI_Comm_split(MPI_COMM_WORLD, params.wrld_rank % 2, params.wrld_rank, &m_mpi_comm);

	// inquire info of the new group
	MPI_Comm_rank(m_mpi_comm, &params.node_rank);
	MPI_Comm_size(m_mpi_comm, &params.node_amnt);
#endif
}


void YaoBase::init_network(EnvParams &params)
{
#if !(defined GEN_CODE || defined EVL_CODE || defined TRD_CODE) // just in case (shouldn't reach this point to begin with)
	return;
#endif

	const int PORT = params.port_base + params.node_rank;
	Bytes send, recv;

	// get local IP
	char hostname[1024];
	gethostname(hostname, 1024);
	struct hostent *host = gethostbyname(hostname);
	const std::string local_ip = inet_ntoa(*((struct in_addr *)host->h_addr_list[0]));

	//std::cout << local_ip<< "\n";

	EVL_BEGIN
        params.serverEval = new ServerSocket(params.thirdport_base+1+params.node_rank);
    
		// collect IPs from slaves and send them the evaluator via IP server
		send.resize(sizeof(struct in_addr));
		memcpy(&send[0], host->h_addr_list[0], send.size());

		//recv.resize(sizeof(struct in_addr)*params.node_amnt); // only used by node 0
		//MPI_Gather(&send[0], send.size(), MPI_BYTE, &recv[0], send.size(), MPI_BYTE, 0, m_mpi_comm);
       
		params.server = new ServerSocket(PORT);
        
		/*if (params.node_rank == 0)
		{
			ServerSocket ip_exchanger(Env::IP_SERVER_PORT);
			Socket *sock = ip_exchanger.accept();
			sock->write_bytes(recv); // send slaves' IPs to remote
		}*/

		//LOG4CXX_INFO(logger, "EVL (" << params.node_rank << ":" << local_ip << ") is listening at port " << PORT);
		
		params.remote = params.server->accept();
        	params.remote->latency = params.latencyserver;
		//LOG4CXX_INFO(logger, "EVL (" << params.node_rank << ":" << local_ip << ") is connected at port " << PORT);


		/*std::string remote_ip = inet_ntoa(*((struct in_addr *)params.thirdipaddr));
		*/
		//std::cout << "EVL (" << params.node_rank << ":" << "" << ") is connecting (" <<  local_ip << ") at port " << (params.thirdport_base)<<"\n";
		/*if (params.node_rank == 0)
		{
			ClientSocket ip_exchanger(params.thirdipaddr,params.thirdport_base, 1);
			send = ip_exchanger.read_bytes(); // receive the generator's slaves' IPs
		}

		recv.resize(sizeof(struct in_addr));
		MPI_Scatter(&send[0], recv.size(), MPI_BYTE, &recv[0], recv.size(), MPI_BYTE, 0, m_mpi_comm);

		 remote_ip = inet_ntoa(*((struct in_addr *)&recv[0]));*/
		//LOG4CXX_INFO(logger, "EVL (" << params.node_rank << ":" << local_ip << ") is connecting (" <<  remote_ip << ") at port " << (params.thirdport_base+1+params.node_rank));

		//std::cout <<"warning, single IP for third used, scatter disregarded\n";
    
		//params.remoteToThird = new ClientSocket(params.thirdipaddr, params.thirdport_base+1+params.node_rank,1);
    
        
        /*if (params.node_rank == 0)
         {
         ServerSocket ip_exchanger(Env::IP_SERVER_PORT);
         Socket *sock = ip_exchanger.accept();
         sock->write_bytes(recv); // send slaves' IPs to remote
         }*/
        
        //LOG4CXX_INFO(logger, "EVL (" << params.node_rank << ":" << local_ip << ") is listening at port " << PORT);
        
        params.remoteToThird = params.serverEval->accept();
        params.remoteToThird->latency = params.latencythird;
		//std::cout <<"EVL (" << params.node_rank << ":" << local_ip << ") succeeded connecting"<<"\n";
	EVL_END

	GEN_BEGIN
        params.serverGen = new ServerSocket(params.thirdport_base+1+params.node_rank);
       
		// receive IPs from the generator via IP server and forward them to slaves
		/*if (params.node_rank == 0)
		{
			ClientSocket ip_exchanger(params.ipserve_addr, Env::IP_SERVER_PORT,60);
			send = ip_exchanger.read_bytes(); // receive the generator's slaves' IPs
		}*/

		//recv.resize(sizeof(struct in_addr));
		//MPI_Scatter(&send[0], recv.size(), MPI_BYTE, &recv[0], recv.size(), MPI_BYTE, 0, m_mpi_comm);

		//std::string remote_ip = inet_ntoa(*((struct in_addr *)&recv[0]));
		//std::cout << "GEN (" << params.node_rank << ":" << local_ip << ") is connecting (" <<  "" << ") at port " << PORT<<"\n";
		params.remote = new ClientSocket(params.ipserve_addr, PORT,60);
        	params.remote->latency = params.latencyserver;
		//std::cout << "GEN (" << params.node_rank << ":" << local_ip << ") succeeded connecting\n";
		
		//remote_ip = inet_ntoa(*((struct in_addr *)params.thirdipaddr));
		//LOG4CXX_INFO(logger, "GEN (" << params.node_rank << ":" << local_ip << ") is connecting (" <<  remote_ip << ") at port " << (params.thirdport_base));
		/*if (params.node_rank == 0)
		{
			ClientSocket ip_exchanger(params.thirdipaddr,params.thirdport_base,1);
			send = ip_exchanger.read_bytes(); // receive the generator's slaves' IPs
		}

		recv.resize(sizeof(struct in_addr));
		MPI_Scatter(&send[0], recv.size(), MPI_BYTE, &recv[0], recv.size(), MPI_BYTE, 0, m_mpi_comm);

		remote_ip = inet_ntoa(*((struct in_addr *)&recv[0]));*/
		//std::cout << "GEN (" << params.node_rank << ":" << local_ip << ") is connecting (" <<  "t" << ") at port " << (params.thirdport_base+1+params.node_rank)<<"\n";
		//std::cout <<"warning, single IP for third used, scatter disregarded\n";
		//params.remoteToThird = new ClientSocket(params.thirdipaddr, params.thirdport_base+1+params.node_rank, 1);
    
        
        /*if (params.node_rank == 0)
         {
         ServerSocket ip_exchanger(Env::IP_SERVER_PORT);
         Socket *sock = ip_exchanger.accept();
         sock->write_bytes(recv); // send slaves' IPs to remote
         }*/
        
        //LOG4CXX_INFO(logger, "EVL (" << params.node_rank << ":" << local_ip << ") is listening at port " << PORT);
        
        params.remoteToThird = params.serverGen->accept();
        params.remoteToThird->latency = params.latencythird;
    
    
		//std::cout <<  "GEN (" << params.node_rank << ":" << local_ip << ") succeeded connecting\n";
	GEN_END

	THIRD_BEGIN
		//gather ips 
		//send to both eval and gen
		//receive both eval and gen		
				
		send.resize(sizeof(struct in_addr));
		memcpy(&send[0], host->h_addr_list[0], send.size());

		recv.resize(sizeof(struct in_addr)*params.node_amnt); // only used by node 0
		MPI_Gather(&send[0], send.size(), MPI_BYTE, &recv[0], send.size(), MPI_BYTE, 0, m_mpi_comm);

		//to eval
		/*if (params.node_rank == 0)
		{
			ServerSocket ip_exchanger(params.port_base);
			Socket *sock = ip_exchanger.accept();
			sock->write_bytes(recv); // send slaves' IPs to remote
		}*/
		std::cout << "THIRD (" << params.node_rank << ":" << local_ip << ") is listening at port " << params.port_base+1+params.node_rank;
		params.serverEval = new ServerSocket(params.port_base+1+params.node_rank);
		params.remoteFromEval = params.serverEval->accept();
		std::cout << "THIRD (" << params.node_rank << ":" << local_ip << ") is connected at port " << params.port_base+1+params.node_rank;


		//to gen
		/*if (params.node_rank == 0)
		{
			ServerSocket ip_exchanger(params.thirdport_base);
			Socket *sock = ip_exchanger.accept();
			sock->write_bytes(recv); // send slaves' IPs to remote
		}*/
		std::cout <<  "THIRD (" << params.node_rank << ":" << local_ip << ") is listening at port " << params.thirdport_base+1+params.node_rank;
		params.serverGen = new ServerSocket(params.thirdport_base+1+params.node_rank);
		params.remoteFromGen = params.serverGen->accept();
		std::cout <<  "THIRD (" << params.node_rank << ":" << local_ip << ") is connected at port " << params.thirdport_base+1+params.node_rank;

	

	THIRD_END

	//std::cout <<" END OF NET INIT\n";
}


void YaoBase::init_environ(EnvParams &params)
{
	//std::cout << "here\n";

	if (params.secu_param % 8 != 0 || params.secu_param > 128)
	{
		std::cout << "security parameter k needs to be a multiple of 8 less than or equal to 128";
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}

	if (params.stat_param % params.node_amnt != 0)
	{
		std::cout <<  "statistical  parameter s needs to be a multiple of cluster size";
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}

	// # of copies of a circuit each node is responsible for
	params.node_load = params.stat_param/params.node_amnt;

	if (!params.circuit.load_binary(params.circuit_file))
	{
		std::cout << "circuit parsing failed";
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}

	Env::init(params);

	// synchronize claw-free collections
	ClawFree claw_free;
	claw_free.init();
	Bytes bufr(claw_free.size_in_bytes());

	if (Env::is_root())
	{
		EVL_BEGIN
			bufr = claw_free.to_bytes();
			EVL_SEND(bufr);
			TO_THIRD_SEND(bufr);
		EVL_END

		GEN_BEGIN
			bufr = GEN_RECV();
		GEN_END
	}

	// synchronize claw-free collections to the root evaluator's
	MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	Env::claw_free_from_bytes(bufr);
}


void YaoBase::init_private(EnvParams &params)
{
	static byte MASK[8] = { 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F};

	std::ifstream private_file(params.private_file);
	std::string input;

	if (!private_file.is_open())
	{
		std::cout << "file open failed: " << params.private_file;
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}

	EVL_BEGIN // evaluator
		private_file >> input;          // 1st line is the evaluator's input
		//std::cout << input << endl;
		m_evl_inp.from_hex(input);
		m_evl_inp.resize((Env::circuit().evl_inp_cnt()+7)/8);
		m_evl_inp.back() &= MASK[Env::circuit().evl_inp_cnt()%8];
	EVL_END

	THIRD_BEGIN // evaluator
		private_file >> input;          // 1st line is the evaluator's input
		//std::cout << input << endl;
		m_evl_inp.from_hex(input);
		m_evl_inp.resize((Env::circuit().evl_inp_cnt()+7)/8);
		m_evl_inp.back() &= MASK[Env::circuit().evl_inp_cnt()%8];

		//std::cout <<"inputs: " << Env::circuit().evl_inp_cnt() << "\n";
	THIRD_END

	GEN_BEGIN // generator
		private_file >> input >> input; // 2nd line is the generator's input
		m_gen_inp.from_hex(input);
		m_gen_inp.resize((Env::circuit().gen_inp_cnt()+7)/8);
		m_gen_inp.back() &= MASK[Env::circuit().gen_inp_cnt()%8];
	GEN_END

	private_file.close();

	// Init variables
//	m_coms.resize(Env::node_load());
//	m_rnds.resize(Env::node_load());
//	m_ccts.resize(Env::node_load());
//	m_gen_inp_masks.resize(Env::node_load());
}


YaoBase::~YaoBase()
{
	Env::destroy();

	int res;
	MPI_Comm_compare(m_mpi_comm, MPI_COMM_WORLD, &res);
	if (res == MPI_UNEQUAL) MPI_Comm_free(&m_mpi_comm); // MPI_COMM_WORLD can't be freed
}


Bytes YaoBase::recv_data(int src_node)
{
	MPI_Status status;

	uint32_t comm_sz;
	MPI_Recv(&comm_sz, 1, MPI_INT, src_node, 0, MPI_COMM_WORLD, &status);

	Bytes recv(comm_sz);
	MPI_Recv(&recv[0], recv.size(), MPI_BYTE, src_node, 0, MPI_COMM_WORLD, &status);

	return recv;
}


void YaoBase::send_data(int dst_node, const Bytes &data)
{
	assert(data.size() < INT_MAX);

	uint32_t comm_sz = data.size();
	MPI_Send(&comm_sz, 1, MPI_INT, dst_node, 0, MPI_COMM_WORLD);

	MPI_Send(const_cast<byte*>(&data[0]), data.size(), MPI_BYTE, dst_node, 0, MPI_COMM_WORLD);
}



inline std::string print_longlong(uint64_t l)
{
	char buf[8];
	std::string str;

	while (l >= 1000)
	{
		sprintf(buf, ",%03d", (int)(l%1000));
		str = buf + str;

		l = l / 1000LL;
	}

	sprintf(buf, "%d", (int)l);
	str = buf + str;

	return str;
}


void YaoBase::step_init()
{
    m_timer_gen = m_timer_evl = m_timer_mpi = m_timer_com = 0;
}


void YaoBase::step_report(uint64_t comm_sz, std::string step_name)
{
	uint64_t all_comm_sz = 0;
	double max_timer_mpi = 0;

	double start = MPI_Wtime();
		MPI_Reduce(&comm_sz, &all_comm_sz, 1, MPI_LONG_LONG_INT, MPI_SUM, 0, m_mpi_comm);
	m_timer_mpi += MPI_Wtime() - start;

	if (!Env::is_root())
		return;

	m_timer_mpi_vec.push_back(max_timer_mpi);
	m_timer_com_vec.push_back(m_timer_com);
	m_step_name_vec.push_back(step_name);
	m_comm_sz_vec.push_back(all_comm_sz);

	EVL_BEGIN
		m_timer_evl_vec.push_back(m_timer_evl);
		//LOG4CXX_INFO(logger, "EVL \033[33mfinish " << step_name << "\033[37m");
	EVL_END

	GEN_BEGIN
		m_timer_gen_vec.push_back(m_timer_gen);
		//LOG4CXX_INFO(logger, "GEN \033[33mfinish " << step_name << "\033[37m");
	GEN_END
}


void YaoBase::final_report()
{
	if (!Env::is_root())
		return;

	/*LOG4CXX_INFO(logger, "========================================================");
	LOG4CXX_INFO(logger, "Yao protocol completed");
	LOG4CXX_INFO(logger, "========================================================");*/

	std::string name;
	std::vector<double> comp_vec;

	/*EVL_BEGIN
		name = "EVL";
		comp_vec = m_timer_evl_vec;

		LOG4CXX_INFO(logger, "EVL  input: \033[31m " << m_evl_inp.to_hex() << " \033[37m");
		LOG4CXX_INFO(logger, "EVL output: \033[31m " << m_evl_out.to_hex() << " \033[37m");
	EVL_END*/

	/*GEN_BEGIN
		name = "GEN";
		comp_vec = m_timer_gen_vec;

		LOG4CXX_INFO(logger, "GEN  input: \033[31m " << m_gen_inp.to_hex() << " \033[37m");
		LOG4CXX_INFO(logger, "GEN output: \033[31m " << m_gen_out.to_hex() << " \033[37m");
	GEN_END*/

	/*for (size_t i = 0; i < m_comm_sz_vec.size(); i++)
	{
		LOG4CXX_INFO
		(
			logger,
			name << " in " << m_step_name_vec[i] << "> " << std::fixed <<
			"  comp:" << std::setprecision(3) << std::setw(10) << comp_vec[i] <<
			", comm:" << std::setprecision(3) << std::setw(10) << m_timer_com_vec[i] <<
			",  mpi:" << std::setprecision(3) << std::setw(10) << m_timer_mpi_vec[i] <<
			", size:" << std::setprecision(3) << std::setw(18) << print_longlong(m_comm_sz_vec[i])
		);
	}*/

	gettimeofday(&endt, NULL);

	seconds  = endt.tv_sec  - startt.tv_sec;
	useconds = endt.tv_usec - startt.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

	if(Env::is_root())
		printf("%ld\n", mtime);
}


