LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)
LOCAL_MODULE := libz
LOCAL_SRC_FILES := libz.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ssl
LOCAL_SRC_FILES := libcrypto.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := gmp
LOCAL_SRC_FILES := libgmp.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := pbc
LOCAL_SRC_FILES := libpbc.so
LOCAL_SHARED_LIBRARIES :=  libz gmp 
include $(PREBUILT_SHARED_LIBRARY)


include $(CLEAR_VARS)

LOCAL_MODULE    := main
LOCAL_SRC_FILES := main_third.cpp     Bytes.cpp  NetIO.cpp  Circuit.cpp  Prng.cpp   Env.cpp   Algebra.cpp  ClawFree.h
LOCAL_EXPORT_C_INCLUDES = sha.h aes.h pbc.h gmp.h
LOCAL_SHARED_LIBRARIES :=  ssl libz gmp pbc 
LOCAL_LDLIBS  := -L$(SYSROOT)/usr/lib -llog

include $(BUILD_SHARED_LIBRARY)
#include $(BUILD_EXECUTABLE)

