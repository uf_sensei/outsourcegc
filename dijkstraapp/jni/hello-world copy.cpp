#include <stdio.h>

#include <stdint.h>
#include <algorithm>
#include <cassert>
#include <string>
#include <vector>

#include <iostream>
//#include <openssl/sha.h>


#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
//#include <iostream>




typedef uint8_t byte;


class Bytes : public std::vector<byte>
{
	struct bitwise_xor{ byte operator() (byte l, byte r) const { return l ^ r;  }};

public:
	Bytes() {}
	Bytes(uint64_t n) : std::vector<byte>(n) {}
	Bytes(uint64_t n, byte b) : std::vector<byte>(n, b) {}
	Bytes(byte *begin, byte *end) : std::vector<byte>(begin, end) {}
	Bytes(const_iterator begin, const_iterator end) : std::vector<byte>(begin, end) {}
	Bytes(const std::vector<Bytes> &chunks) { merge(chunks); }

	const Bytes &operator =(const Bytes &rhs)
	{
		this->assign(rhs.begin(), rhs.end());
		return *this;
	}

	const Bytes &operator +=(const Bytes &rhs)
	{
		this->insert(this->end(), rhs.begin(), rhs.end());
		return *this;
	}

	const Bytes &operator ^=(const Bytes &rhs)
	{
		// TODO: see if this can be improved by a wider data type pointer
		assert(rhs.size() == size());
		Bytes::iterator dst = this->begin();
		Bytes::const_iterator src = rhs.begin();
		while (dst != this->end()) { *dst++ ^= *src++;}
		return *this;
	}

	byte get_ith_bit(size_t ix) const
	{
		//std::cout << ix<<"\n";
		assert(ix < size()*8);
		return ((*this)[ix/8] >> (ix%8)) & 0x01;
	}

	/*byte get_ith_bit2(size_t ix, Bytes b) const
	{
		std::cout << b.to_hex()<<" " << ix<<"\n";
		while(!(ix < size()*8))
			std::cout <<"found! "<< b.to_hex()<<" " << ix<<"\n";
		assert(ix < size()*8);
		return ((*this)[ix/8] >> (ix%8)) & 0x01;
	}*/

	void set_ith_bit(size_t ix, byte bit)
	{
		assert(ix < size()*8);
		static const byte INVERSE_MASK[8] =
			{ 0xFE, 0xFD, 0xFB, 0xF7, 0xEF, 0xDF, 0xBF, 0x7F };

		(*this)[ix/8] &= INVERSE_MASK[ix%8];
		(*this)[ix/8] |= (bit&0x01) << (ix%8);
	}

	std::string to_hex() const;
	void from_hex(const std::string &s);

	Bytes hash(size_t bits) const;
	std::vector<Bytes> split(const size_t chunk_len) const;
	void merge(const std::vector<Bytes> &chunks);
};

// pre-condition: lhs.size() == rhs.size()
inline Bytes operator^ (const Bytes &lhs, const Bytes &rhs)
{
	assert(lhs.size() == rhs.size());
	Bytes ret(lhs);
	ret ^= rhs;
	return ret;
}

inline Bytes operator+ (const Bytes &lhs, const Bytes &rhs)
{
	Bytes ret(lhs);
	ret += rhs;
	return ret;
}

inline bool operator ==(const Bytes &lhs, const Bytes &rhs)
{
	return (lhs.size() != rhs.size())?
		false : std::equal(lhs.begin(), lhs.end(), rhs.begin());
}


namespace
{
byte MASK[8] =
{
	0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F
};
}

/*Bytes Bytes::hash(size_t bits) const
{
	assert((0 < bits) && (bits <= 256));

	byte buf[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, &((*this)[0]), this->size());
	SHA256_Final(buf, &sha256);

	Bytes hash(buf, buf + (bits+7)/8);
	hash.back() &= MASK[bits % 8]; // clear the extra bits

	return hash;
}*/

namespace
{
static const char HEX_TABLE[16] =
{
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};
}

std::string Bytes::to_hex() const
{
	std::string out;
	out.reserve(this->size()*2);

	for (const_iterator it = this->begin(); it != this->end(); it++)
	{
		out.push_back(HEX_TABLE[*it/16]);
		out.push_back(HEX_TABLE[*it%16]);
	}
	return out;
}

namespace
{
static const int REVERSE_HEX_TABLE[256] =
{
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
		-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
};

static const int HEX_EXP[2] = { 16, 1 };
}

void Bytes::from_hex(const std::string &s)
{
	//std::cout << "Begin change"<<std::endl;;
	this->clear();
	this->resize((s.size()+1)/2, 0); // zero out this

	for (size_t ix = 0; ix < s.size(); ix++)
	{
		if (REVERSE_HEX_TABLE[s[ix]] == -1)
		{
            std::cerr << "Invalid hex format: " << s;
		}
		//std::cout << s[ix]<<" " << REVERSE_HEX_TABLE[s[ix]] << " "<< REVERSE_HEX_TABLE[s[ix]]*HEX_EXP[ix%2] << std::endl;
		(*this)[ix/2] += REVERSE_HEX_TABLE[s[ix]]*HEX_EXP[ix%2];
	}
	//std::cout << "End change"<<std::endl;
}

std::vector<Bytes> Bytes::split(const size_t chunk_len) const
{
	assert(this->size() % chunk_len == 0);
	std::vector<Bytes> ret(this->size()/chunk_len);

	const_iterator it = this->begin();
	for (int ix = 0; ix < ret.size(); ix++, it+= chunk_len)
		ret[ix].insert(ret[ix].end(), it, it+chunk_len);

	return ret;
}

void Bytes::merge(const std::vector<Bytes> &chunks)
{
	this->clear();
	this->reserve(chunks.size()*chunks[0].size());

	std::vector<Bytes>::const_iterator it;
	for (it = chunks.begin(); it != chunks.end(); it++) *this += *it;
}

class Socket
{
protected:
	int m_socket;

public:
	Socket();
	Socket(int socket) : m_socket(socket) {}
	virtual ~Socket();

	void write_bytes(const Bytes &bytes);
	Bytes read_bytes();

	void write_string(const std::string &str);
	std::string read_string();
};

class ClientSocket : public Socket
{
public:
	ClientSocket(const char *host_ip, size_t port);
	virtual ~ClientSocket() {}
};

class ServerSocket : public Socket
{
	std::vector<int> m_sockets;

public:
	ServerSocket();
	ServerSocket(size_t port);
	Socket *accept();
	virtual ~ServerSocket();
};



const int CHUNK_SIZE = 1024;

Socket::Socket() : m_socket(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))
{}

Socket::~Socket()
{
	shutdown(m_socket, SHUT_RDWR);
    close(m_socket);
}

void Socket::write_bytes(const Bytes &bytes)
{
	uint32_t sz = htonl(bytes.size());

	send(m_socket, &sz, sizeof(sz), 0);
	sz = bytes.size();

	size_t ix = 0;
	for (; sz > CHUNK_SIZE; sz -= CHUNK_SIZE, ix += CHUNK_SIZE)
			send(m_socket, &bytes[ix], CHUNK_SIZE, 0);
	send(m_socket, &bytes[ix], bytes.size()-ix, 0);
}

inline void my_read(int socket, void *data, size_t n)
{
	for (size_t ix = 0; ix != n; )
		ix += recv(socket, reinterpret_cast<char*>(data)+ix, n-ix, 0);
}

Bytes Socket::read_bytes()
{
        uint32_t sz, ix;
        my_read(m_socket, &sz, 4);
        sz = ntohl(sz);

        Bytes bytes(sz);
        for (ix = 0; sz > CHUNK_SIZE; sz -= CHUNK_SIZE, ix += CHUNK_SIZE)
        	my_read(m_socket, &bytes[0]+ix, CHUNK_SIZE);

        my_read(m_socket, &bytes[0]+ix, bytes.size()-ix);

        return bytes;
}

void Socket::write_string(const std::string &str)
{
	byte *ptr = (byte *)(str.c_str());
	write_bytes(Bytes(ptr, ptr+str.size()));
}

std::string Socket::read_string()
{
	Bytes bytes = read_bytes();
	char *ptr = reinterpret_cast<char*>(&bytes[0]);
	return std::string(ptr, bytes.size());
}

ClientSocket::ClientSocket(const char *host_ip, size_t port)
{
	const int MAX_SLEEP = 16;

	struct sockaddr_in addr;
	int res;

	if (-1 == m_socket)
	{
		perror("cannot create socket");
		exit(EXIT_FAILURE);
	}

	memset(&addr, 0, sizeof(addr));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	res = inet_pton(AF_INET, host_ip, &addr.sin_addr);

	if (0 > res)
	{
		perror("error: first parameter is not a valid address family");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
	else if (0 == res)
	{
		perror("error: second parameter does not contain valid IP address");
		close(m_socket);
		exit(EXIT_FAILURE);
	}

	// exponential backoff algorithm
	for (int sec = 1; sec < MAX_SLEEP; sec <<= 1)
	{
		int ret;
		if (0 == (ret = connect(m_socket, (struct sockaddr *)&addr, sizeof(addr))))
		{
			return;
		}

		sleep(sec);

		// state of m_socket is unspecified after failure. need to recreate
		close(m_socket);
		m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}

	perror("connect failed");
	close(m_socket);
	exit(EXIT_FAILURE);
}

ServerSocket::ServerSocket()
{
}

ServerSocket::ServerSocket(size_t port)
{
	struct sockaddr_in addr;

	if(-1 == m_socket)
	{
		perror("can not create socket");
		exit(EXIT_FAILURE);
	}

	memset(&addr, 0, sizeof addr);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;

	if(-1 == bind(m_socket,(struct sockaddr *)&addr, sizeof addr))
	{
		perror("error bind failed");
		close(m_socket);
		exit(EXIT_FAILURE);
	}

	if(-1 == listen(m_socket, 10))
	{
		perror("error listen failed");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
}

Socket *ServerSocket::accept()
{
	int socket = ::accept(m_socket, NULL, NULL);

	if(socket < 0)
	{
		fprintf(stderr, "accept() failed: %s\n", strerror(errno));
		close(m_socket);
		exit(EXIT_FAILURE);
	}

	m_sockets.push_back(socket);

	return new Socket(socket);
}

ServerSocket::~ServerSocket() {}





int main()
{
 //Bytes b;


 printf("Hello to Android World\n");
 return 1;
}
