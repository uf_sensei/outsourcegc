#include <jni.h>

#include <iostream>
#include <stdint.h>
#include <algorithm>
#include <cassert>
#include <string>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>


#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "gmp.h"
#include "sha.h"
#include "pbc.h"
#include "aes.h"


#include "pbc_a_param.h"
#include "symtab.h"
#include "param.h"

typedef uint8_t byte;


class Bytes : public std::vector<byte>
{
	struct bitwise_xor{ byte operator() (byte l, byte r) const { return l ^ r;  }};
    
public:
	Bytes() {}
	Bytes(uint64_t n) : std::vector<byte>(n) {}
	Bytes(uint64_t n, byte b) : std::vector<byte>(n, b) {}
	Bytes(byte *begin, byte *end) : std::vector<byte>(begin, end) {}
	Bytes(const_iterator begin, const_iterator end) : std::vector<byte>(begin, end) {}
	Bytes(const std::vector<Bytes> &chunks) { merge(chunks); }
    
	const Bytes &operator =(const Bytes &rhs)
	{
		this->assign(rhs.begin(), rhs.end());
		return *this;
	}
    
	const Bytes &operator +=(const Bytes &rhs)
	{
		this->insert(this->end(), rhs.begin(), rhs.end());
		return *this;
	}
    
	const Bytes &operator ^=(const Bytes &rhs)
	{
		// TODO: see if this can be improved by a wider data type pointer
		assert(rhs.size() == size());
		Bytes::iterator dst = this->begin();
		Bytes::const_iterator src = rhs.begin();
		while (dst != this->end()) { *dst++ ^= *src++;}
		return *this;
	}
    
	byte get_ith_bit(size_t ix) const
	{
		//std::cout << ix<<"\n";
		assert(ix < size()*8);
		return ((*this)[ix/8] >> (ix%8)) & 0x01;
	}
    
	/*byte get_ith_bit2(size_t ix, Bytes b) const
     {
     std::cout << b.to_hex()<<" " << ix<<"\n";
     while(!(ix < size()*8))
     std::cout <<"found! "<< b.to_hex()<<" " << ix<<"\n";
     assert(ix < size()*8);
     return ((*this)[ix/8] >> (ix%8)) & 0x01;
     }*/
    
	void set_ith_bit(size_t ix, byte bit)
	{
		assert(ix < size()*8);
		static const byte INVERSE_MASK[8] =
        { 0xFE, 0xFD, 0xFB, 0xF7, 0xEF, 0xDF, 0xBF, 0x7F };
        
		(*this)[ix/8] &= INVERSE_MASK[ix%8];
		(*this)[ix/8] |= (bit&0x01) << (ix%8);
	}
    
	std::string to_hex() const;
	void from_hex(const std::string &s);
    
	Bytes hash(size_t bits) const;
	std::vector<Bytes> split(const size_t chunk_len) const;
	void merge(const std::vector<Bytes> &chunks);
};

// pre-condition: lhs.size() == rhs.size()
inline Bytes operator^ (const Bytes &lhs, const Bytes &rhs)
{
	assert(lhs.size() == rhs.size());
	Bytes ret(lhs);
	ret ^= rhs;
	return ret;
}

inline Bytes operator+ (const Bytes &lhs, const Bytes &rhs)
{
	Bytes ret(lhs);
	ret += rhs;
	return ret;
}

inline bool operator ==(const Bytes &lhs, const Bytes &rhs)
{
	return (lhs.size() != rhs.size())?
    false : std::equal(lhs.begin(), lhs.end(), rhs.begin());
}





namespace
{
    byte MASK[8] =
    {
        0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F
    };
}

Bytes Bytes::hash(size_t bits) const
{
	assert((0 < bits) && (bits <= 256));
    
	byte buf[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, &((*this)[0]), this->size());
	SHA256_Final(buf, &sha256);
    
	Bytes hash(buf, buf + (bits+7)/8);
	hash.back() &= MASK[bits % 8]; // clear the extra bits
    
	return hash;
}

namespace
{
    static const char HEX_TABLE[16] =
    {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };
}

std::string Bytes::to_hex() const
{
	std::string out;
	out.reserve(this->size()*2);
    
	for (const_iterator it = this->begin(); it != this->end(); it++)
	{
		out.push_back(HEX_TABLE[*it/16]);
		out.push_back(HEX_TABLE[*it%16]);
	}
	return out;
}

namespace
{
    static const int REVERSE_HEX_TABLE[256] =
    {
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
		-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    };
    
    static const int HEX_EXP[2] = { 16, 1 };
}

void Bytes::from_hex(const std::string &s)
{
	//std::cout << "Begin change"<<std::endl;;
	this->clear();
	this->resize((s.size()+1)/2, 0); // zero out this
    
	for (size_t ix = 0; ix < s.size(); ix++)
	{
		if (REVERSE_HEX_TABLE[s[ix]] == -1)
		{
            std::cerr << "Invalid hex format: " << s;
		}
		//std::cout << s[ix]<<" " << REVERSE_HEX_TABLE[s[ix]] << " "<< REVERSE_HEX_TABLE[s[ix]]*HEX_EXP[ix%2] << std::endl;
		(*this)[ix/2] += REVERSE_HEX_TABLE[s[ix]]*HEX_EXP[ix%2];
	}
	//std::cout << "End change"<<std::endl;
}

std::vector<Bytes> Bytes::split(const size_t chunk_len) const
{
	assert(this->size() % chunk_len == 0);
	std::vector<Bytes> ret(this->size()/chunk_len);
    
	const_iterator it = this->begin();
	for (int ix = 0; ix < ret.size(); ix++, it+= chunk_len)
		ret[ix].insert(ret[ix].end(), it, it+chunk_len);
    
	return ret;
}

void Bytes::merge(const std::vector<Bytes> &chunks)
{
	this->clear();
	this->reserve(chunks.size()*chunks[0].size());
    
	std::vector<Bytes>::const_iterator it;
	for (it = chunks.begin(); it != chunks.end(); it++) *this += *it;
}



class Prng {
    
	Bytes   m_state;
	AES_KEY m_key;
    
public:
	static int cnt;
	static const char *RANDOM_FILE;
    
	Prng() { srand(); }
	Prng(const Bytes &seed) { srand(seed); }
	virtual ~Prng() {}
    
	void srand();
	void srand(const Bytes &seed);
	Bytes rand();
	Bytes rand(size_t bits);
	uint64_t rand_range(uint64_t n);  // sample a number from { 0, 1, ..., n-1 }
};

const char *Prng::RANDOM_FILE = "/dev/urandom";

static Bytes shen_sha256(const Bytes &data, const size_t bits)
{
	static const byte MASK[8] =
    { 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };
    
	byte buf[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, &data[0], data.size());
	SHA256_Final(buf, &sha256);
    
	Bytes hash(buf, buf + (bits+7)/8);
	hash.back() &= MASK[bits % 8]; // clear the extra bits
    
	return hash;
}


const int AES_BLOCK_SIZE_IN_BITS = AES_BLOCK_SIZE*8;

void Prng::srand()
{
	Bytes seed(AES_BLOCK_SIZE);
    //printf("R1");
    
	FILE *fp = fopen(RANDOM_FILE, "r");
	fread(&seed[0], 1, seed.size(), fp);
	fclose(fp);
    
	srand(seed);
    
    //printf("R2");
}

void Prng::srand(const Bytes &seed)
{
	Bytes hashed_seed = shen_sha256(seed, AES_BLOCK_SIZE_IN_BITS);
	memset(&m_key, 0, sizeof(AES_KEY));
	AES_set_encrypt_key(&hashed_seed[0], AES_BLOCK_SIZE_IN_BITS, &m_key);
	m_state = shen_sha256(seed, AES_BLOCK_SIZE_IN_BITS);
}


uint64_t Prng::rand_range(uint64_t n) // sample a number from { 0, 1, ..., n-1 }
{
	int bit_length = 0;
	while ((1<<bit_length)<n) { bit_length++; }
    
	Bytes rnd;
	std::string hex_rnd;
	char *endptr;
	uint64_t ret = 0;
	do
	{ // repeat if the sampled number is >= n
		rnd = rand(bit_length);
		hex_rnd = "0x" + rnd.to_hex();
		ret = strtoll(hex_rnd.c_str(), &endptr, 16);
        
	} while (ret >= n);
    
	return ret;
}

Bytes Prng::rand(size_t bits)
{
	static const byte MASK[8] =
    { 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };
    
	Bytes rnd;
    
	while (bits > 128)
	{
		rnd += rand();
		bits -= 128;
	}
    
	Bytes last = rand();
    
	last.resize((bits+7)/8);
	last.back() &= MASK[bits%8]; // clear the extra bits
    
	return rnd + last;
}

int Prng::cnt = 0;

Bytes Prng::rand()
{
	Bytes rnd(AES_BLOCK_SIZE);
	AES_ecb_encrypt(&m_state[0], &rnd[0], &m_key, AES_ENCRYPT);
    
	cnt++;
    
	// update state
	long long cnt = *(long long*)(&m_state[m_state.size()-1-sizeof(cnt)]);
	cnt++;
	*(long long*)(&m_state[m_state.size()-1-sizeof(cnt)]) = cnt;
    
	return rnd;
}


class Socket
{
protected:
	int m_socket;
    
public:
	Socket();
	Socket(int socket) : m_socket(socket) {}
	virtual ~Socket();
    
	void write_bytes(const Bytes &bytes);
	Bytes read_bytes();
    
	void write_string(const std::string &str);
	std::string read_string();
};

class ClientSocket : public Socket
{
public:
	ClientSocket(const char *host_ip, size_t port);
	virtual ~ClientSocket() {}
};

class ServerSocket : public Socket
{
	std::vector<int> m_sockets;
    
public:
	ServerSocket();
	ServerSocket(size_t port);
	Socket *accept();
	virtual ~ServerSocket();
};


const int CHUNK_SIZE = 1024;

Socket::Socket() : m_socket(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))
{
    
    int one = 1; setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
    
}

Socket::~Socket()
{
	shutdown(m_socket, SHUT_RDWR);
    close(m_socket);
}

void Socket::write_bytes(const Bytes &bytes)
{
	uint32_t sz = htonl(bytes.size());
    
	send(m_socket, &sz, sizeof(sz), 0);
	sz = bytes.size();
    
	size_t ix = 0;
	for (; sz > CHUNK_SIZE; sz -= CHUNK_SIZE, ix += CHUNK_SIZE)
        send(m_socket, &bytes[ix], CHUNK_SIZE, 0);
	send(m_socket, &bytes[ix], bytes.size()-ix, 0);
}

inline void my_read(int socket, void *data, size_t n)
{
	for (size_t ix = 0; ix != n; )
		ix += recv(socket, reinterpret_cast<char*>(data)+ix, n-ix, 0);
}

Bytes Socket::read_bytes()
{
    uint32_t sz, ix;
    my_read(m_socket, &sz, 4);
    sz = ntohl(sz);
    
    Bytes bytes(sz);
    for (ix = 0; sz > CHUNK_SIZE; sz -= CHUNK_SIZE, ix += CHUNK_SIZE)
        my_read(m_socket, &bytes[0]+ix, CHUNK_SIZE);
    
    my_read(m_socket, &bytes[0]+ix, bytes.size()-ix);
    
    return bytes;
}

void Socket::write_string(const std::string &str)
{
	byte *ptr = (byte *)(str.c_str());
	write_bytes(Bytes(ptr, ptr+str.size()));
}

std::string Socket::read_string()
{
	Bytes bytes = read_bytes();
	char *ptr = reinterpret_cast<char*>(&bytes[0]);
	return std::string(ptr, bytes.size());
}

ClientSocket::ClientSocket(const char *host_ip, size_t port)
{
	const int MAX_SLEEP = 16;
    
	struct sockaddr_in addr;
	int res;
    
	if (-1 == m_socket)
	{
		perror("cannot create socket");
		exit(EXIT_FAILURE);
	}
    
	memset(&addr, 0, sizeof(addr));
    
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	res = inet_pton(AF_INET, host_ip, &addr.sin_addr);
    
	if (0 > res)
	{
		perror("error: first parameter is not a valid address family");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
	else if (0 == res)
	{
		perror("error: second parameter does not contain valid IP address");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
    
	// exponential backoff algorithm
	for (int sec = 1; sec < MAX_SLEEP; sec <<= 1)
	{
		int ret;
		if (0 == (ret = connect(m_socket, (struct sockaddr *)&addr, sizeof(addr))))
		{
			return;
		}
        
		sleep(sec);
        
		// state of m_socket is unspecified after failure. need to recreate
		close(m_socket);
		m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}
    
	perror("connect failed");
	close(m_socket);
	exit(EXIT_FAILURE);
}

ServerSocket::ServerSocket()
{
}

ServerSocket::ServerSocket(size_t port)
{
	struct sockaddr_in addr;
    
	if(-1 == m_socket)
	{
		perror("can not create socket");
		exit(EXIT_FAILURE);
	}
    
	memset(&addr, 0, sizeof addr);
    
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
    
	if(-1 == bind(m_socket,(struct sockaddr *)&addr, sizeof addr))
	{
		perror("error bind failed");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
    
	if(-1 == listen(m_socket, 10))
	{
		perror("error listen failed");
		close(m_socket);
		exit(EXIT_FAILURE);
	}
}

Socket *ServerSocket::accept()
{
	int socket = ::accept(m_socket, NULL, NULL);
    
	if(socket < 0)
	{
		fprintf(stderr, "accept() failed: %s\n", strerror(errno));
		close(m_socket);
		exit(EXIT_FAILURE);
	}
    
	m_sockets.push_back(socket);
    
	return new Socket(socket);
}

ServerSocket::~ServerSocket() {}



class G_Base
{
	class Init
	{
	public:
		Init();
		~Init();
		pairing_t 		          m_p;
		element_t                 m_r;
		element_pp_t           m_g_pp;
        
		Prng                   m_prng;
	};
    
protected:
	static Init I;
    
	element_t m_e;
    
public:
	static const char *PARAMS_FILE;
    
	int length_in_bytes() const
	{
		return element_length_in_bytes(const_cast<element_s*>(&m_e[0]));
	}
    
	Bytes to_bytes() const
	{
		Bytes ret(length_in_bytes());
		element_to_bytes(&ret[0], const_cast<element_s*>(&m_e[0]));
		return ret;
	}
    
	void from_bytes(const Bytes &b)
	{
		element_from_bytes(const_cast<element_s*>(&m_e[0]), const_cast<byte*>(&b[0]));
	}
    
	virtual void random() = 0;
	virtual void random(Prng &prng) = 0;
	virtual void random(int length) = 0;
    
	virtual ~G_Base()
	{
		element_clear(m_e);
	}
};

class G;

class Z : public G_Base
{
	friend const G &operator ^(const G &lhs, const Z &rhs);
	friend void    exp(G &out, const G &lhs, const Z &rhs);
	friend void pp_exp(G &out, const G &lhs, const Z &rhs);
    
	void init()
	{
		element_init_Zr(m_e, I.m_p);
	}
public:
	Z()
	{
		init();
	}
    
	Z(const Z &z)
	{
		init();
		element_set(m_e, const_cast<element_s*>(&(z.m_e[0])));
	}
    
	Z(const Bytes &b)
	{
		init();
		element_from_bytes(m_e, const_cast<byte*>(&b[0]));
	}
    
	Z(signed long int i)
	{
		init();
		element_set_si(m_e, i);
	}
    
	virtual void random() { random(I.m_prng); }
    
	virtual void random(Prng &prng)
	{
		Bytes rnd = prng.rand(element_length_in_bytes(m_e)*8);
		element_from_bytes(m_e, &rnd[0]);
	}
    
	virtual void random(int length)
	{
		Bytes rnd = I.m_prng.rand(length);
		element_from_bytes(m_e, &rnd[0]);
	}
    
	Z &operator=(const Z &rhs)
	{
		element_set(m_e, const_cast<element_s*>(&(rhs.m_e[0])));
		return *this;
	}
    
	Z &operator+= (const Z &rhs)
	{
		element_add(m_e, m_e, const_cast<element_s*>(&(rhs.m_e[0])));
		return *this;
	}
    
	Z &operator-= (const Z &rhs)
	{
		element_sub(m_e, m_e, const_cast<element_s*>(&(rhs.m_e[0])));
		return *this;
	}
    
	Z &operator*= (const Z &rhs)
	{
		element_mul(m_e, m_e, const_cast<element_s*>(&(rhs.m_e[0])));
		return *this;
	}
};

inline Z operator+(const Z &lhs, const Z &rhs)
{
	Z ret(lhs);
	ret += rhs;
	return ret;
}

inline Z operator-(const Z &lhs, const Z &rhs)
{
	Z ret(lhs);
	ret -= rhs;
	return ret;
}

inline Z operator*(const Z &lhs, const Z &rhs)
{
	Z ret(lhs);
	ret *= rhs;
	return ret;
}

extern void    exp(G &out, const G &lhs, const Z &rhs);
extern void pp_exp(G &out, const G &lhs, const Z &rhs);

// multiplicative group
class G : public G_Base
{
	element_pp_t                        m_e_pp;
	bool		                m_is_e_pp_init;
	void (*m_fptr_exp)(G&, const G&, const Z&); // use to call exp or pp_exp
    
	friend const G &operator ^(const G &lhs, const Z &rhs);
	friend void    exp(G &out, const G &lhs, const Z &rhs);
	friend void pp_exp(G &out, const G &lhs, const Z &rhs);
    
	void init()
	{
		m_fptr_exp = &exp;
		m_is_e_pp_init = false;
		element_init_G1(m_e, I.m_p);
	}
    
	static G ret;
    
public:
	G()
	{
		init();
	}
    
	G(const G &g)
	{
		init();
		element_set(m_e, const_cast<element_s*>(&(g.m_e[0])));
	}
    
	G(const Bytes &b)
	{
		init();
		element_from_bytes(m_e, const_cast<byte*>(&b[0]));
	}
    
	void from_bytes(const Bytes &b)
	{
		if (m_is_e_pp_init) element_pp_clear(m_e_pp);
		element_clear(m_e);
        
		init();
		G_Base::from_bytes(b);
	}
    
	// it's worth-preprocessing only if 5+ exps will be executed
	void fast_exp()
	{
		if (m_is_e_pp_init) element_pp_clear(m_e_pp);
		element_pp_init(m_e_pp, m_e);
		m_is_e_pp_init = true;
		m_fptr_exp = &pp_exp;
	}
    
	virtual void random() { random(I.m_prng); }
    
	virtual void random(Prng &prng)
	{
		m_fptr_exp = &exp; // preprocessing needs to be redone
        
        //		element_random(I.m_r);
		Bytes rnd = prng.rand(element_length_in_bytes(I.m_r)*8);
		element_from_bytes(I.m_r, &rnd[0]);
		element_pp_pow_zn(m_e, I.m_r, I.m_g_pp);
	}
    
	virtual void random(int length)
	{
		m_fptr_exp = &exp; // preprocessing needs to be redone
        
		Bytes rnd = I.m_prng.rand(length);
		element_from_bytes(I.m_r, &rnd[0]);
		element_pp_pow_zn(m_e, I.m_r, I.m_g_pp);
	}
    
	G &operator=(const G &rhs)
	{
		m_fptr_exp = &exp; // preprocessing needs to be redone
		element_set(m_e, const_cast<element_s*>(&(rhs.m_e[0])));
		return *this;
	}
    
	G &operator*= (const G &rhs)
	{
		m_fptr_exp = &exp; // preprocessing needs to be redone
		element_mul(m_e, m_e, const_cast<element_s*>(&(rhs.m_e[0])));
		return *this;
	}
    
	G &operator/= (const G &rhs)
	{
		m_fptr_exp = &exp; // preprocessing needs to be redone
		element_div(m_e, m_e, const_cast<element_s*>(&(rhs.m_e[0])));
		return *this;
	}
    
	bool operator==(const G &rhs) const
	{
		return element_cmp
		(
         const_cast<element_s*>(&(this->m_e[0])),
         const_cast<element_s*>(&(rhs.m_e[0]))
         ) == 0;
	}
    
	virtual ~G()
	{
		if (m_is_e_pp_init) element_pp_clear(m_e_pp);
	}
};

inline const G &operator^(const G &lhs, const Z &rhs)
{
	lhs.m_fptr_exp(G::ret, lhs, rhs);
	return G::ret;
}



static FILE *fp = 0;

// excerpt from PBC. super slow due to frequent IO access (commented lines)
static void my_file_mpz_random(mpz_t r, mpz_t limit, void *data) {
    //  char *filename = (char *) data;
    //  FILE *fp;
    int n, bytecount, leftover;
    unsigned char *bytes;
    mpz_t z;
    mpz_init(z);
    //  fp = fopen(filename, "rb");
    //  if (!fp) return;
    n = mpz_sizeinbase(limit, 2);
    bytecount = (n + 7) / 8;
    leftover = n % 8;
    bytes = (unsigned char *) pbc_malloc(bytecount);
    for (;;) {
        if (!fread(bytes, 1, bytecount, fp)) {
            pbc_warn("error reading source of random bits");
            return;
        }
        if (leftover) {
            *bytes = *bytes % (1 << leftover);
        }
        mpz_import(z, bytecount, 1, 1, 0, 0, bytes);
        if (mpz_cmp(z, limit) < 0) break;
    }
    //  fclose(fp);
    mpz_set(r, z);
    mpz_clear(z);
    pbc_free(bytes);
}

static void my_random_set_file(char *filename)
{
	fp = fopen(filename, "rb");
	if (!fp) pbc_die("random open failed");
    
	pbc_random_set_function(my_file_mpz_random, filename);
}

static void my_close_random_file()
{
	fclose(fp);
}

const char *G_Base::PARAMS_FILE = "CCS.params";
G_Base::Init G_Base::I;



/*int my_pbc_param_init_set_buf(pbc_param_t par, const char *input, size_t len) {
 symtab_t tab;
 symtab_init(tab);
 printf("after init\n"); fflush(stdout);
 read_symtab(tab, input, len);
 printf("after tab\n"); fflush(stdout);
 int res = param_set_tab(par, tab);
 printf("after red2\n"); fflush(stdout);
 symtab_forall_data(tab, pbc_free);
 printf("after forall\n"); fflush(stdout);
 symtab_clear(tab);
 printf("after clear2\n"); fflush(stdout);
 return res;
 }*/

int my_pairing_init_set_buf(pairing_t pairing, const char *input, size_t len)
{
    pbc_param_t par;
    printf("after par\n"); fflush(stdout);
    int res = pbc_param_init_set_buf(par, input, len);
    printf("after res\n"); fflush(stdout);
    if (res) {
        pbc_error("error initializing pairing");
        return 1;
    }
    pairing_init_pbc_param(pairing, par);
    printf("after p init\n"); fflush(stdout);
    pbc_param_clear(par);
    printf("after clear\n"); fflush(stdout);
    return 0;
}



G_Base::Init::Init()
{
    printf("InitInitStart"); fflush(stdout);
    
    // initialize pairing_t from PARAMS_FILE
	char s[16384];
	FILE *f = fopen(G_Base::PARAMS_FILE, "r");
	if (!f) pbc_die("params open failed");
    
    //printf("1\n"); fflush(stdout);
    
	size_t count = fread(s, 1, 16384, f);
	if (!count) pbc_die("input error");
	fclose(f);
    
    element_t g2;
    printf("2.7\n"); fflush(stdout);
    printf("m_p:%p\ncount:%d\n",m_p,count); fflush(stdout);
    
    /*for(int i=0;i<count;i++)
     std::cout<< s[i] <<"\n";*/
    //count--;
    
    //pairing_t 		          m_p2;
    
	if (my_pairing_init_set_buf(m_p, s, count)) pbc_die("pairing init failed");
    
    printf("3.5\n"); fflush(stdout);
    
    // set random source
	my_random_set_file(const_cast<char*>(Prng::RANDOM_FILE));
    
    printf("4\n"); fflush(stdout);
    
	// fast random element selection
	element_t g;
	element_init_G1(g, m_p);
	element_random(g);
	element_pp_init(m_g_pp, g);
	element_clear(g);
    
	// temporary variable
	element_init_Zr(m_r, m_p);
    
    //printf("InitInitEnd"); fflush(stdout);
}

G_Base::Init::~Init()
{
    element_clear(m_r);
    element_pp_clear(m_g_pp);
    my_close_random_file();
    //pairing_clear(p); // can't be executed. there are other static element_t
    //printf("Init~InitEnd");
}

G G::ret;

void exp(G &out, const G &lhs, const Z &rhs)
{
	element_s *non_const_lhs_e = const_cast<element_s*>(&(lhs.m_e[0]));
	element_s *non_const_rhs_e = const_cast<element_s*>(&(rhs.m_e[0]));
	element_pow_zn(out.m_e, non_const_lhs_e, non_const_rhs_e);
}

void pp_exp(G &out, const G &lhs, const Z &rhs)
{
	element_pp_s *non_const_lhs_e_pp = const_cast<element_pp_s*>(&(lhs.m_e_pp[0]));
	element_s *non_const_rhs_e = const_cast<element_s*>(&(rhs.m_e[0]));
	element_pp_pow_zn(out.m_e, non_const_rhs_e, non_const_lhs_e_pp);
}



using std::vector;

struct Gate
{
	uint8_t          m_tag;
	uint64_t         m_ref_cnt;
	vector<uint64_t> m_input;   // input gates
	vector<bool>     m_table;   // truth table
    
	vector<uint64_t> m_input_idx;
	uint64_t         m_idx;
};


inline bool is_xor(const Gate &g)
{
	uint8_t tbl = 0;
	for (size_t ix = 0; ix < g.m_table.size(); ix++) { tbl |= g.m_table[ix]<<ix; }
	return (g.m_input.size()==1 && tbl==0x02) || (g.m_input.size()==2 && tbl==0x06);
}


class Circuit
{
private:
	Gate          m_current_gate;
    
	// for text parser
	std::ifstream m_circuit_fs;
    
	// for binary parser
	uint64_t      m_gate_idx;
	int           m_circuit_fd;
	uint8_t      *m_ptr, *m_ptr_begin, *m_ptr_end;
    
	// hearder information
	uint64_t      m_gate_cnt;
    
	uint32_t      m_gen_inp_cnt;
	uint32_t      m_gen_out_cnt;
	uint32_t      m_evl_inp_cnt;
	uint32_t      m_evl_out_cnt;
    
public:
	uint8_t       m_cnt_size;
	uint64_t      m_cnt;
    
	//Bytes permvalue;
    
	enum { ETC = 0, GEN_INP = 1, EVL_INP = 2, GEN_OUT = 3, EVL_OUT = 4};
    
	Circuit() :
    m_gate_idx(0),
    m_ptr(0), m_ptr_begin(0), m_ptr_end(0),
    m_gate_cnt(0), m_gen_inp_cnt(0), m_gen_out_cnt(0), m_evl_inp_cnt(0), m_evl_out_cnt(0),
    m_cnt_size(0), m_cnt(0), m_circuit_fd(0) {}
    
	~Circuit() { m_circuit_fs.close(); }
    
	bool load(const char *circuit_file);
	bool load_binary(const char *circuit_file);
	bool load_binary_old(const char *circuit_file);
    
	bool more_gate() { return !m_circuit_fs.eof(); }
	bool more_gate_binary() const { return m_ptr < m_ptr_end; }
    
	const Gate &next_gate();
	const Gate &next_gate_binary();
	const Gate &next_gate_binary_old();
    
	void evaluate(const Bytes &gen_inp, const Bytes &evl_inp, Bytes &gen_out, Bytes &evl_out);
	void evaluate_binary(const Bytes &gen_inp, const Bytes &evl_inp, Bytes &gen_out, Bytes &evl_out);
	void evaluate_binary_old(const Bytes &gen_inp, const Bytes &evl_inp, Bytes &gen_out, Bytes &evl_out);
    
	void reload() { m_circuit_fs.clear();  m_circuit_fs.seekg(0, std::ios_base::beg); }
	void reload_binary_old()
	{ m_ptr = m_ptr_begin+25; m_gate_idx = 0; }
	void reload_binary()
	{ m_ptr = m_ptr_begin+25+m_cnt_size; m_gate_idx = 0; }
    
	uint64_t gate_cnt()    const { return m_gate_cnt;    }
	uint32_t gen_inp_cnt() const { return m_gen_inp_cnt; }
	uint32_t gen_out_cnt() const { return m_gen_out_cnt; }
	uint32_t evl_inp_cnt() const { return m_evl_inp_cnt; }
	uint32_t evl_out_cnt() const { return m_evl_out_cnt; }
};

std::ostream &operator <<(std::ostream &os, const Gate &gate);

inline void tokenize
(
 std::vector<std::string>& tokens,
 const std::string& str,
 const std::string& delimiters = " "
 )
{
	using namespace std;
    
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);
    
    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}


const Gate & Circuit::next_gate()
{
	assert(!m_circuit_fs.eof());
    
	using std::string;
	using std::vector;
    
	string line;
	std::getline(m_circuit_fs, line);
    
	m_current_gate.m_input.clear();
	m_current_gate.m_table.clear();
    
	if (line.find("output.bob") != string::npos)
	{
		m_current_gate.m_tag = GEN_OUT;
	}
	else if (line.find("output.alice") != string::npos)
	{
		m_current_gate.m_tag = EVL_OUT;
	}
	else if (line.find("input.bob") != string::npos)
	{
		m_current_gate.m_tag = GEN_INP;
	}
	else if (line.find("input.alice") != string::npos)
	{
		m_current_gate.m_tag = EVL_INP;
	}
	else
	{
		m_current_gate.m_tag = ETC;
	}
    
	if (line.find("table") != string::npos)
	{
		vector<string> tokens;
		tokenize(tokens, line);
        
		vector<string>::iterator it = tokens.begin();
        
		// parsing gate arity
		while (it->compare("arity")) it++;
		int arity = atoi((++it)->c_str());
        
		// parsing table entries
		while (it->compare("table")) it++;
		it += 2; // skip 'table' and '['
		for (int i = 0; i < (1<<arity); i++, it++)
			m_current_gate.m_table.push_back(it->at(0) - '0');
        
		while (it->compare("inputs")) it++;
		it += 2; // skip 'inputs' and '['
		for (int i = 0; i < arity; i++, it++)
		{
			const char *str = it->c_str();
			char *endptr = const_cast<char*>(str + it->size());
			long long idx = strtoll(str, &endptr, 10);
			m_current_gate.m_input.push_back(idx);
		}
	}
    
	return m_current_gate;
}


/*const Gate & Circuit::next_gate_binary_old()
 {
 assert(m_ptr < m_ptr_end);
 
 uint8_t hdr = *m_ptr; m_ptr += sizeof(uint8_t);
 
 m_current_gate.m_tag = (hdr>>5)&0x07;
 
 if (m_current_gate.m_tag == GEN_INP || m_current_gate.m_tag == EVL_INP)
 {
 m_current_gate.m_input.clear();
 m_current_gate.m_table.clear();
 }
 else
 {
 uint8_t arity = ((hdr>>4)&0x01) + 1;
 
 m_current_gate.m_input.resize(arity);
 m_current_gate.m_table.resize(1<<arity);
 
 for (size_t ix = 0; ix < m_current_gate.m_table.size(); ix++)
 m_current_gate.m_table[ix] = (hdr>>ix)&0x01;
 
 if (m_gate_idx <= UINT16_MAX)
 {
 for (size_t ix = 0; ix < m_current_gate.m_input.size(); ix++)
 {
 m_current_gate.m_input[ix] = *reinterpret_cast<uint16_t *>(m_ptr);
 m_ptr += sizeof(uint16_t);
 }
 }
 else if (m_gate_idx <= UINT32_MAX)
 {
 for (size_t ix = 0; ix < m_current_gate.m_input.size(); ix++)
 {
 m_current_gate.m_input[ix] = *reinterpret_cast<uint32_t *>(m_ptr);
 m_ptr += sizeof(uint32_t);
 }
 }
 else
 {
 for (size_t ix = 0; ix < m_current_gate.m_input.size(); ix++)
 {
 m_current_gate.m_input[ix] = *reinterpret_cast<uint64_t *>(m_ptr);
 m_ptr += sizeof(uint64_t);
 }
 }
 }
 
 m_current_gate.m_ref_cnt = 0;
 memcpy(&m_current_gate.m_ref_cnt, m_ptr, m_cnt_size);
 m_ptr += m_cnt_size;
 
 m_gate_idx++;
 
 return m_current_gate;
 }*/


const Gate & Circuit::next_gate_binary()
{
	assert(m_ptr < m_ptr_end);
    
	uint8_t hdr = *m_ptr; m_ptr += sizeof(uint8_t);
    
	m_current_gate.m_tag = (hdr>>5)&0x07;
    
	if (m_current_gate.m_tag == GEN_INP || m_current_gate.m_tag == EVL_INP)
	{
		m_current_gate.m_input.clear();
		m_current_gate.m_table.clear();
	}
	else
	{
		uint8_t arity = ((hdr>>4)&0x01) + 1;
        
		m_current_gate.m_input.resize(arity);
		m_current_gate.m_table.resize(1<<arity);
        
		for (size_t ix = 0; ix < m_current_gate.m_table.size(); ix++)
			m_current_gate.m_table[ix] = (hdr>>ix)&0x01;
        
		for (size_t ix = 0; ix < m_current_gate.m_input.size(); ix++)
		{
			memcpy(&(m_current_gate.m_input[ix]), m_ptr, m_cnt_size);
			m_ptr += m_cnt_size;
		}
	}
    
	m_current_gate.m_ref_cnt = 0;
	memcpy(&m_current_gate.m_ref_cnt, m_ptr, m_cnt_size);
	m_ptr += m_cnt_size;
    
	m_gate_idx++;
    
	return m_current_gate;
}

bool Circuit::load(const char *circuit_file)
{
	m_circuit_fs.open(circuit_file, std::ifstream::in);
    
	if (m_circuit_fs.fail())
	{
		perror("can't open file for reading");
		return false;
	}
    
	return true;
}


bool Circuit::load_binary_old(const char *circuit_file)
{
	struct stat statbuf;
    
	if ((m_circuit_fd = open(circuit_file, O_RDONLY)) < 0)
	{
		perror("can't open file for reading");
		return false;
	}
    
	if (fstat(m_circuit_fd, &statbuf) < 0)
	{
		perror("fstat in load_binary failed");
		return false;
	}
    
    if ((m_ptr_begin = (uint8_t *)mmap(0, statbuf.st_size, PROT_READ, MAP_SHARED, m_circuit_fd, 0)) == MAP_FAILED)
	{
		perror("mmap in load_binary failed");
		return false;
	}
    m_ptr = m_ptr_begin;
	m_ptr_end = m_ptr_begin + statbuf.st_size; // mark the end of the file
    
	m_gate_cnt    = *reinterpret_cast<uint64_t *>(m_ptr); m_ptr += sizeof(uint64_t);
	m_gen_inp_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_evl_inp_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_gen_out_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_evl_out_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_cnt_size    = *reinterpret_cast<uint8_t  *>(m_ptr); m_ptr += sizeof(uint8_t );
    
	m_gate_idx = 0;
    
	return true;
}


bool Circuit::load_binary(const char *circuit_file)
{
	struct stat statbuf;
    
	if ((m_circuit_fd = open(circuit_file, O_RDONLY)) < 0)
	{
		perror("can't open file for reading");
		return false;
	}
    
	if (fstat(m_circuit_fd, &statbuf) < 0)
	{
		perror("fstat in load_binary failed");
		return false;
	}
    
    if ((m_ptr_begin = (uint8_t *)mmap(0, statbuf.st_size, PROT_READ, MAP_SHARED, m_circuit_fd, 0)) == MAP_FAILED)
	{
		perror("mmap in load_binary failed");
		return false;
	}
    m_ptr = m_ptr_begin;
	m_ptr_end = m_ptr_begin + statbuf.st_size; // mark the end of the file
    
	m_gate_cnt    = *reinterpret_cast<uint64_t *>(m_ptr); m_ptr += sizeof(uint64_t);
	m_gen_inp_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_evl_inp_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_gen_out_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_evl_out_cnt = *reinterpret_cast<uint32_t *>(m_ptr); m_ptr += sizeof(uint32_t);
	m_cnt_size    = *reinterpret_cast<uint8_t  *>(m_ptr); m_ptr += sizeof(uint8_t );
    
	memcpy(&m_cnt, m_ptr, m_cnt_size);
	m_ptr += m_cnt_size;
    
	m_gate_idx = 0;
    
	return true;
}

std::ostream &operator<<(std::ostream &os, const Gate &g)
{
	switch(g.m_tag)
	{
        case Circuit::GEN_INP:
            os << "GEN_INP: ";
            break;
            
        case Circuit::EVL_INP:
            os << "EVL_INP: ";
            break;
            
        case Circuit::GEN_OUT:
            os << "GEN_OUT: ";
            break;
            
        case Circuit::EVL_OUT:
            os << "EVL_OUT: ";
            break;
	}
    
	if (g.m_input.size() == 0)
		return os;
    
	os << "arity: " << g.m_input.size();
    
	os << " table [";
	for (vector<bool>::const_iterator it = g.m_table.begin(); it != g.m_table.end(); it++)
		os << " " << (int)*it;
	os << " ]";
    
	os << " input [";
	for (vector<uint64_t>::const_iterator it = g.m_input.begin(); it != g.m_input.end(); it++)
		os << " " << *it;
	os << " ]";
    
	os << ", cnt: " << g.m_ref_cnt;
    
	return os;
}


void Circuit::evaluate(const Bytes &gen_inp, const Bytes &evl_inp, Bytes &gen_out, Bytes &evl_out)
{
	uint32_t gen_inp_ix = 0, evl_inp_ix = 0, gen_out_ix = 0, evl_out_ix = 0;
	uint64_t wire_ix = 0;
    
	Bytes wire(1);
	gen_out.resize(1);
	evl_out.resize(1);
    
	while (more_gate())
	{
		const Gate &g = next_gate();
        
		if (wire_ix/8+1 > wire.size())
			wire.resize(wire.size()*2);
        
		if (g.m_tag == GEN_INP)
		{
			wire.set_ith_bit(wire_ix, gen_inp.get_ith_bit(gen_inp_ix++));
		}
		else if (g.m_tag == EVL_INP)
		{
			wire.set_ith_bit(wire_ix, evl_inp.get_ith_bit(evl_inp_ix++));
		}
		else
		{
			size_t inp_ix = 0;
            
			for (size_t ix = 0; ix < g.m_input.size(); ix++)
			{
				inp_ix |= ((wire.get_ith_bit(g.m_input[ix]) & 0x01) << ix);
			}
			wire.set_ith_bit(wire_ix, g.m_table[inp_ix]);
            
			if (g.m_tag == GEN_OUT)
			{
				if (gen_out_ix/8+1 > gen_out.size())
					gen_out.resize(gen_out.size()*2);
				gen_out.set_ith_bit(gen_out_ix++, wire.get_ith_bit(wire_ix));
			}
			else if (g.m_tag == EVL_OUT)
			{
				if (evl_out_ix/8+1 > evl_out.size())
					evl_out.resize(evl_out.size()*2);
				evl_out.set_ith_bit(evl_out_ix++, wire.get_ith_bit(wire_ix));
			}
		}
        
		wire_ix++;
	}
	gen_out.resize(gen_out_ix/8+1);
	evl_out.resize(evl_out_ix/8+1);
}


void Circuit::evaluate_binary_old(const Bytes &gen_inp, const Bytes &evl_inp, Bytes &gen_out, Bytes &evl_out)
{
	uint32_t gen_inp_ix = 0, evl_inp_ix = 0, gen_out_ix = 0, evl_out_ix = 0;
	uint64_t wire_ix = 0;
    
	Bytes wire((m_gate_cnt+7)/8);
    
	gen_out.resize((m_gen_out_cnt+7)/8);
	evl_out.resize((m_evl_out_cnt+7)/8);
    
	while (more_gate_binary())
	{
		const Gate &g = next_gate_binary_old();
        
		if (g.m_tag == GEN_INP)
		{
			wire.set_ith_bit(wire_ix, gen_inp.get_ith_bit(gen_inp_ix++));
		}
		else if (g.m_tag == EVL_INP)
		{
			wire.set_ith_bit(wire_ix, evl_inp.get_ith_bit(evl_inp_ix++));
		}
		else
		{
			size_t tbl_ix = 0;
            
			for (size_t ix = 0; ix < g.m_input.size(); ix++)
			{
				tbl_ix |= ((wire.get_ith_bit(g.m_input[ix]) & 0x01) << ix);
			}
			wire.set_ith_bit(wire_ix, g.m_table[tbl_ix]);
            
			if (g.m_tag == GEN_OUT)
			{
				gen_out.set_ith_bit(gen_out_ix++, wire.get_ith_bit(wire_ix));
			}
			else if (g.m_tag == EVL_OUT)
			{
				evl_out.set_ith_bit(evl_out_ix++, wire.get_ith_bit(wire_ix));
			}
		}
        
		wire_ix++;
	}
}


void Circuit::evaluate_binary(const Bytes &gen_inp, const Bytes &evl_inp, Bytes &gen_out, Bytes &evl_out)
{
	uint32_t gen_inp_ix = 0, evl_inp_ix = 0, gen_out_ix = 0, evl_out_ix = 0;
	uint64_t wire_ix = 0;
    
	Bytes wire((m_cnt+7)/8);
    
	std::cout << "wire.size(): " << wire.size() << std::endl;
    
	gen_out.resize((m_gen_out_cnt+7)/8);
	evl_out.resize((m_evl_out_cnt+7)/8);
    
	while (more_gate_binary())
	{
		const Gate &g = next_gate_binary();
        
		if (g.m_tag == GEN_INP)
		{
			wire.set_ith_bit(g.m_ref_cnt, gen_inp.get_ith_bit(gen_inp_ix++));
		}
		else if (g.m_tag == EVL_INP)
		{
			wire.set_ith_bit(g.m_ref_cnt, evl_inp.get_ith_bit(evl_inp_ix++));
		}
		else
		{
			size_t tbl_ix = 0;
            
			for (size_t ix = 0; ix < g.m_input.size(); ix++)
			{
				tbl_ix |= wire.get_ith_bit(g.m_input[ix]) << ix;
			}
			wire.set_ith_bit(g.m_ref_cnt, g.m_table[tbl_ix]);
            
			if (g.m_tag == GEN_OUT)
			{
				gen_out.set_ith_bit(gen_out_ix++, wire.get_ith_bit(g.m_ref_cnt));
			}
			else if (g.m_tag == EVL_OUT)
			{
				evl_out.set_ith_bit(evl_out_ix++, wire.get_ith_bit(g.m_ref_cnt));
			}
		}
        
		wire_ix++;
	}
}


class ClawFree
{
    
	G m_g, m_h; // generators of the Diffie-Helman-based claw-free collections
    
public:
	ClawFree() {}
    
	void init()
	{
		m_g.random();
		m_h.random();
	}
    
	Bytes to_bytes() const
	{
		return m_g.to_bytes() + m_h.to_bytes();
	}
    
	void from_bytes(const Bytes &b)
	{
		std::vector<Bytes> chunks = b.split(b.size()/2);
		m_g.from_bytes(chunks[0]);
		m_h.from_bytes(chunks[1]);
		m_h.fast_exp();
	}
    
	size_t size_in_bytes() const
	{
		return m_g.length_in_bytes() + m_h.length_in_bytes();
	}
    
	Z D() const
	{
		Z z;
		z.random();
		return z;
	}
    
	Z D(Prng &prng) const
	{
		Z z;
		z.random(prng);
		return z;
	}
    
	// R(m) = h^m
	G R(const Z &m)
	{
		return m_h^m;
	}
    
	// F(b,m) = g^b h^m
	G F(bool b, const Z &m)
	{
		G g = R(m);
		if (b) { g *= m_g; }
		return g;
	}
    
	static void claw_free_pool(const bool b, const bool p, Bytes &k);
	static void claw_free_pool(const bool b, const bool p, Z &m, Bytes &k);
	static void claw_free_pool(const bool b, const bool p, Z &m, G &M, Bytes &k);
    
	static void claw_free_pool(const bool b, const bool p, Bytes &k, Prng &prng);
	static void claw_free_pool(const bool b, const bool p, Z &m, Bytes &k, Prng &prng);
	static void claw_free_pool(const bool b, const bool p, Z &m, G &M, Bytes &k, Prng &prng);
};



struct EnvParams
{
	EnvParams() :
    secu_param(0), stat_param(0),
    wrld_rank(0),
    node_rank(0), node_load(0), node_amnt(0),
    port_base(0), remote(0), server(0),
    circuit_file(0), private_file(0),
    ipserve_addr(0), saveouts(0) {}
    
	~EnvParams() { delete remote; delete server; }
    
	size_t        secu_param;    // security parameter
	size_t        stat_param;    // statistical security parameter
    
	int           wrld_rank;
    
	int           node_rank;
	int           node_load;
	int           node_amnt;
    
	int           port_base;
	int		thirdport_base;
    
	Socket       *remote;
	Socket       *remoteToThird;
	Socket       *remoteFromEval;
	Socket       *remoteFromGen;
	//Socket       *remote;
	ServerSocket *server;
	ServerSocket *serverGen;
	ServerSocket *serverEval;
    
	Circuit       circuit;
	ClawFree      claw_free;
    
	const char   *circuit_file; // boolean circuit computing f(x,y) -> (f1, f2)
	const char   *private_file;
    
	const char   *ipserve_addr;
	const char   *thirdipaddr;
    
	int gen_evl_node_amnt;
    
	vector<ServerSocket *> servergen;
	vector<ServerSocket *> servereval;
	vector<Socket *> toeval;
	vector<Socket *> togen;
    
	Bytes permuvalue;
    
	int saveouts;
};

class Env
{
	Env (EnvParams &params) : m_params(params)
	{
		exp_length = Z().length_in_bytes();
		elm_length = G().length_in_bytes();
		key_length = (params.secu_param+7)/8;
	}
    
	// prohibited member functions
	Env(const Env &);
	Env &operator=(const Env &);
    
	EnvParams  &m_params;
    
	size_t      key_length;
	size_t      exp_length;
	size_t      elm_length;
    
	static Env *instance;  // singleton instance
    
    
public:
	enum { GEN, EVL }; // for ipserver
    
	static const int IP_SERVER_PORT;
    
	static void init(EnvParams &params)
	{
		if (!instance)
		{
			instance = new Env(params);
		}
	}
    
	static void destroy()
	{
		delete instance;
	}
    
	static size_t k()
	{
		assert(instance != 0);
		return instance->m_params.secu_param;
	}
    
	static uint32_t s()
	{
		assert(instance != 0);
		return instance->m_params.stat_param;
	}
    
	static size_t key_size_in_bytes()
	{
		assert(instance != 0);
		return instance->key_length;
	}
    
	static size_t exp_size_in_bytes()
	{
		assert(instance != 0);
		return instance->exp_length;
	}
    
	static size_t elm_size_in_bytes()
	{
		assert(instance != 0);
		return instance->elm_length;
	}
    
	static Circuit &circuit()
	{
		assert(instance != 0);
		return instance->m_params.circuit;
	}
    
	static ClawFree &clawfree()
	{
		assert(instance != 0);
		return instance->m_params.claw_free;
	}
    
	static void claw_free_from_bytes(const Bytes b)
	{
		assert(instance != 0);
		instance->m_params.claw_free.from_bytes(b);
	}
    
	static int world_rank()
	{
		assert(instance != 0);
		return instance->m_params.wrld_rank;
	}
    
	static bool is_evl()
	{
		assert(instance != 0);
		return instance->m_params.wrld_rank % 2;
	}
    
	static bool is_root()
	{
		assert(instance != 0);
		return instance->m_params.node_rank == 0;
	}
    
	static int group_rank()
	{
		assert(instance != 0);
		return instance->m_params.node_rank;
	}
    
	static int node_load()
	{
		assert(instance != 0);
		return instance->m_params.node_load;
	}
    
	static int node_amnt()
	{
		assert(instance != 0);
		return instance->m_params.node_amnt;
	}
    
	static Socket *remote()
	{
		assert(instance != 0);
		return instance->m_params.remote;
	}
    
	static Socket *remote_from_eval()
	{
		assert(instance != 0);
		return instance->m_params.remoteFromEval;
	}
    
	static Socket *remote_from_gen()
	{
		assert(instance != 0);
		return instance->m_params.remoteFromGen;
	}
    
	
	static Socket *remote_to_third()
	{
		assert(instance != 0);
		return instance->m_params.remoteToThird;
	}
    
	static void setSockets(int num)
	{
		instance->m_params.remoteFromEval = instance->m_params.toeval[num];
		instance->m_params.remoteFromGen = instance->m_params.togen[num];
	}
    
	static void setPermu(Bytes b)
	{
		instance->m_params.permuvalue = b;
	}
    
	static Bytes getPermu()
	{
		return instance->m_params.permuvalue;
	} 
    
    
    
    
	static int saveouts()
	{
		return instance->m_params.saveouts;
	}
    
	static void setSaveouts(int value)
	{
		instance->m_params.saveouts = value;
	}
    
    
	
    
    
	virtual ~Env() {}
};


const int Env::IP_SERVER_PORT = 4096;

Env *Env::instance = 0;



int main()
{
    Bytes b(10);
   
    
    b.set_ith_bit(0,1);
    b.set_ith_bit(1,1);

    Bytes d = b.hash(80);


    printf("Hello to Android World\n");
    std::string s =  b.to_hex();
    std::cout <<" "<< s <<"\n";
    s =  d.to_hex();
    std::cout <<" "<< s <<"\n";
    
    return 1;
}
