

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
//#include <openssl/ec.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

//#include <err.h>
//#include <obj_mac.h>
#include <opensslconf.h>

#include <pbc.h>

#include "Bytes.h"
#include "Prng.h"

#include "NetIO.h"
#include "Env.h"

#include "Algebra.h"

#include <cstring>

#include <pthread.h>
#include <cmath>


#include <math.h>  

#include <jni.h>
#include <jni_sig.h>

#include <android/log.h>

#define LOGD(...) __android_log_print(ANDROID_LOG_INFO , "JNILOG", __VA_ARGS__)

// Calculates log2 of number.  
double log2( double n )  
{  
    // log(n)/log(2) is log2.  
    return log( n ) / log( 2 );  
}

/*EC_GROUP * theGroup;
BIGNUM * theZero;



typedef struct 
{
	BIGNUM * privkey;
	//BIGNUM * pubkey;
	EC_POINT * point;

}KeyGenReturn;


void  keyGen(EC_GROUP * group, BIGNUM * order, KeyGenReturn * returner)
{
	

	//const EC_POINT * gen = EC_GROUP_get0_generator(group);
	EC_POINT * temppoint = EC_POINT_new(group); //to be disgarded, only for temp/ treat like a 0
	
	returner->point = EC_POINT_new(group);
	
	BIGNUM * bg = BN_new();
	BN_rand(bg,192,-1,0);
	returner->privkey = bg;
	
	EC_POINT_mul(group,returner->point,bg,temppoint,theZero,0);
	
	EC_POINT_free(temppoint);
}*/

int node_total;

void init_network(EnvParams &params)
{
	const int PORT = params.port_base + params.node_rank;
	Bytes send, recv;
    
	//std::cout<< "starting connect\n";
    
	// get local IP
	char hostname[1024];
	gethostname(hostname, 1024);
	struct hostent *host = gethostbyname(hostname);
	const std::string local_ip = inet_ntoa(*((struct in_addr *)host->h_addr_list[0]));
    
	//std::cout << local_ip<< "\n";
    
	send.resize(sizeof(struct in_addr));
	memcpy(&send[0], host->h_addr_list[0], send.size());
    
	recv.resize(sizeof(struct in_addr)*params.gen_evl_node_amnt); // only used by node 0
    
	for(int i=0;i<params.gen_evl_node_amnt;i++)
	{
		memcpy(&recv[0]+send.size()*i, host->h_addr_list[0], send.size());
	}
    
	//to eval
	/*{
     ServerSocket ip_exchanger(params.port_base);
     Socket *sock = ip_exchanger.accept();
     sock->write_bytes(recv); // send slaves' IPs to remote
     }*/
    
    
    
	
	params.servereval.resize(params.stat_param);
	params.toeval.resize(params.stat_param);
	params.servergen.resize(params.stat_param);
	params.togen.resize(params.stat_param);
    
	for(size_t i=0;i<params.stat_param;i++)
	{
		//ServerSocket * ip_exchanger = new ServerSocket(params.port_base+1+i);
		//params.servereval[i] = new ServerSocket((params.port_base+1+i));
		//params.servergen[i] = new ServerSocket((params.thirdport_base+1+i));
		//params.toeval[i] = params.servereval[i]->accept();
	}
    //char * m;
    //m = itoa(i, m, 10);
	for(size_t i=0;i<params.stat_param;i++)
	{
		//ServerSocket * ip_exchanger = new ServerSocket(params.port_base+1+i);
		//params.servereval[i] = new ServerSocket((params.port_base+1+i));
		params.toeval[i] = new ClientSocket(params.ipserve_addr, params.port_base+1+i);//params.servereval[i]->accept();
        LOGD("e");
	}
    
    
	//to gen
	/*{
     ServerSocket ip_exchanger(params.thirdport_base);
     Socket *sock = ip_exchanger.accept();
     sock->write_bytes(recv); // send slaves' IPs to remote
     }*/
    
	for(size_t i=0;i<params.stat_param;i++)
	{
		//params.servergen[i] = new ServerSocket((params.thirdport_base+1+i));
		params.togen[i] = new ClientSocket(params.thirdipaddr, params.thirdport_base+1+i);//params.servergen[i]->accept();
        LOGD("g");
	}
}

long countout=0;
long countin=0;


Bytes readInBytesEval()
{
    Bytes b = Env::remote_from_eval()->read_bytes();
    countin += b.size()+4;
    return b;
}

Bytes readInBytesGen()
{
    Bytes b = Env::remote_from_gen()->read_bytes();
    countin += b.size()+4;
    return b;
}


#define THIRD_BEGIN 
#define THIRD_END

#define TRD_EVL_RECV()    readInBytesEval()/*Env::remote_from_eval()->read_bytes();*/
#define TRD_EVL_SEND(d)   Env::remote_from_eval()->write_bytes(d); countout+=d.size()+4
#define TRD_GEN_SEND(d)   Env::remote_from_gen()->write_bytes(d); countout+=d.size()+4
#define TRD_GEN_RECV()    readInBytesGen()/*Env::remote_from_gen()->read_bytes();*/


void init_environ(EnvParams &params)
{
	//std::cout << "here\n";
    
	if (params.secu_param % 8 != 0 || params.secu_param > 128)
	{
		std::cout << ("security parameter k needs to be a multiple of 8 less than or equal to 128\n");
		exit(1);
		//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	}
    
    LOGD("aftersec");
    
	if (params.stat_param % params.node_amnt != 0)
	{
		std::cout << ( "statistical  parameter s needs to be a multiple of cluster size\n");
		exit(1);
	}
    
    LOGD("afterstat");
    
	// # of copies of a circuit each node is responsible for
	params.node_load = params.stat_param/params.node_amnt;
    
	if (!params.circuit.load_binary(params.circuit_file))
	{
		std::cout <<("circuit parsing failed\n");
		exit(1);
	}
    
    LOGD("afterloadfile");
    
    
	Env::init(params);
    
    LOGD("afterinit(param)");
    
	Env::setSockets(0);
    LOGD("aftersets");
	Bytes bufr = TRD_EVL_RECV();
    LOGD("afterreci");
	Env::claw_free_from_bytes(bufr);
    
    LOGD("afterclawfree");
	
    
	/*// synchronize claw-free collections
     ClawFree claw_free;
     claw_free.init();
     Bytes bufr(claw_free.size_in_bytes());
     
     if (Env::is_root())
     {
     EVL_BEGIN
     bufr = claw_free.to_bytes();
     EVL_SEND(bufr);
     EVL_END
     
     GEN_BEGIN
     bufr = GEN_RECV();
     GEN_END
     }
     
     // synchronize claw-free collections to the root evaluator's
     MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
     Env::claw_free_from_bytes(bufr);*/
}




Bytes m_evl_inp;

void init_private(EnvParams &params)
{
	static byte MASK[8] = { 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F};
    
	std::ifstream private_file(params.private_file);
	std::string input;
    
	if (!private_file.is_open())
	{
		std::cout << "file open failed: " << params.private_file << "\n";
		exit(1);
	}
    
    
	
	private_file >> input;          // 1st line is the evaluator's input
	
	m_evl_inp.from_hex(input);
	m_evl_inp.resize((Env::circuit().evl_inp_cnt()+7)/8);
	m_evl_inp.back() &= MASK[Env::circuit().evl_inp_cnt()%8];
	//std::cout <<"input: "<< m_evl_inp.to_hex();
    
	//std::cout <<"inputs: " << m_evl_inp.to_hex() << "\n";
    
    
	private_file.close();
    
	// Init variables
    //	m_coms.resize(Env::node_load());
    //	m_rnds.resize(Env::node_load());
    //	m_ccts.resize(Env::node_load());
    //	m_gen_inp_masks.resize(Env::node_load());
}


int numIteration=0;







Bytes save1,save2,save3;

//G                      m_ot_g[2];
//G                      m_ot_h[2];

vector<vector<Bytes> > m_ot_keys; // ot output

vector<Bytes> outs;

int m_ot_bit_cnt;

void oblivious_transfer_with_inputs_gen_third(int lengthOfInOuts)
{
	//step_init();
    
	double start;
	uint64_t comm_sz = 0;
    
	Bytes send, recv, bufr(Env::elm_size_in_bytes()*4);
	std::vector<Bytes> bufr_chunks, recv_chunks;
    
	G  gr, hr, X[2], Y[2];
    Z r, y, a, s[2], t[2];
    
    
	
	G                      m_ot_g[2];
	G                      m_ot_h[2];
    
    
	//std::cout <<" Node load: " <<  Env::node_load() << "\n";
    
	// step 1: generating the CRS: g[0], h[0], g[1], h[1]
	if (numIteration==0)
	{
		THIRD_BEGIN
        //start = MPI_Wtime();
        //std::cout <<"segfault!\n";
        save1 = TRD_GEN_RECV();
        //std::cout <<"segfault! no?\n";
        //m_timer_com += MPI_Wtime() - start;
		THIRD_END
	}
	
	// send g[0], g[1], h[0], h[1] to slave processes
	//start = MPI_Wtime();
	//MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm);
	bufr = save1;
	//m_timer_mpi += MPI_Wtime() - start;
    
	//start = MPI_Wtime();
	bufr_chunks = bufr.split(Env::elm_size_in_bytes());
    
	m_ot_g[0].from_bytes(bufr_chunks[0]);
	m_ot_g[1].from_bytes(bufr_chunks[1]);
	m_ot_h[0].from_bytes(bufr_chunks[2]);
	m_ot_h[1].from_bytes(bufr_chunks[3]);
    
	// pre-processing
	m_ot_g[0].fast_exp();
	m_ot_g[1].fast_exp();
	m_ot_h[0].fast_exp();
	m_ot_h[1].fast_exp();
    
	// allocate memory for m_keys
	m_ot_keys.resize(1);
    
	for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
	{
		m_ot_keys[ix].reserve(m_ot_bit_cnt*2);
	}
    
    
	//std::cout << "2\n";
	// Step 2: ZKPoK of (g[0], g[1], h[0], h[1])
    
	//MPI_Barrier(m_mpi_comm);
    
	if (numIteration==0)
	{
		THIRD_BEGIN
        // receive (gr, hr)'s
        //start = MPI_Wtime();
        save2 = TRD_GEN_RECV();
        //m_timer_com += MPI_Wtime() - start;
		THIRD_END
	}
	//std::cout << "4\n";
	// Step 4: the generator computes X[0], Y[0], X[1], Y[1]
    
	THIRD_BEGIN
    // forward (gr, hr)'s to slaves
    //start = MPI_Wtime();
    bufr = save2;
    bufr.resize(m_ot_bit_cnt*2*Env::elm_size_in_bytes());
    //m_timer_gen += MPI_Wtime() - start;
    
    //start = MPI_Wtime();
    //MPI_Bcast(&bufr[0], bufr.size(), MPI_BYTE, 0, m_mpi_comm); // now every Bob has bufr
    //m_timer_mpi += MPI_Wtime() - start;
    
    
    //start = MPI_Wtime();
    bufr_chunks = bufr.split(Env::elm_size_in_bytes());
    
    //std::cout <<"size: "<<Env::elm_size_in_bytes()<<"\n";
    //m_timer_gen += MPI_Wtime() - start;
    
    //std::cout << bufr.to_hex() << "\n";
    
    for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
    {
        //start = MPI_Wtime();
        gr.from_bytes(bufr_chunks[2*bix+0]);
        hr.from_bytes(bufr_chunks[2*bix+1]);
        
        if (m_ot_keys.size() > 2)
        {
            gr.fast_exp();
            hr.fast_exp();
        }
        //m_timer_gen += MPI_Wtime() - start;
        
        for (size_t cix = 0; cix < m_ot_keys.size(); cix++)
        {
            //std::cout << cix<<"\n";
            
            //start = MPI_Wtime();
            Y[0].random(); // K[0]
            Y[1].random(); // K[1]
            
            
            m_ot_keys[cix].push_back(Y[0].to_bytes().hash(lengthOfInOuts));
            m_ot_keys[cix].push_back(Y[1].to_bytes().hash(lengthOfInOuts));
            
            s[0].random(); s[1].random();
            t[0].random(); t[1].random();
            
            // X[b] = ( g[b]^s[b] ) * ( h[b]^t[b] ), where b = 0, 1
            X[0] = m_ot_g[0]^s[0]; X[0] *= m_ot_h[0]^t[0];
            X[1] = m_ot_g[1]^s[1]; X[1] *= m_ot_h[1]^t[1];
            
            // Y[b] = ( gr^s[b] ) * ( hr^t[b] ) * K[b], where b = 0, 1
            Y[0] *= gr^s[0]; Y[0] *= hr^t[0];
            Y[1] *= gr^s[1]; Y[1] *= hr^t[1];
            
            send.clear();
            send += X[0].to_bytes(); send += X[1].to_bytes();
            send += Y[0].to_bytes(); send += Y[1].to_bytes();
            //m_timer_gen += MPI_Wtime() - start;
            
            //start = MPI_Wtime();
            TRD_GEN_SEND(send);
            //m_timer_com += MPI_Wtime() - start;
            
            //comm_sz += send.size();
        }
    }
    
    for (size_t ix = 0; ix < m_ot_keys.size(); ix++)
    {
        assert(m_ot_keys[ix].size() == m_ot_bit_cnt*2);
    }
	THIRD_END
    
}


Prng                m_prng;

void BM_OT_ext_with_third( const size_t k, const size_t l)
{
	/*if (!(Env::is_root()))
     {
     return;
     }*/
    
	Bytes                   S, R, switchBits;
	vector<Bytes>	        T;
	vector<Bytes>         &Q = T;
	vector<vector<Bytes> > X, Y;
	Bytes m_evl_inp_back;
	vector<Bytes>                   m_ot_send_pairs;
    
	
    
	// expand bit_cnt such that workload for each node is balanced
	/*uint32_t m = ((m_ot_ext_bit_cnt+Env::node_amnt()-1)/Env::node_amnt())*Env::node_amnt();
     Bytes    r = m_ot_ext_recv_bits;
     r.resize((m+7)/8);
     
     // expand the amount of seed OT such that workload for each node is balanced
     size_t new_sigma = (sigma+Env::node_amnt()-1)/Env::node_amnt(); // number of copies for each process*/
    
	Bytes send, recv, bufr;
	vector<Bytes> bufr_chunks;
	
    
    
	int m=Env::circuit().evl_inp_cnt();
    
	S = m_prng.rand(k); //should be k
	//cout << Env::circuit().evl_inp_cnt() << "\n"
	R = m_evl_inp;//m_prng.rand(m);// instead of m
	//R_in = R;
	
	//std::cout << S.to_hex() << "\n";
	//std::cout << R.to_hex() << "\n";
    
    
	if(numIteration==0)
	{
		Bytes in = m_evl_inp;
		//m_evl_inp = m_prng.rand(k);
		m_ot_bit_cnt = k*node_total;
		oblivious_transfer_with_inputs_gen_third(m);
        
		outs.resize(k*node_total*2);
		for(int i=0;i<k*node_total*2;i++)
		{
			outs[i] = m_ot_keys[0][i];
		}
        
		for(int i=0;i<k*node_total;i++)
		{
			//std::cout<< m_ot_keys[0][i*2].to_hex()<<" "<<m_ot_keys[0][i*2+1].to_hex()<<"\n";
		}
        
		m_ot_keys.clear();
		m_evl_inp = in;
	}
    
    
	THIRD_BEGIN
    T.resize(k);
    
    for (size_t jx = 0; jx < k; jx++)
    {
        T[jx] = m_prng.rand(m); //instead of m - which it should be m
    }
	THIRD_END
    
	m_ot_bit_cnt = k;
    
	THIRD_BEGIN // evaluator (OT-EXT receiver/OT sender)
    //start = MPI_Wtime();
    m_ot_send_pairs.clear();
    m_ot_send_pairs.reserve(2*k);
    for (size_t ix = 0; ix < k; ix++)
    {
        /*for(int i=0;i<32;i++)
         {
         T[ix].set_ith_bit(i,1);
         R.set_ith_bit(i,0);
         }*/
        
        Bytes q = T[ix]^R;
        
        /*if(Env::group_rank()==1)
         std::cout << T[ix].to_hex() << " " << (q).to_hex() << "\n";*/
        
        m_ot_send_pairs.push_back(T[ix]);
        m_ot_send_pairs.push_back(q);
    }
    //m_timer_evl += MPI_Wtime() - start;
	THIRD_END
    
	//backup eval's input
	m_evl_inp_back = m_evl_inp;
    
	//std::cout << "enter ot\n";
	// real OT
	//oblivious_transfer_with_inputs_gen_third(m); 
    
	m_ot_keys.resize(1);
	m_ot_keys[0].resize(k*2);
	for(int i=0;i<k*2;i++)
	{
		m_ot_keys[0][i] = outs[i+numIteration*(k*2)];
	}
    
	
	/*m_ot_keys.clear();
     numIteration++;
     oblivious_transfer_with_inputs_gen_third(m); 
     numIteration--;*/
	//std::cout << "after ot\n";
    
    
	/*if(Env::group_rank()==1)
     for(int i=0;i<m_ot_keys[0].size();i++)
     {
     GEN_BEGIN
     std::cout << m_ot_keys[0][i].to_hex()<<" "<<Env::group_rank() << "\n";
     GEN_END	
     EVL_BEGIN
     std::cout << m_ot_keys[0][i].to_hex()<<" " << m_ot_keys[0][i+1].to_hex()<< " " << Env::group_rank() <<  "\n";
     i++;
     EVL_END
     }*/
    
	THIRD_BEGIN
    vector<Bytes> xorValues;
    xorValues.resize(2*k);
    send.clear();
    
    for(int i=0;i<k;i++)
    {
        Bytes x0 = T[i]^m_ot_keys[0][i*2];
        Bytes x1 = T[i]^m_ot_keys[0][i*2+1]^R;
        
        send += x0;
        send += x1;
        
        //std::cout << x0.to_hex() << " " << x1.to_hex() << "\n";
    }
    TRD_GEN_SEND(send);
	THIRD_END
    
	int splitSize = (m+7)/8;
    
    
	//START MATRIX TRANSOFMR
    
	THIRD_BEGIN
    vector<Bytes> src = T;
    vector<Bytes> &dst = T;
	
    dst.clear();
    dst.resize(m);
    for(int i=0;i<m;i++)
    {
        dst[i].resize((k+7)/8, 0);
        for(int j=0;j<k;j++)
        {
            dst[i].set_ith_bit(j,src[j].get_ith_bit(i));
        }
    }
	
	THIRD_END
	//END MATRIX TRANSFORM
    
    
	THIRD_BEGIN
    switchBits = TRD_GEN_RECV();
    //std::cout << switchBits.to_hex()<<"\n";
	THIRD_END
	
    
    
	//send R
	THIRD_BEGIN
    send.clear();
    send.resize(1);
    send = R;
    for(int i=0;i<m;i++)
    {
        int bit = R.get_ith_bit(i);
        if(switchBits.get_ith_bit(i)==1)
            bit = 1 - bit;
        send.set_ith_bit(i,bit);
        
        /*if(Env::is_root())
         {
         std::cout <<"selecting: "<<bit<<" orig: "<<R.get_ith_bit(i)<<"  switch bit: " << switchBits.get_ith_bit(i) << " after "<< send.get_ith_bit(i)<<"\n";
         }*/
    }
    TRD_EVL_SEND(send); 
	THIRD_END
    
	//send T's
	THIRD_BEGIN
    send.clear();
    send.reserve(m);
    
    for (size_t jx = 0; jx < m; jx++)
    {
        //could also be Q
        send += T[jx];
        //send += X[jx][1] ^ (Q[jx]^S).hash(l);
    }
    //std::cout <<send.size()<<"\n";
    TRD_EVL_SEND(send);
	THIRD_END
    
	//reset input to be correct wires
	m_evl_inp = m_evl_inp_back;
}


vector<Bytes> m_coms;
vector<Bytes> hashes;

void getHashes()
{
	Bytes bufr;
    
	THIRD_BEGIN
    //start = MPI_Wtime();
    bufr = TRD_GEN_RECV();
    //m_timer_com += MPI_Wtime() - start;
    
    //start = MPI_Wtime();
    m_coms[numIteration] = bufr.split(bufr.size());
    //m_timer_evl += MPI_Wtime() - start;
	THIRD_END
}

void getHashesPartII()
{
	Bytes bufr;
    
	//std::cout << "Hashes2\n";
    
	THIRD_BEGIN
    bufr = TRD_EVL_RECV();
    
    hashes[numIteration] = bufr.split((Env::k()+7)/8);
    
	THIRD_END
}



vector<Bytes> m_gen_permubit;
void getOutputXorValues()
{
	m_gen_permubit[numIteration] = TRD_GEN_RECV();
}

vector<Bytes> m_evl_outbits;
void getOutput()
{
	m_evl_outbits[numIteration] = TRD_EVL_RECV();
}

vector<vector<Bytes> > m_gen_inp_com;
int ccccount=0;

void getCCchecks()
{
	for (size_t jx = 0; jx < m_gen_inp_com[numIteration].size(); jx++)
	{
        
        
		
		//	start = MPI_Wtime();
		m_gen_inp_com[numIteration][jx] = TRD_GEN_RECV(); //this number to third
		//TRD_GEN_SEND(m_gen_inp_com[numIteration][jx]);
		//ccccount++;
		//	m_timer_com += MPI_Wtime() - start;
		
        
		//comm_sz += bufr.size();
	}
}

std::string savedString;

int mainx(int argc, char **argv)
{
	/*Bytes b(10);
     
     
     b.set_ith_bit(0,1);
     b.set_ith_bit(1,1);
     
     Bytes d = b.hash(80);
     
     
     printf("Hello to Android World\n");
     std::string s =  b.to_hex();
     std::cout <<" "<< s <<"\n";
     s =  d.to_hex();
     std::cout <<" "<< s <<"\n";*/
    
    
	struct timeval start, end;
	struct timeval startot, endot;
    
	long mtime, seconds, useconds;    
    
	
	
    
    
	/*pairing_t 		   m_p;
     element_t                 m_r;
     element_pp_t           m_g_pp;
     
     Prng                   m_prng;*/
    
    
	/*theZero = BN_new();
     BN_zero(theZero);
     theGroup = EC_GROUP_new_by_curve_name(NID_X9_62_prime192v1);
     //printf("hi from third");
     EC_GROUP * group = EC_GROUP_new(EC_GFp_nist_method());
     BIGNUM * b = BN_new();
     BIGNUM * b2 = BN_new();
     
     BN_set_bit(b,0);
     BN_set_bit(b,2);
     
     BN_set_bit(b2,10);
     BN_set_bit(b2,12);
     
     printf("\n %d numbits\n",BN_num_bits(b));
     
     EC_POINT * point = EC_POINT_new(group);
     EC_POINT * point2 = EC_POINT_new(group);
     EC_POINT * point3 = EC_POINT_new(group);
     const EC_POINT * gen = EC_POINT_new(group);
     
     //printf("%d\n",EC_GROUP_set_generator(group,point,b,b2));
     //gen = EC_GROUP_get0_generator(group);
     //printf("%p\n",gen);
     //printf("%d\n",EC_POINT_add(group,point2,point3,point,0));
     //printf("%d\n",EC_POINT_mul(group,point,0,point2,b2,0));
     //printf("%d\n",EC_POINT_invert(group,point2,0));
     
     //EC_GROUP * ecgroup = EC_GROUP_new_by_curve_name(NID_X9_62_prime192v1);
     //printf("%p\n",ecgroup);
     
     KeyGenReturn kgr;
     keyGen(theGroup,b2,&kgr);*/
    
	if (argc < 8)
	{
		std::cout << "Usage:" << std::endl
        << "\tbetteryao [secu_param] [stat_param] [circuit_file] [input_file] [ip_server] [port_base] [setting]" << std::endl
        << std::endl
        << "[secu_param]: multiple of 8 but 128 at most" << std::endl
        << "[stat_param]: multiple of the cluster size" << std::endl
        << " [ip_server]: the IP (not domain name) of the IP exchanger" << std::endl
        << " [port_base]: for the \"IP address in use\" hassles" << std::endl
        << "   [setting]: 0 = honest-but-curious; 1 = malicious (I+2C); 2 = malicious (I+C)" << std::endl
        << std::endl;
		exit(EXIT_FAILURE);
	}
    
	
    
	EnvParams params;
    
	params.secu_param   = atoi(argv[1]);
	params.stat_param   = atoi(argv[2]);
    
	params.circuit_file = argv[3];
	params.private_file = argv[4];
	params.ipserve_addr = argv[5];
    
	params.port_base    = atoi(argv[6]);
	params.thirdipaddr = argv[8];
    
	params.thirdport_base    = atoi(argv[9]);
    
	int setting         = atoi(argv[7]);
    
	params.gen_evl_node_amnt = atoi(argv[10]);
	node_total = params.gen_evl_node_amnt;
    
	params.node_amnt = 1;
    
    LOGD("afterparam");
    
	/*G                      m_ot_g[2];
     G                      m_ot_h[2];*/
    
	//std::cout <<  "THIRD:: FINISHED PARAM\n" ;
    
	init_network(params);
    
    LOGD("afternet");
    
	gettimeofday(&start, NULL);
    
	//std::cout <<  "THIRD:: FINISHED INIT NET\n" ;
    
	init_environ(params);
    
     LOGD("afterenv");
    
	//std::cout <<  "THIRD:: FINISHED INIT ENV\n" ;
    
	init_private(params);
    
	//std::cout <<  "THIRD:: FINISHED INIT PRIVATE\n" ;
    
    LOGD("after init");
    
    
	//m_ot_bit_cnt = 16;
	gettimeofday(&startot, NULL);
	//OT
	
    
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		//std::cout << "ni "<<numIteration<<"\n";
		Env::setSockets(numIteration);
		
		
		//oblivious_transfer_with_inputs_gen_third(80);
		BM_OT_ext_with_third((int)log2(Env::circuit().evl_inp_cnt())+1,Env::k());
        
        
		m_ot_keys.clear();
        
		//if(numIteration==0)
		//{
		/*numIteration++;
         BM_OT_ext_with_third((int)log2(Env::circuit().evl_inp_cnt())+1,Env::k());
         m_ot_keys.clear();
         numIteration--;*/
		//}
        
        
        
		//BM_OT_ext_with_third((int)log2(Env::circuit().evl_inp_cnt())+1,Env::k());
		//m_ot_keys.clear();
	}
    
	gettimeofday(&endot, NULL);
    
	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;
    
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    
	printf("%s,%ld , ", argv[3], mtime);
    
    LOGD("after ot");
	
	//permu for truth tables
    
	gettimeofday(&startot, NULL);
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		Bytes permu = m_prng.rand(Env::circuit().evl_inp_cnt());
		TRD_GEN_SEND(permu);
        
		//TRD_EVL_SEND(permu);
        
        
		Bytes selection = m_evl_inp;
        
		for(int i=0;i<Env::circuit().evl_inp_cnt();i++)
		{
			int bit = selection.get_ith_bit(i);
			if(permu.get_ith_bit(i)==1)
				bit = 1 - bit;
			selection.set_ith_bit(i,bit);
		}
		TRD_EVL_SEND(selection);
	}
    
    
    
    
    
	if(Env::circuit().evl_out_cnt()!=0)
	{
        
		m_gen_permubit.resize(params.gen_evl_node_amnt);	
		for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
		{
			Env::setSockets(numIteration);
			getOutputXorValues();
		}
	}
    
    
    
    
	m_coms.resize(params.gen_evl_node_amnt);
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		getHashes();
	}
    
	vector<Bytes>          m_gen_inp_masks;
	m_gen_inp_masks.resize(params.gen_evl_node_amnt);
	m_gen_inp_com.resize(params.gen_evl_node_amnt);
    
	for (size_t ix = 0; ix < m_gen_inp_com.size(); ix++)
	{
		m_gen_inp_com[ix].resize(2*Env::circuit().gen_inp_cnt());
	}
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		getCCchecks();
	}
    
	//std::cout<<" "<<ccccount<<"\n";
    
     LOGD("begin check");
	///begin consistency check
    
	//checking to make sure both parties have the same check values for circuits
	vector<Bytes> checksGen;
	vector<Bytes> checksEvl;
	
	
	checksGen.resize(params.gen_evl_node_amnt);
	checksEvl.resize(params.gen_evl_node_amnt);
	
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		checksEvl[numIteration] = TRD_EVL_RECV();
	}
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		//std::cout <<"NI :"<<numIteration<<"\n";
		checksGen[numIteration] = TRD_GEN_RECV();
		//std::cout <<"NI :"<<numIteration<<"\n";
	}
    
	int * checks = new int[params.gen_evl_node_amnt];
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		if(checksEvl[numIteration].size() != checksGen[numIteration].size())
		{
			std::cout << "Check circuits not the same, Exiting... \n";
			exit(1);
		}
		else
		{
			if(checksEvl[numIteration].size()!=1)
				checks[numIteration] = 0;
			else
				checks[numIteration] = 1;
		}
	}
    
   
	
	/*Bytes m_chks;
     m_chks.resize(params.gen_evl_node_amnt);
     for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
     {
     Env::setSockets(numIteration);
     m_chks[numIteration] = (TRD_GEN_RECV())[0];
     }*/
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		if(checks[numIteration]){continue;}
        
		Env::setSockets(numIteration);
		//std::cout<<numIteration<<"\n";
		m_gen_inp_masks[numIteration] = TRD_GEN_RECV();
		//TRD_GEN_SEND(m_gen_inp_masks[numIteration]);
	}	
    
    
	Env::setSockets(0);
	vector<Bytes> ccheckhash;
	ccheckhash.resize(Env::circuit().gen_inp_cnt());
	for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		byte bit = m_gen_inp_masks[0].get_ith_bit(jx);
		ccheckhash[jx] = TRD_EVL_RECV();
		// check the decommitment R||M_{0,j} for M_{0,j} by checking com(M_{0,j},R)=Hash(R||M_{0,j})
		if (!(ccheckhash[jx] == m_gen_inp_com[0][2*jx+bit]))
		{
			std::cout << "Consistency check failed: ccts[0] failure\n";
			//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			exit(1);
		}
        
		// skip the first key_size_in_bytes()-byte randomness
		//bufr.insert(bufr.end(), bufr_chunks[jx].begin()+Env::key_size_in_bytes(), bufr_chunks[jx].end());
	}
    
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{	
		if(checks[numIteration]){continue;}
		Env::setSockets(numIteration);
        
		for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
		{
			//start = MPI_Wtime();
			Bytes recv2 = TRD_EVL_RECV();
			//m_timer_com += MPI_Wtime() - start;
            
			//m_comm_sz += recv.size();
            
			//start = MPI_Wtime();
			byte bit = m_gen_inp_masks[numIteration].get_ith_bit(jx);
            
			if (!(recv2 == m_gen_inp_com[numIteration][2*jx+bit]))
			{
				std::cout<< "Consistency check failed: decommitment failure!\n";
				exit(1);
				//MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			}
		}
	}	
    
	//for()
    
    //if(checks[numIteration]){continue;}
	Z m;
	G M;
	G x;
	vector<Bytes> Ms;
	Ms.resize(Env::circuit().gen_inp_cnt());
    
    
	for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		Env::setSockets(0);
		Ms[jx] = TRD_EVL_RECV();
	}
    
	
    
    vector<Bytes> bufgen;
    vector<Bytes> bufeval;
    
    bufgen.resize(Env::circuit().gen_inp_cnt()*params.gen_evl_node_amnt);
    bufeval.resize(Env::circuit().gen_inp_cnt()*params.gen_evl_node_amnt);
    
	int bufevali =0;
    int bufgeni = 0;
    
    for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		//Bytes Mtemp = TRD_EVL_RECV();
		//M.from_bytes(Ms[jx]);
		for (numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
		{
			Env::setSockets(numIteration);
			if(checks[numIteration]){continue;}
            
			bufgen[bufgeni++] = TRD_GEN_RECV();
			
			/*m.from_bytes(b);
             G value = (M*Env::clawfree().R(m));
             
             Bytes c = TRD_EVL_RECV();
             x.from_bytes(c);
             if (!(x == value))
             {
             std::cout << "Consistency check failed: claw-free failure\n";
             exit(1);
             }
             else
             {
             //std::cout <<"succeed\n";
             }*/
		}
	}
    
    for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		//Bytes Mtemp = TRD_EVL_RECV();
		//M.from_bytes(Ms[jx]);
		for (numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
		{
			Env::setSockets(numIteration);
			if(checks[numIteration]){continue;}
            
			/*Bytes b = TRD_GEN_RECV();
             
             m.from_bytes(b);
             G value = (M*Env::clawfree().R(m));*/
            
			bufeval[bufevali++] = TRD_EVL_RECV();
			/*x.from_bytes(c);
             if (!(x == value))
             {
             std::cout << "Consistency check failed: claw-free failure\n";
             exit(1);
             }
             else
             {
             //std::cout <<"succeed\n";
             }*/
		}
	}
    
    bufevali=0;
    bufgeni=0;
    
	for (size_t jx = 0; jx < Env::circuit().gen_inp_cnt(); jx++)
	{
		//Bytes Mtemp = TRD_EVL_RECV();
		M.from_bytes(Ms[jx]);
		for (numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
		{
			Env::setSockets(numIteration);
			if(checks[numIteration]){continue;}
            
			Bytes b = bufgen[bufgeni++];
			
			m.from_bytes(b);
			G value = (M*Env::clawfree().R(m));
            
			Bytes c = bufeval[bufevali++];
			x.from_bytes(c);
			if (!(x == value))
			{
				std::cout << "Consistency check failed: claw-free failure\n";
				exit(1);
			}
			else
			{
				//std::cout <<"succeed\n";
			}
		}
	}

	
    
	///end consistency check
    
    
	
	gettimeofday(&endot, NULL);
    
	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;
    
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    
	printf("%ld, ", mtime);
	gettimeofday(&startot, NULL);
    
	hashes.resize(params.gen_evl_node_amnt);
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		getHashesPartII();
        
		if(Env::circuit().evl_out_cnt()!=0)
		{
			hashes[numIteration]+=m_gen_permubit[numIteration];
			hashes[numIteration] = hashes[numIteration].hash(Env::k());
		}
	}
    
	gettimeofday(&endot, NULL);
    
	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;
    
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    
	printf("%ld, ", mtime);	
	gettimeofday(&startot, NULL);
    
    
    
    
	int verify = 1;
    
	//hashes[0] = m_prng.rand(5); //to test incorrect hashes
    
	for (size_t ix = 0; ix < hashes.size(); ix++)
	{
		verify &= (hashes[ix] == m_coms[ix]);
        
	}
    
	//std::cout <<"1\n";
    
    
    
    
    
	//std::cout <<"2\n";
    
    
    
	//std::cout <<"3\n";
	
	//checking to make sure the hashes are the same
	Bytes bufr;
    
	if (!verify)
	{
		bufr = m_prng.rand(16);
	}
	else
	{
		bufr = m_prng.rand(8);
	}
    
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		TRD_EVL_SEND(bufr);
	}	
    
	//std::cout <<"4\n";
	
	if(bufr.size()==2)
	{	
		std::cout << "hashes did not match\nExiting...\n";
		exit(1);
	}
    
	
	if(Env::circuit().evl_out_cnt()!=0)
	{
        
		/*m_gen_permubit.resize(params.gen_evl_node_amnt);	
         for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
         {
         Env::setSockets(numIteration);
         getOutputXorValues();
         }*/
        
		/*m_evl_outbits.resize(params.gen_evl_node_amnt);	
         for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
         {
         Env::setSockets(numIteration);
         //std::cout <<"o "<<numIteration<<"\n";
         getOutput();
         }
         
         Bytes ZEROS((Env::circuit().evl_out_cnt()+7)/8, 0);
         Bytes recv;
         for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
         {
         if(!checks[numIteration])
         {
         //std::cout << numIteration<< " " << (m_gen_permubit[numIteration]^m_evl_outbits[numIteration]).to_hex() <<"\n";
         }
         Bytes data = (m_gen_permubit[numIteration]^m_evl_outbits[numIteration]);
         if(checks[numIteration]) data = ZEROS;
         if(numIteration==0)
         {
         recv= data;
         }
         else
         {
         recv+= data;
         }
         }
         
         
         size_t chks_total = 0;
         for (size_t ix = 0; ix < params.gen_evl_node_amnt; ix++)
         {
         //std::cout<<"mcheck "<< (int)checks[ix]<<"\n";
         chks_total += checks[ix];
         }
         
         
         
         // find majority by locating the median of output from evaluation-circuits
         std::vector<Bytes> vec = recv.split((Env::circuit().evl_out_cnt()+7)/8);
         size_t median_ix = (chks_total+vec.size())/2;
         std::nth_element(vec.begin(), vec.begin()+median_ix, vec.end());
         
         Bytes m_evl_out = *(vec.begin()+median_ix);
         
         std::cout << "Final answer: "<<m_evl_out.to_hex()<< ", ";*/
		Bytes m_evl_out;
        
        
		//recieve result
		Env::setSockets(0);
		m_evl_out = TRD_EVL_RECV();
		//std::cout << "Final answer: "<<m_evl_out.to_hex()<< ", ";
        LOGD("answer:");
        LOGD(m_evl_out.to_hex().c_str());
        savedString=m_evl_out.to_hex();
        
        /*FILE* file = fopen("/sdcard/outputmap.txt","w");
        
        if (file != NULL)
        {
            fputs(m_evl_out.to_hex().c_str(), file);
            fflush(file);
            fclose(file);
        }
        else
        {
            LOGD("Failed");
        }
        
        LOGD("written");*/
		
        
	}
	//std::cout <<"15\n";
    
	gettimeofday(&endot, NULL);
    
	seconds  = endot.tv_sec  - startot.tv_sec;
	useconds = endot.tv_usec - startot.tv_usec;
    
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    
	printf("%ld, ", mtime);	
	
    
	
	gettimeofday(&end, NULL);
    
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
    
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

    
    params.circuit.close();
    
	printf("%ld, %ld, %ld\n", mtime, countin, countout);
    
	for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		TRD_EVL_SEND(bufr);
		TRD_GEN_SEND(bufr);
	}
    
    for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		bufr = TRD_EVL_RECV();
		bufr = TRD_GEN_RECV();
	}
	
    for(numIteration=0;numIteration<params.gen_evl_node_amnt;numIteration++)
	{
		Env::setSockets(numIteration);
		TRD_EVL_SEND(bufr);
		TRD_GEN_SEND(bufr);
	}	
}




/*JNIEXPORT void JNICALL 
Java_com_example_textshell_MainActivity_someFunction(JNIEnv * env, jobject  obj)
{
    int argc = 0;
    char * argv[] = {"hello","goodby"};
    printf("hello world\n");
    //main(argc,argv);//int xx = x;
    return;
}*/


JNIEXPORT jstring JNICALL Java_edu_uoregon_cs_MainActivity_someFunction
(JNIEnv* env, jobject)
{
    
    //printf("hi from ndk \n");
    int argc = 11;
    char * argv[] = {"programname","80","8","/data/local/circuits/dijkstra100.cir.bin.hash-free","/data/local/inpthird.txt","128.223.8.73","25000","1","128.223.8.73","18000","8"};
    for(int i=0;i<argc;i++)
        LOGD(argv[i]);
    mainx(argc,argv);
    
    m_ot_keys.clear();
    outs.clear();
    m_coms.clear();
    hashes.clear();
    m_gen_permubit.clear();
    
    m_evl_outbits.clear();
    
    m_gen_inp_com.clear();
    
    
    
    Env::destroy();
    
    return env->NewStringUTF(savedString.c_str());
}

JNIEXPORT jstring JNICALL Java_edu_uoregon_cs_MainActivity_someFunction20
(JNIEnv* env, jobject)
{
    
    //printf("hi from ndk \n");
    int argc = 11;
    char * argv[] = {"programname","80","8","/data/local/circuits/dijkstra20.cir.bin.hash-free","/data/local/inpthird.txt","128.223.8.73","25000","1","128.223.8.73","18000","8"};
    for(int i=0;i<argc;i++)
        LOGD(argv[i]);
    mainx(argc,argv);
    
    m_ot_keys.clear();
    outs.clear();
    m_coms.clear();
    hashes.clear();
    m_gen_permubit.clear();
    
    m_evl_outbits.clear();
    
    m_gen_inp_com.clear();
    
    
    
    Env::destroy();
    
    return env->NewStringUTF(savedString.c_str());
}



JNIEXPORT jstring JNICALL Java_edu_uoregon_cs_MainActivity_someFunctiondeathstar
(JNIEnv* env, jobject)
{
    
    //printf("hi from ndk \n");
    int argc = 11;
    char * argv[] = {"programname","80","32","/data/local/circuits/dijkstra100.cir.bin.hash-free","/data/local/inpthird.txt","66.193.37.224","13010","1","66.193.37.224","13050","32"};
    for(int i=0;i<argc;i++)
        LOGD(argv[i]);
    mainx(argc,argv);
    
    m_ot_keys.clear();
    outs.clear();
    m_coms.clear();
    hashes.clear();
    m_gen_permubit.clear();
    
    m_evl_outbits.clear();
    
    m_gen_inp_com.clear();
    
    
    
    Env::destroy();
    
    return env->NewStringUTF(savedString.c_str());
}

JNIEXPORT jstring JNICALL Java_edu_uoregon_cs_MainActivity_someFunction20deathstar
(JNIEnv* env, jobject)
{
    
    //printf("hi from ndk \n");
    int argc = 11;
    char * argv[] = {"programname","80","32","/data/local/circuits/dijkstra20.cir.bin.hash-free","/data/local/inpthird.txt","66.193.37.224","13010","1","66.193.37.224","13050","32"};
    for(int i=0;i<argc;i++)
        LOGD(argv[i]);
    mainx(argc,argv);
    
    m_ot_keys.clear();
    outs.clear();
    m_coms.clear();
    hashes.clear();
    m_gen_permubit.clear();
    
    m_evl_outbits.clear();
    
    m_gen_inp_com.clear();
    
    
    
    Env::destroy();
    
    return env->NewStringUTF(savedString.c_str());
}








