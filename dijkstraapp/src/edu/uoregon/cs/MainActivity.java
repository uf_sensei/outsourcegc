package edu.uoregon.cs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.Projection;
import org.mapsforge.android.maps.overlay.ArrayItemizedOverlay;
import org.mapsforge.android.maps.overlay.ArrayWayOverlay;
import org.mapsforge.android.maps.overlay.OverlayItem;
import org.mapsforge.android.maps.overlay.OverlayWay;
import org.mapsforge.core.GeoPoint;

public class MainActivity extends MapActivity {
	
	static { 
		System.load("/data/local/libgmp.so.10");
		System.load("/data/local/libpbc.so.1");
        System.load("/data/local/libmain.so");
	} 
	 
	static Random rand = new Random();
	
	public native String someFunction();  
	public native String someFunction20();  
	public native String someFunctiondeathstar();  
	public native String someFunction20deathstar(); 
	
	public void sort(ArrayList<Node> ALN)
	{
    	Collections.sort(ALN, new Comparator<Node>(){
  		  public int compare(Node s1, Node s2) {
  		    if(s1.value > s2.value)
  		    	return -1;
  		    if(s2.value > s1.value)
  		    	return 1;
  		    return 0;
  		  }
  		});
	}
    
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.om1, menu);
	    return true;
	}
	
	
	boolean selectedRegion = false;
	boolean minimized = false;
	ArrayList<Node> BackupRegion = new ArrayList<Node>();
	double dlat1 = 38.910876;//p1.getLatitude();
	double dlat2 = 38.901617;//p2.getLatitude();
	
	double dlog1 = -77.028799;//p1.getLongitude();
	double dlog2 = -77.021074;//p2.getLongitude();
	@SuppressLint("NewApi")
	public synchronized boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		
		rand.setSeed(1);
	    switch (item.getItemId()) {
	        case R.id.selectr:
	        	selectedRegion=true;
	        	minimized=false;
	        	selectedItemIndexStart = -1;
	        	selectedItemIndexEnd = -1;
	        	Projection proj =  mapView.getProjection();
	        	 
	        	Display display = getWindowManager().getDefaultDisplay();
	        	Point size = new Point();
	        	display.getSize(size);
	        	int width = size.x;
	        	int height = size.y;
	        	
	        	GeoPoint p1 = proj.fromPixels(0, 0);
	        	
	        	GeoPoint p2 = proj.fromPixels(width, height-75);
	        	
	        	
	        	 dlat1 = 38.910876;//p1.getLatitude();
	        	 dlat2 = 38.901617;//p2.getLatitude();
	        	
	        	 dlog1 = -77.028799;//p1.getLongitude();
	        	 dlog2 = -77.021074;//p2.getLongitude();
	        	
	        	System.out.println(dlat1);
	        	System.out.println(dlat2);
	        	System.out.println(dlog1);
	        	System.out.println(dlog2);
	        	
	        	if(dlat1 > dlat2)
	        	{
	        		double t = dlat2;
	        		dlat2 = dlat1;
	        		dlat1 = t;
	        	}
	        	
	        	if(dlog1 > dlog2)
	        	{
	        		double t = dlog2;
	        		dlog2 = dlog1;
	        		dlog1 = t;	        		
	        	}
	        	
	        	int tnodes =0;
	        	

               	//borderOverlay.clear();
	        	itemizedOverlay.clear();
	        	wayOverlays.clear();
	        	wayOverlaysMarked.clear();
	        	itemizedOverlayStart.clear();
	        	itemizedOverlayEnd.clear();
	        	nodesInGroup.clear();
	        	for(int i=0;i<nodes.length;i++)
	        	{
	        		if(dlat1 < nodes[i].lad && dlat2 > nodes[i].lad && dlog1 < nodes[i].log && dlog2 > nodes[i].log )
	        		{
	        			tnodes++;
	        			
	                    /*GeoPoint geoPointt = new GeoPoint(nodes[i].lad, nodes[i].log);

	                    // create an OverlayItem with title and description
	                    OverlayItem itemt = new OverlayItem(geoPointt, "", "");
	                    
	                    
	                    
	                    itemizedOverlay.addItem(itemt);*/
	                    nodesInGroup.add(nodes[i]);
	        		}
	        		else
	        		{
	        			//System.out.println(dlat1+"<"+nodes[i].log+"<"+dlat2);
	        		}
	        	}
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat1, dlog1) , new GeoPoint(dlat2, dlog1)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat1, dlog1) , new GeoPoint(dlat1, dlog2)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat2, dlog2) , new GeoPoint(dlat2, dlog1)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat2, dlog2) , new GeoPoint(dlat1, dlog2)  }}));
                
	        	
	        	//minimize
	        	
	        	

	        	
	        	System.out.println(tnodes);
	        	
	            selectedItemStart=null; selectedItemEnd=null ;
	            selectedItemIndexStart=-1;selectedItemIndexEnd=-1;
	            
	            BackupRegion = (ArrayList<Node>) nodesInGroup.clone();
	        	
	            return true;
	        case R.id.selectstart:
	        	if(!minimized || !selectedRegion)
	        	{ 
	                DialogFragment dialog = new MessageDialogFragment("Please select region and minimize nodes first");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	                return true; 
	        	}  
	        	inStart=true; 
 
	            return true; 
	        case R.id.selectend:
	        	if(!minimized || !selectedRegion)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please select region and minimize nodes first");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	                return true;
	        	}
	        	inEnd=true;

	            return true;
	        case R.id.findnode:
	        	
	        	if(!minimized || !selectedRegion)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please select region and minimize nodes first");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	                return true;
	        	}
	        	inFind=true;
	        	return true;
	        case R.id.minimizeto100:
	        	minimized=true;
	        	itemizedOverlay.clear();
	        	wayOverlays.clear();
	        	//wayOverlaysMarked.clear();
	        	itemizedOverlayStart.clear();
	        	itemizedOverlayEnd.clear();
	        	nodesInGroup = (ArrayList<Node>) BackupRegion.clone();
	        	wayOverlaysMarked.clear();
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat1, dlog1) , new GeoPoint(dlat2, dlog1)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat1, dlog1) , new GeoPoint(dlat1, dlog2)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat2, dlog2) , new GeoPoint(dlat2, dlog1)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat2, dlog2) , new GeoPoint(dlat1, dlog2)  }}));

	        	
	        	//ArrayList<Node> alcopy = (ArrayList<Node>) nodesInGroup.clone();
	        	//ArrayList<Node> listb = new ArrayList<Node>();
	        	
	        	Node [] alcopy = new Node[nodesInGroup.size()];
	        	Node [] listb = new Node[100];
	        	int listbsize = 0;
	        	int alcopysize = nodesInGroup.size();
	        	

	        	
	        	
	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		alcopy[i]=nodesInGroup.get(i);
	        	}
	        	
	        	for(int i=0;i<alcopysize;i++)
	        	{
	        		
	        		alcopy[i].value = alcopy[i].backvalue;
	        	}
	        	
	        	int wi=listbsize;
	        	double distance = 100;
	        	String [] names = new String[10];
	        	
	        	for(int c=0;c<50;c++)
	        	{
	        	outest:
	        	while(wi<100 & alcopysize>0)
	        	{
	        	
	        	
	        		Arrays.sort(alcopy,0,alcopysize);
	        		
	        		//Iterator<Node> it = alcopy.iterator();
	        		boolean added=false;
	        		out:
	        		for (int j=0;j<alcopysize;j++)
	        		{
	        			Node n = alcopy[j];
	        			
	        			boolean found = false;
	        			//Iterator<Node> itb = listb.iterator();
	        			
	        			//while(itb.hasNext())
	        			for(int k=0;k<listbsize;k++)
	        			{
	        				Node n2 = listb[k];//itb.next();
	        				
	        				if(distance(n,n2)<distance)
	        				{
	        					continue out;
	        				}
	        			}
	        			
	        			if(!found)
	        			{
	        				added=true;
	        				wi++;
	        				listb[listbsize] =(n);
	        				listbsize++;	
	        				
	        				
	        				for(int k=j+1;k<alcopysize;k++)//alcopy.remove(n);
	        				{
	        					alcopy[k-1]=alcopy[k];
	        				}
	        				
	        				alcopysize--;
	        				continue outest;
	        			}
	        		}
	        		
	        		if(!added)
	        		{
	        			distance = distance / 2;
	        		}
	        	}
	        	
	        	//nodesInGroup = (ArrayList<Node>) nodesInGroup.subList(0, 100);
	        	
	        	
	        	
	        	/*for(int i=0;i<100 & i < nodesInGroup.size();i++)
	        	{
	        		listb.add(nodesInGroup.get(i));
	        	}*/
	        	
	        	boolean hasRemoval=true;
	        	
	        	//while(hasRemoval)
	        	{
	        		nextnode:
	        		for(int i=0;i<listbsize;i++)
	        		{
	        			Node n = listb[i];
	        			
	        			
	        			int nlen=0;
	        			
	        			String [] nodea = listb[i].names;
	        			for(int j=0;j<listbsize;j++)
	        			{
	        				if(i==j)
	        					continue;
	        				
	        				String[] nodeb = listb[j].names;
	        				
	        				for(int i2=0;i2<nodea.length;i2++)
	        				{
	        					for(int i3=0;i3<nodeb.length;i3++)
	        					{
	        						if(nodea[i2].equals(nodeb[i3]))
	        						{
	        							boolean in=false;
	        							
	        							for(int i4=0;i4<nlen;i4++)
	        								if(names[i4].equals(nodea[i2]))
	        									in=true;
	        							
	        							if(!in)
	        							{
		        							names[nlen++]=nodea[i2];
		        							
		        							if(nlen==9)
		        							{
		        								continue nextnode;
		        							}
	        							}
	        						}
	        					}
	        				}
	        				
	        			}
	        			
	        			if(nlen<2)
	        			{
	        				alcopy[alcopysize++]=n;
	        				
	        				for(int k=i+1;k<listbsize;k++)//alcopy.remove(n);
	        				{
	        					listb[k-1]=listb[k];
	        				}
	        				listbsize--;
	        				
	        				
	        				i--;
	        			}
	        		}
	        	}
	        	System.out.println("midnodes:"+listbsize);
	        	for(int i=0;i<alcopysize;i++)
	        	{
	        		
	        		alcopy[i].value = rand.nextInt(50);
	        	}
	        	wi=listbsize;
	        	}
	        	
	        	int numr = 3;
	        	
	        	outestloop:
	        	while(numr>0)
	        	{
	        		distance = 100;
	        		while(distance > .5)
	        		{
	        			
	        			if(listbsize==100)
	        				break outestloop;
	        			
		        		nextnode:
		        		for(int i=0;i<alcopysize;i++)
		        		{
		        			Node n = alcopy[i];
		        			
		        			
		        			int nlen=0;
		        			
		        			String [] nodea = alcopy[i].names;
		        			checkloop:
		        			for(int j=0;j<listbsize;j++)
		        			{
		        				if(!(distance(listb[j],n)>distance))
		        				{
		        					continue checkloop;
		        				}
		        				//System.out.println(distance +" "+distance(listb[j],n));
		        				
		        				if(i==j)
		        					continue;
		        				
		        				String[] nodeb = listb[j].names;
		        				
		        				for(int i2=0;i2<nodea.length;i2++)
		        				{
		        					for(int i3=0;i3<nodeb.length;i3++)
		        					{
		        						if(nodea[i2].equals(nodeb[i3]))
		        						{
		        							boolean in=false;
		        							
		        							for(int i4=0;i4<nlen;i4++)
		        								if(names[i4].equals(nodea[i2]))
		        									in=true;
		        							
		        							if(!in)
		        							{
			        							names[nlen++]=nodea[i2];
			        							
			        							if(nlen==9)
			        							{
			        								break checkloop;
			        							}
		        							}
		        						}
		        					}
		        				}
		        				
		        			}
	
		        			
		        			if(nlen>numr)
		        			{
		        				for(int k=i+1;k<alcopysize;k++)//alcopy.remove(n);
		        				{
		        					alcopy[k-1]=alcopy[k];
		        				}
		        				
		        				i--;
		        				listb[listbsize++]=n;
		        			}
		        			
		        			if(listbsize==100)
		        				break outestloop;
		        		}
		        	
		        		distance = distance/1.5;
		        		//System.out.println(distance);	        		
		        	}	
	        			
	        		System.out.println("R: "+numr);
	        		numr--;
	        	}
	        	
	        	System.out.println("endingnodes:"+listbsize);
	        	
	        	nodesInGroup.clear();
	        	
	        	for(int i=0;i<listbsize;i++)
	        	{
	        		if(!nodesInGroup.contains(listb[i])) nodesInGroup.add(listb[i]);
	        	}
	        	
	        	System.out.println("nodesInGroup:"+listbsize);
	        	
	        	System.out.println("*****start find node*****");
	        	
	        	//for(int i=0;)

	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		nodesInGroup.get(i).gconnum=0;
	        	}
	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		
	        		//if(nodesInGroup.get(i).index==8815)
	        			findConnections(nodesInGroup.get(i));
	        		
	        	}
	        	System.out.println("*****end find node*****");
	        	
	        	
	        	genMap(100);
	        	 
	        	
	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	                GeoPoint geoPointt = new GeoPoint(nodesInGroup.get(i).lad, nodesInGroup.get(i).log);
	                
	                 
	
	                // create an OverlayItem with title and description
	                OverlayItem itemt = new OverlayItem(geoPointt, "", "");
	                
	                nodesInGroup.get(i).p =itemt;
	                
	                itemizedOverlay.addItem(itemt);	        
	                
	                nodesInGroup.get(i).transativeConnections();
	        	}	        	
	        	
	            return true;
	            
	        case R.id.minimizeto20:
	        	minimized=true;
	        	itemizedOverlay.clear();
	        	wayOverlays.clear();
	        	//wayOverlaysMarked.clear();
	        	itemizedOverlayStart.clear();
	        	itemizedOverlayEnd.clear();
	        	nodesInGroup = (ArrayList<Node>) BackupRegion.clone();
	        	wayOverlaysMarked.clear();
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat1, dlog1) , new GeoPoint(dlat2, dlog1)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat1, dlog1) , new GeoPoint(dlat1, dlog2)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat2, dlog2) , new GeoPoint(dlat2, dlog1)  }}));
	        	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(dlat2, dlog2) , new GeoPoint(dlat1, dlog2)  }}));

	        	
	        	//ArrayList<Node> alcopy = (ArrayList<Node>) nodesInGroup.clone();
	        	//ArrayList<Node> listb = new ArrayList<Node>();
	        	
	        	alcopy = new Node[nodesInGroup.size()];
	        	listb = new Node[20];
	        	listbsize = 0;
	        	alcopysize = nodesInGroup.size();
	        	

	        	
	        	
	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		alcopy[i]=nodesInGroup.get(i);
	        	}
	        	
	        	for(int i=0;i<alcopysize;i++)
	        	{
	        		
	        		alcopy[i].value = alcopy[i].backvalue;
	        	}
	        	
	        	wi=listbsize;
	        	distance = 100;
	        	names = new String[10];
	        	
	        	doneiterating:
	        	for(int c=0;c<50;c++)
	        	{
	        	outest:
	        	while(wi<20 & alcopysize>0)
	        	{
	        	
	        	
	        		Arrays.sort(alcopy,0,alcopysize);
	        		
	        		//Iterator<Node> it = alcopy.iterator();
	        		boolean added=false;
	        		out:
	        		for (int j=0;j<alcopysize;j++)
	        		{
	        			Node n = alcopy[j];
	        			
	        			boolean found = false;
	        			//Iterator<Node> itb = listb.iterator();
	        			
	        			//while(itb.hasNext())
	        			for(int k=0;k<listbsize;k++)
	        			{
	        				Node n2 = listb[k];//itb.next();
	        				
	        				if(distance(n,n2)<distance)
	        				{
	        					continue out;
	        				}
	        			}
	        			
	        			if(!found)
	        			{
	        				added=true;
	        				wi++;
	        				listb[listbsize] =(n);
	        				listbsize++;	
	        				
	        				
	        				/*for(int k=j+1;k<alcopysize;k++)//alcopy.remove(n);
	        				{
	        					alcopy[k-1]=alcopy[k];
	        				}*/
	        				alcopy[j] = alcopy[alcopysize-1];
	        				alcopy[alcopysize-1]=null;
	        				
	        				alcopysize--;
	        				continue outest;
	        			}
	        		}
	        		
	        		if(!added)
	        		{
	        			distance = distance / 2;
	        		}
	        	}
	        	
	        	//nodesInGroup = (ArrayList<Node>) nodesInGroup.subList(0, 100);
	        	
	        	System.out.println("emidnodes:"+listbsize);
	        	if(c==49)
	        		break doneiterating;
	        	
	        	/*for(int i=0;i<100 & i < nodesInGroup.size();i++)
	        	{
	        		listb.add(nodesInGroup.get(i));
	        	}*/
	        	//checkDuplications(listb);
	        	//checkDuplications(alcopy);
	        	boolean hasRemoval=true;
	        	
	        	//while(hasRemoval)
	        	{
	        		nextnode:
	        		for(int i=0;i<listbsize;i++)
	        		{
	        			Node n = listb[i];
	        			
	        			
	        			int nlen=0;
	        			
	        			String [] nodea = listb[i].names;
	        			for(int j=0;j<listbsize;j++)
	        			{
	        				if(i==j)
	        					continue;
	        				
	        				String[] nodeb = listb[j].names;
	        				
	        				for(int i2=0;i2<nodea.length;i2++)
	        				{
	        					for(int i3=0;i3<nodeb.length;i3++)
	        					{
	        						if(nodea[i2].equals(nodeb[i3]))
	        						{
	        							boolean in=false;
	        							
	        							for(int i4=0;i4<nlen;i4++)
	        								if(names[i4].equals(nodea[i2]))
	        									in=true;
	        							
	        							if(!in)
	        							{
		        							names[nlen++]=nodea[i2];
		        							
		        							if(nlen==9)
		        							{
		        								continue nextnode;
		        							}
	        							}
	        						}
	        					}
	        				}
	        				
	        			}
	        			
	        			if(nlen<2)
	        			{
	        				alcopy[alcopysize++]=n;
	        				
	        				/*for(int k=i+1;k<listbsize;k++)//alcopy.remove(n);
	        				{
	        					listb[k-1]=listb[k];
	        				}*/
	        				listb[i]=listb[listbsize-1];
	        				listb[listbsize-1]=null;
	        				listbsize--;
	        				
	        				
	        				i--;
	        			}
	        		}
	        	}
	        	System.out.println("midnodes:"+listbsize);
	        	for(int i=0;i<alcopysize;i++)
	        	{
	        		
	        		alcopy[i].value = rand.nextInt(50);
	        	}
	        	wi=listbsize;
	        	}
	        	
	        	numr = 3;
	        	
	        	outestloop:
	        	while(numr>0)
	        	{
	        		distance = 100;
	        		while(distance > .5)
	        		{
	        			
	        			if(listbsize==20)
	        				break outestloop;
	        			
		        		nextnode:
		        		for(int i=0;i<alcopysize;i++)
		        		{
		        			Node n = alcopy[i];
		        			
		        			
		        			int nlen=0;
		        			
		        			String [] nodea = alcopy[i].names;
		        			checkloop:
		        			for(int j=0;j<listbsize;j++)
		        			{
		        				if(!(distance(listb[j],n)>distance))
		        				{
		        					continue checkloop;
		        				}
		        				//System.out.println(distance +" "+distance(listb[j],n));
		        				
		        				if(i==j)
		        					continue;
		        				
		        				String[] nodeb = listb[j].names;
		        				
		        				for(int i2=0;i2<nodea.length;i2++)
		        				{
		        					for(int i3=0;i3<nodeb.length;i3++)
		        					{
		        						if(nodea[i2].equals(nodeb[i3]))
		        						{
		        							boolean in=false;
		        							
		        							for(int i4=0;i4<nlen;i4++)
		        								if(names[i4].equals(nodea[i2]))
		        									in=true;
		        							
		        							if(!in)
		        							{
			        							names[nlen++]=nodea[i2];
			        							
			        							if(nlen==9)
			        							{
			        								break checkloop;
			        							}
		        							}
		        						}
		        					}
		        				}
		        				
		        			}
	
		        			
		        			if(nlen>numr)
		        			{
		        				/*for(int k=i+1;k<alcopysize;k++)//alcopy.remove(n);
		        				{
		        					alcopy[k-1]=alcopy[k];
		        				}*/
		        				alcopy[i] = alcopy[alcopysize-1];
		        				alcopy[alcopysize-1]=null;
		        				alcopysize--;
		        				
		        				i--;
		        				listb[listbsize++]=n;
		        			}
		        			
		        			if(listbsize==20)
		        				break outestloop;
		        		}
		        	
		        		distance = distance/1.5;
		        		//System.out.println(distance);	        		
		        	}	
	        			
	        		System.out.println("R: "+numr);
	        		numr--;
	        	}
	        	
	        	System.out.println("endingnodes:"+listbsize);
	        	
	        	nodesInGroup.clear();
	        	
	        	for(int i=0;i<listbsize;i++)
	        	{
	        		if(!nodesInGroup.contains(listb[i])) nodesInGroup.add(listb[i]);
	        	}
	        	
	        	System.out.println("nodesInGroup: "+nodesInGroup.size());
	        	
	        	System.out.println("*****start find node*****");
	        	
	        	//for(int i=0;)

	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		nodesInGroup.get(i).gconnum=0;
	        	}
	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		
	        		//if(nodesInGroup.get(i).index==8815)
	        			findConnections(nodesInGroup.get(i));
	        		
	        	}
	        	System.out.println("*****end find node*****");
				for(int i=0;i<nodesInGroup.size();i++)
				{
					Node n = nodesInGroup.get(i);

					for(int k=0;k<n.gconnum;k++)
					{
						//output.writeInt(n.connsList[k].size());
						System.out.println(n.index+" to "+n.gconn[k]);
						for(int j=0;j<n.connsList[k].size();j++)
						{
							//output.writeInt(n.connsList[k].get(j).index);
							System.out.print(" "+n.connsList[k].get(j).index);
						}
						System.out.println();
					}
					//System.out.println("send: "+n.gconnum+" "+i);
				}	
	        	
	        	genMap(20);
	        	 
	        	
	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	                GeoPoint geoPointt = new GeoPoint(nodesInGroup.get(i).lad, nodesInGroup.get(i).log);
	                
	                 
	
	                // create an OverlayItem with title and description
	                OverlayItem itemt = new OverlayItem(geoPointt, "", "");
	                
	                nodesInGroup.get(i).p =itemt;
	                
	                itemizedOverlay.addItem(itemt);	        
	                
	                nodesInGroup.get(i).transativeConnections();
	        	}	        	
	        	
	            return true;
	        case R.id.execute:
	        	//if not start and end selected
	        	
	        	if(selectedRegion==false)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please select a region first");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");	        
	                
	                return true;
	        	}
	        	
	        	if(minimized == false)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please minimize nodes first");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");	 
	                
	                return true;
	        	}
	        	
	        	
	        	if(selectedItemIndexStart==-1 || selectedItemIndexEnd == -1)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please Select start and end Nodes");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	                
	        		return true;
	        	}
	        	
	        	try
	        	{
	        		
	        		if(MAPSIZE==100)
	        			new RetreiveFeedTask().execute();
	        		if(MAPSIZE==20)
	        			new RetreiveFeedTask20().execute();
	        		
	        	}
	        	catch(Exception e)
	        	{
	        		e.printStackTrace();
	        	}
	        	
	        	
	        	
	        	
	        	return true;
	        case R.id.executegal:
	        	//if not start and end selected
	        	
	        	if(selectedRegion==false)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please select a region first");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");	        
	                
	                return true;
	        	}
	        	
	        	if(minimized == false)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please minimize nodes first");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");	 
	                
	                return true;
	        	}
	        	
	        	
	        	if(selectedItemIndexStart==-1 || selectedItemIndexEnd == -1)
	        	{
	                DialogFragment dialog = new MessageDialogFragment("Please Select start and end Nodes");
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	                
	        		return true;
	        	}
	        	
	        	try
	        	{
	        		
	        		if(MAPSIZE==100)
	        			new RetreiveFeedTaskDeathstar().execute();
	        		if(MAPSIZE==20)
	        			new RetreiveFeedTask20Deathstar().execute();
	        		
	        	}
	        	catch(Exception e)
	        	{
	        		e.printStackTrace();
	        	}
	        	
	        	
	        	
	        	
	        	return true;
	        case R.id.exit:
	        	System.exit(1);
	        default:
	            return super.onOptionsItemSelected(item);
	        	
	    }
	}
	
	
	@SuppressLint("NewApi")
	public class MessageDialogFragment extends DialogFragment {
		String msg;
		public MessageDialogFragment(String message)
		{
			super();
			msg = message;
		}
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage(msg)
	               .setPositiveButton("Back", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // FIRE ZE MISSILES!
	                   }
	               });
	              /* .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog
	                   }
	               });*/
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
	}
	
	
	int MAPSIZE=0;
	class RetreiveFeedTask extends AsyncTask<String, Void, String> {

	    private Exception exception;

	    @SuppressLint("NewApi")
		protected String doInBackground(String... urls) {
	        try {
	        	
	        	
        		PrintWriter out = new PrintWriter(new FileWriter("/data/local/inpthird.txt")); 
        		String s =convertToHex(selectedItemIndexStart,2)+convertToHex(selectedItemIndexEnd,2);
        		
        		s = addEncryption(s);
        		
        		out.println(s); 
        		
        		
        		
        		System.out.println("input: "+s);
        		//out.println("world"); 
        		out.close(); 

                Socket skt = new Socket("128.223.8.73", 13001);
                //BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
                DataOutputStream output = new DataOutputStream(skt.getOutputStream());
                String data = gmap;//"datasend";
                
                

                
                // ****** for testnet.java ****** 
                //output.writeUTF(data);
               
                //**** for testnet2.java *****
                send(100,output);
                
                output.close();
                skt.close(); 
                
                //System.out.println("forward");
                wayOverlaysMarked.clear();
                String res = someFunction();
                //System.out.println("res: "+someFunction()); 
                res = decrypt(res);
                for(int i=0;i<res.length()-1;i+=2)
                {
                	int start = twoDigitHexToInt(res.substring(i,i+2));
                	int end = twoDigitHexToInt(res.substring(i+2,i+4));
                	
                	if (end == 255)
                		break;
                	
                	//System.out.println(start+" "+end);
                	
                	Node nstart = nodesInGroup.get(start);
                	Node nend = nodesInGroup.get(end);
                	
                   	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(nstart.lad, nstart.log) , new GeoPoint(nend.lad, nend.log)  }}));
                    
                }
                //wayOverlays.clear();
                
                /*BufferedReader in = new BufferedReader(new FileReader("output.txt")); 
                String text = in.readLine(); 
                System.out.println(text);
                in.close();*/
                
                DialogFragment dialog = new MessageDialogFragment("Execution Complete");
                dialog.show(getFragmentManager(), "NoticeDialogFragment");
                
                
	            return "success";
	        } catch (Exception e) {
	            this.exception = e;
	            e.printStackTrace();
	            return null; 
	        }
	    }
 
	    protected void onPostExecute(String feed) {
	        // TODO: check this.exception 
	        // TODO: do something with the feed
	    }
	 }
	
	class RetreiveFeedTask20 extends AsyncTask<String, Void, String> {

	    private Exception exception;

	    @SuppressLint("NewApi")
		protected String doInBackground(String... urls) {
	        try {
	        	System.out.println("20 start");
	        	
	        	
        		PrintWriter out = new PrintWriter(new FileWriter("/data/local/inpthird.txt")); 
        		String s =convertToHex(selectedItemIndexStart,2)+convertToHex(selectedItemIndexEnd,2);
        		
        		s = addEncryption(s);
        		out.println(s); 
        		System.out.println("input: "+s);
        		//out.println("world"); 
        		out.close(); 

                Socket skt = new Socket("128.223.8.73", 13001);
                //BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
                DataOutputStream output = new DataOutputStream(skt.getOutputStream());
                String data = gmap;//"datasend";
                
                

                
                // ****** for testnet.java ****** 
                //output.writeUTF(data);
               
                //**** for testnet2.java *****
                send(20,output);
                
                output.close();
                skt.close(); 
                
                //System.out.println("forward");
                wayOverlaysMarked.clear();
                String res = someFunction20();
                
                res = decrypt(res);
                //System.out.println("res: "+someFunction()); 
                
                for(int i=0;i<res.length()-1;i+=2)
                {
                	int start = twoDigitHexToInt(res.substring(i,i+2));
                	int end = twoDigitHexToInt(res.substring(i+2,i+4));
                	
                	if (end == 255)
                		break;
                	
                	//System.out.println(start+" "+end);
                	
                	Node nstart = nodesInGroup.get(start);
                	Node nend = nodesInGroup.get(end);
                	
                   	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(nstart.lad, nstart.log) , new GeoPoint(nend.lad, nend.log)  }}));
                    
                }
                //wayOverlays.clear();
                
                /*BufferedReader in = new BufferedReader(new FileReader("output.txt")); 
                String text = in.readLine(); 
                System.out.println(text);
                in.close();*/
                
                DialogFragment dialog = new MessageDialogFragment("Execution Complete");
                dialog.show(getFragmentManager(), "NoticeDialogFragment");
                
                
	            return "success";
	        } catch (Exception e) {
	            this.exception = e;
	            e.printStackTrace();
                DialogFragment dialog = new MessageDialogFragment("Cannot Connect to Server");
                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	            return null; 
	        }
	    }
	    
	    
 
	    protected void onPostExecute(String feed) {
	        // TODO: check this.exception 
	        // TODO: do something with the feed
	    }
	 }
	
	class RetreiveFeedTaskDeathstar extends AsyncTask<String, Void, String> {

	    private Exception exception;

	    @SuppressLint("NewApi")
		protected String doInBackground(String... urls) {
	        try {
	        	
	        	
        		PrintWriter out = new PrintWriter(new FileWriter("/data/local/inpthird.txt")); 
        		String s =convertToHex(selectedItemIndexStart,2)+convertToHex(selectedItemIndexEnd,2);
        		s = addEncryption(s);
        		
        		out.println(s); 
        		System.out.println("input: "+s);
        		//out.println("world"); 
        		out.close(); 

                Socket skt = new Socket("66.193.37.224", 13001);
                //BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
                DataOutputStream output = new DataOutputStream(skt.getOutputStream());
                String data = gmap;//"datasend";
                
                

                
                // ****** for testnet.java ****** 
                //output.writeUTF(data);
               
                //**** for testnet2.java *****
                send(100,output);
                
                output.close();
                skt.close(); 
                
                //System.out.println("forward");
                wayOverlaysMarked.clear();
                String res = someFunctiondeathstar();
                //System.out.println("res: "+someFunction()); 
                res = decrypt(res);
                for(int i=0;i<res.length()-1;i+=2)
                {
                	int start = twoDigitHexToInt(res.substring(i,i+2));
                	int end = twoDigitHexToInt(res.substring(i+2,i+4));
                	
                	if (end == 255)
                		break;
                	
                	//System.out.println(start+" "+end);
                	
                	Node nstart = nodesInGroup.get(start);
                	Node nend = nodesInGroup.get(end);
                	
                   	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(nstart.lad, nstart.log) , new GeoPoint(nend.lad, nend.log)  }}));
                    
                }
                //wayOverlays.clear();
                
                /*BufferedReader in = new BufferedReader(new FileReader("output.txt")); 
                String text = in.readLine(); 
                System.out.println(text);
                in.close();*/
                
                DialogFragment dialog = new MessageDialogFragment("Execution Complete");
                dialog.show(getFragmentManager(), "NoticeDialogFragment");
                
                
	            return "success";
	        } catch (Exception e) {
	            this.exception = e;
	            e.printStackTrace();
	            return null; 
	        }
	    }
 
	    protected void onPostExecute(String feed) {
	        // TODO: check this.exception 
	        // TODO: do something with the feed
	    }
	 }
	
	class RetreiveFeedTask20Deathstar extends AsyncTask<String, Void, String> {

	    private Exception exception;

	    @SuppressLint("NewApi")
		protected String doInBackground(String... urls) {
	        try {
	        	System.out.println("20 start");
	        	
	        	
        		PrintWriter out = new PrintWriter(new FileWriter("/data/local/inpthird.txt")); 
        		String s =convertToHex(selectedItemIndexStart,2)+convertToHex(selectedItemIndexEnd,2);
        		s = addEncryption(s);
        		out.println(s); 
        		System.out.println("input: "+s);
        		//out.println("world"); 
        		out.close(); 

                Socket skt = new Socket("66.193.37.224", 13001);
                //BufferedReader in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
                DataOutputStream output = new DataOutputStream(skt.getOutputStream());
                String data = gmap;//"datasend";
                
                

                
                // ****** for testnet.java ****** 
                //output.writeUTF(data);
               
                //**** for testnet2.java *****
                send(20,output);
                
                output.close();
                skt.close(); 
                
                //System.out.println("forward");
                wayOverlaysMarked.clear();
                String res = someFunction20deathstar();
                //System.out.println("res: "+someFunction()); 
                res = decrypt(res);
                for(int i=0;i<res.length()-1;i+=2)
                {
                	int start = twoDigitHexToInt(res.substring(i,i+2));
                	int end = twoDigitHexToInt(res.substring(i+2,i+4));
                	
                	if (end == 255)
                		break;
                	
                	//System.out.println(start+" "+end);
                	
                	Node nstart = nodesInGroup.get(start);
                	Node nend = nodesInGroup.get(end);
                	
                   	wayOverlaysMarked.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(nstart.lad, nstart.log) , new GeoPoint(nend.lad, nend.log)  }}));
                    
                }
                //wayOverlays.clear();
                
                /*BufferedReader in = new BufferedReader(new FileReader("output.txt")); 
                String text = in.readLine(); 
                System.out.println(text);
                in.close();*/
                
                DialogFragment dialog = new MessageDialogFragment("Execution Complete");
                dialog.show(getFragmentManager(), "NoticeDialogFragment");
                
                
	            return "success";
	        } catch (Exception e) {
	            this.exception = e;
	            e.printStackTrace();
                DialogFragment dialog = new MessageDialogFragment("Cannot Connect to Server");
                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	            return null; 
	        }
	    }
	    
	    
 
	    protected void onPostExecute(String feed) {
	        // TODO: check this.exception 
	        // TODO: do something with the feed
	    }
	 }
    
	
	public int twoDigitHexToInt(String hex)
	{
		return new BigInteger(hex,16).intValue();
	}
	
	
	void findConnections(Node n)
	{
		//Node temp=n;
		
		nextNode:
		for(int i=0;i<n.connections;i++)
		{
			if(n.gconnum>=4)
			{
				break;
			}
			 
			lastDistance = 0;
			String common= getCommonName(n,nodes[n.Nodes[i]],i);
			//System.out.println(common);
			if(common!=null)
			{
		    	//if(!common.equals( "7th_Street_Northwest"))
		    	//	 continue;
				
				//System.out.println("fnding: "+common+"\nAt node:"+n);
				//System.out.println("traversed to: "+nodes[n.Nodes[i]]);
				
				//if(n.connsList[n.gconnum]==null)
				{
					n.connsList[n.gconnum] = new ArrayList<Node>();
				}
				
				n.connsList[n.gconnum].clear();
				
				n.connsList[n.gconnum].add(n);
				n.connsList[n.gconnum].add(nodes[n.Nodes[i]]);
				
				
				Node connection = traverse(common,nodes[n.Nodes[i]],n,n);
				

				
				if(connection==null)
					continue;
				
				
				/*int pstart = n.connsList[n.gconnum].get(0).index;
				int pend = n.connsList[n.gconnum].get(n.connsList[n.gconnum].size()-1).index;
				
				if(!((pstart == n.index && pend == connection.index) || (pstart == connection.index && pend == n.index)))
				{
					System.out.println("***errorr***");
					System.out.println(pstart+" "+pend+"  !=   " + n.index+" "+connection.index);
				}*/
				
				
				if(connection.gconnum < 4 && n.gconnum < 4)
				{
				
					for(int ii=0;ii<n.gconnum;ii++)
						if(connection.index==n.gconn[ii])
							continue nextNode;
					
					for(int ii=0;ii<connection.gconnum;ii++)
						if(n.index==connection.gconn[ii])
							continue nextNode;
					
					{	
						if(n==connection)
							continue nextNode;
						
						
						
						//System.out.println("b: "+n.gconnum+" "+connection.gconnum);
						
						//n.testsame();
						//System.out.println("before******* \n"+n+"and\n"+connection);
						//System.out.println(n.index+" "+connection.index+" "+lastDistance);
						//System.out.println(pstart+" "+pend+"  !=   " + n.index+" "+connection.index);
						
						n.glen[n.gconnum] = lastDistance;
						n.gconn[n.gconnum++] = connection.index;
						
						connection.glen[connection.gconnum] = lastDistance;
						connection.connsList[connection.gconnum] = n.connsList[n.gconnum-1];
						connection.gconn[connection.gconnum++] = n.index;
						
						
						/*System.out.println(pstart+" "+pend+"  !=   " + n.index+" "+connection.index);*/
						
						//n.testsame();
						//System.out.println(" after connectioning******* \n"+n+"and\n"+connection+"\n******");
						
		            	wayOverlays.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(n.lad, n.log) , new GeoPoint(connection.lad, connection.log)  }}));
						
	            		//System.out.println("a: "+n.gconnum+" "+connection.gconnum);
					}
				}
			}
		}

	}
	
	String getCommonName(Node n1, Node n2, int nodeindex)
	{
		
		String [] s1 = n1.names;
		String [] s2 = n2.names;
		
		for(int i=0;i<s1.length;i++)
		{
			for(int j=0;j<s2.length;j++)
			{
				if(s1[i].equals(s2[j]))
				{
					lastDistance = n1.lengths[nodeindex];	
					//System.out.println("l: "+n1.lengths[nodeindex]+" "+n1.index+" "+n2.index+ " "+i);
					return s1[i];
				}
			}
		}
		
		return null;
	}

	double lastDistance;
	//traverse for a given name until a node of that name is found in the nodesInGroup, returning then node it links too
    Node traverse(String name,Node n, Node lastNode, Node origin)
    {
    	
    	
    	/*if(!name.equals( "7th_Street_Northwest"))
    		return null;*/

    	boolean traverse=false;
    	
    	int depth=0;
    	//lastDistance = 
    	//Node n;
		if(nodesInGroup.contains(n))
		{
			//System.out.println("return");
			return n;
		}
    	
    	for(int i=0;i<nodes[n.index].connections;i++)
    	{
    		
    		if(nodes[nodes[n.index].Nodes[i]]!=lastNode)
    		{
    			
	    		if(containsStreet(nodes[nodes[n.index].Nodes[i]],name))
	    		{
	    			if(nodesInGroup.contains(nodes[nodes[n.index].Nodes[i]]))
	    			{
	    				
	    				if(origin.connsList[origin.gconnum].contains(nodes[nodes[n.index].Nodes[i]]))
	    				{
	    					return null;
	    				}
	    				else
	    				{
	    					origin.connsList[origin.gconnum].add(nodes[nodes[n.index].Nodes[i]]);
	    				}
	    				
	    				lastDistance+=nodes[n.index].lengths[i];
	    				
	    				//System.out.println("return");
	    				//if(traverse)
	    				//	System.out.println("TraversedSUCCSUSSSUSUSUSUUSUSUSUSU");
	    				return nodes[nodes[n.index].Nodes[i]];
	    			}
	    			else
	    			{
	    				
	    				
	    				lastNode = n;
	    				lastDistance+=nodes[n.index].lengths[i];
	    				n=nodes[nodes[n.index].Nodes[i]];
	    				
	    				if(origin.connsList[origin.gconnum].contains(n))
	    				{
	    					return null;
	    				}
	    				else
	    				{
	    					origin.connsList[origin.gconnum].add(n);
	    				}
	    				
	    				i=0;
	    				//System.out.println("at: "+n +"\nlastnode: "+lastNode );
	    				depth++;
	    				traverse=true;
	    				if(depth==100)
	    					return null;
	    			}
	    		}
	    		else
	    		{
	    			//System.out.println("checking..."+nodes[nodes[n.index].Nodes[i]]);
	    			/*System.out.println("Not traverse"+nodes[nodes[n.index].Nodes[i]]);
	    			System.out.println("nte: ");*/
	    		}
    		}
    	}
    	
    	
    	return null;
    }
    
    boolean containsStreet(Node n, String street)
    {
    	for(int i=0;i<n.names.length;i++)
    	{
    		//System.out.println(n.names[i]);
    		if(n.names[i].equals(street))
    		{
    			//System.out.println("contains: "+street+"\n"+n);
    			return true;
    		}
    	}
    	
    	
    	
    	return false;
    }
    
    /*public boolean onTouch()
    {
    	
    }*/
    
    boolean inStart,inEnd,inFind;
    double touched_x;
    double touched_y;
    //@Override
    @SuppressLint("NewApi")
	public boolean onTouchEvent(MotionEvent event) {
     // TODO Auto-generated method stub
      
     touched_x = event.getX();
     touched_y = event.getY();
     boolean touched;
      
     //System.out.println("t");
     
     
     int action = event.getAction();
     switch(action){
     case MotionEvent.ACTION_DOWN:
    	 if(nodesInGroup.size()>0 & (inStart | inEnd | inFind))
    	 {
    		 	Projection proj =  mapView.getProjection();
    		 
	        	GeoPoint p = proj.fromPixels((int)touched_x, (int)touched_y);
	        	
	        	//GeoPoint p2 = proj.fromPixels(width, height);
	        	
	        	
	        	double dlat = p.getLatitude();
	        	//double dlat2 = p2.getLatitude();
	        	
	        	double dlog = p.getLongitude();
	        	//double dlog2 = p2.getLongitude();
	        	
	        	int indexclosest=-1;
	        	double distance=999;
	        	
	        	Node newNode = new Node();
	        	newNode.lad = dlat;
	        	newNode.log = dlog;
	        	
	        	for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		if(distance > distance(nodesInGroup.get(i),newNode))
	        		{
	        			distance = distance(nodesInGroup.get(i),newNode);
	        			indexclosest = i;
	        		}
	        	}
	        	
	        	

	        	/*for(int i=0;i<nodesInGroup.size();i++)
	        	{
	        		itemizedOverlay.removeItem(nodesInGroup.get(i).p);
	        	}*/
	        	
	        	if(inStart)
	        	{
	        		if(selectedItemStart!=null)
		        	{
		        		itemizedOverlay.addItem(selectedItemStart);
		        		itemizedOverlayStart.clear();
		        	}
		        	
		        	Node n = nodesInGroup.get(indexclosest);
		        	itemizedOverlay.removeItem(n.p);
		        	itemizedOverlayStart.addItem(n.p);
		        	selectedItemStart = n.p;
		        	selectedItemIndexStart = indexclosest;
		        	
		        	inStart = false;
	        	}
	        	if(inEnd)
	        	{
	        		if(selectedItemEnd!=null)
		        	{
		        		itemizedOverlay.addItem(selectedItemEnd);
		        		itemizedOverlayEnd.clear();
		        	}
		        	
		        	Node n = nodesInGroup.get(indexclosest);
		        	itemizedOverlay.removeItem(n.p);
		        	itemizedOverlayEnd.addItem(n.p);
		        	selectedItemEnd = n.p;
		        	selectedItemIndexEnd = indexclosest;
		        	
		        	inEnd=false;
	        	}
	        	if(inFind)
	        	{
	        		System.out.println(nodesInGroup.get(indexclosest).index);
	        		
	        		inFind=false;
	                DialogFragment dialog = new MessageDialogFragment("Node ID: "+nodesInGroup.get(indexclosest).index);
	                dialog.show(getFragmentManager(), "NoticeDialogFragment");
	        	}
	        	
    	 }
    	 
      touched = true;
      break;
     case MotionEvent.ACTION_MOVE:
      touched = true;
      break;
     case MotionEvent.ACTION_UP:
      touched = false;
      break;
     case MotionEvent.ACTION_CANCEL:
      touched = false;
      break;
     case MotionEvent.ACTION_OUTSIDE:
      touched = false;
      break;
     default:
     }
     return super.onTouchEvent(event); //processed
    }
    
    OverlayItem selectedItemStart,selectedItemEnd;
    int selectedItemIndexStart=-1,selectedItemIndexEnd=-1;
    
    
    
    protected void onStop()
    {
    	System.out.println("on stop exiting");
    	System.exit(1);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapView = new MapView(this);
        mapView.setClickable(true);
        mapView.setBuiltInZoomControls(true);
        mapView.setMapFile(new File("/sdcard/dc.map"));
        
        mapView.setOnTouchListener(new OnTouchListener()
        {



			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				//System.out.println("touch");
				onTouchEvent(arg1);
				return false;
			}
       });
        
        itemizedOverlay=null;
        itemizedOverlayStart=null;
        itemizedOverlayEnd=null;
        wayOverlays=null;
        wayOverlaysMarked=null;
        
        // R.drawable.marker is just a placeholder for your own drawable
        Drawable defaultMarker = getResources().getDrawable(R.drawable.b1);
        itemizedOverlay = new ArrayItemizedOverlay(getResources().getDrawable(R.drawable.b1));
        itemizedOverlayStart = new ArrayItemizedOverlay(getResources().getDrawable(R.drawable.b2));
        itemizedOverlayEnd = new ArrayItemizedOverlay(getResources().getDrawable(R.drawable.b3));
        //Paint wayfill = new Paint();
        
        
        Paint wayoutline = new Paint();
        wayoutline.setARGB(255, 0, 0, 0);
        
		Paint paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStroke.setStyle(Paint.Style.STROKE);
		paintStroke.setColor(Color.BLACK);
		paintStroke.setAlpha(128);
		paintStroke.setStrokeWidth(7);
		paintStroke.setPathEffect(new DashPathEffect(new float[] { 25, 15 }, 0));

        wayOverlays = new ArrayWayOverlay(paintStroke,wayoutline);
        
        
        Paint wayoutlinemarked = new Paint();
        //wayoutlinemarked.setARGB(255, 0, 0, 0);
        wayoutlinemarked.setColor(Color.argb(255, 100, 0, 100));
        wayoutlinemarked.setAlpha(255);
        wayoutlinemarked.setStrokeWidth(5);
        
		Paint paintStrokemarked = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStrokemarked.setStyle(Paint.Style.STROKE);
		paintStrokemarked.setColor(Color.argb(255, 255, 0, 255));
		paintStrokemarked.setAlpha(255);
		paintStrokemarked.setStrokeWidth(20);
		paintStrokemarked.setPathEffect(new DashPathEffect(new float[] { 10, 10,25,2,16,10,20,2 }, 0));

        wayOverlaysMarked = new ArrayWayOverlay(paintStrokemarked,wayoutlinemarked);
        
        /*Paint wayoutlineBord = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintStroke.setStyle(Paint.Style.STROKE);
        wayoutline.setARGB(255, 0, 0, 0);
		paintStroke.setColor(Color.GREEN);
		paintStroke.setAlpha(255);
		paintStroke.setStrokeWidth(10);
        
		Paint paintStrokeBord = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStroke.setStyle(Paint.Style.STROKE);
		paintStroke.setColor(Color.YELLOW);
		paintStroke.setAlpha(255);
		paintStroke.setStrokeWidth(30);
		paintStroke.setPathEffect(new DashPathEffect(new float[] { 50, 4 }, 0));
        
        borderOverlay = new ArrayWayOverlay(paintStrokeBord,wayoutlineBord);*/

        // create a GeoPoint with the latitude and longitude coordinates

        
        
        //int i=0;
        /*try
        {
        File f = new File("/data/local/intersections.txt");
        Scanner scan = new Scanner(f);
        
        while(scan.hasNext())
        {
        	String s1 = scan.next();
        	String s2 = scan.next();
        	double d1 = Double.parseDouble(s1)-0.000009;
        	double d2 = Double.parseDouble(s2);
        	
            // create a GeoPoint with the latitude and longitude coordinates
            GeoPoint geoPointt = new GeoPoint(d1, d2);

            // create an OverlayItem with title and description
            OverlayItem itemt = new OverlayItem(geoPointt, "", "");
            itemizedOverlay.addItem(itemt);
            //System.out.println(i++);
        }
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }*/
        
        try
        {
        //File f = new File("/data/local/nodes.txt");
        InputStream is = new FileInputStream("/data/local/nodes.txt");
        XScanner scan = new XScanner(is);
        int totalnodes = scan.nextInt();
        System.out.println("totalnodes: "+totalnodes);
        nodes = new Node[totalnodes];
        while(scan.hasNext())
        {
        	
        	int index = Integer.parseInt(scan.next());
        	nodes[index]=new Node();
        	
        	String s1 = scan.next();
        	String s2 = scan.next();
        	double d1 = Double.parseDouble(s1)-0.000009;
        	double d2 = Double.parseDouble(s2);
        	
        	nodes[index].value = Integer.parseInt(scan.next());
        	nodes[index].backvalue = nodes[index].value;
        	nodes[index].log = d2;
        	nodes[index].lad = d1;
        	
        	int amtnodes = Integer.parseInt(scan.next());
        	nodes[index].connections = amtnodes;
        	nodes[index].lengths = new double[amtnodes];
        	nodes[index].Nodes = new int[amtnodes];
        	nodes[index].index = index;
        	
        	for(int j=0;j<amtnodes;j++)
        	{
        		int n = Integer.parseInt(scan.next());
            	String s = scan.next();
            	double d = Double.parseDouble(s);
            	nodes[index].lengths[j]=d;
            	nodes[index].Nodes[j]=n;
        	}
        	
        	nodes[index].names = new String[scan.nextInt()];
        	
        	for(int j=0;j<nodes[index].names.length;j++)
        	{
        		nodes[index].names[j] = scan.next();
        	}
        	
        	
        	
            // create a GeoPoint with the latitude and longitude coordinates
            GeoPoint geoPointt = new GeoPoint(d1, d2);

            // create an OverlayItem with title and description
            OverlayItem itemt = new OverlayItem(geoPointt, "", "");
            //itemizedOverlay.addItem(itemt);
            //System.out.println(i++);
        }
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }

        // add the OverlayItem to the ArrayItemizedOverlay
        
        for(int j=0;j<nodes.length;j++)
        {
            /*GeoPoint geoPointt = new GeoPoint(nodes[j].lad, nodes[j].log);

            // create an OverlayItem with title and description
            OverlayItem itemt = new OverlayItem(geoPointt, "", "");
            
            
            
            itemizedOverlay.addItem(itemt);*/
            
            /*for(int i=0;i<nodes[j].connections;i++)
            {
            	wayOverlays.addWay(new OverlayWay(new GeoPoint[][] {{ new GeoPoint(nodes[nodes[j].Nodes[i]].lad, nodes[nodes[j].Nodes[i]].log) , new GeoPoint(nodes[j].lad, nodes[j].log)  }}));
            }*/
        }
        /*for(int j=100;j<200;j++)
        {
            GeoPoint geoPointt = new GeoPoint(nodes[j].lad, nodes[j].log);

            // create an OverlayItem with title and description
            OverlayItem itemt = new OverlayItem(geoPointt, "", "");
            
            
            
            itemizedOverlay2.addItem(itemt);
        }*/  
    
        // add the ArrayItemizedOverlay to the MapView
        //mapView.getOverlays().add(borderOverlay);
        mapView.getOverlays().add(wayOverlays);
        mapView.getOverlays().add(wayOverlaysMarked);
        mapView.getOverlays().add(itemizedOverlay);  
        mapView.getOverlays().add(itemizedOverlayStart);
        mapView.getOverlays().add(itemizedOverlayEnd);
        //mapView.getOverlays().add(itemizedOverlay2);
        
        //mapView.
        
        setContentView(mapView);
    }
    
    
    HashMap<Integer,Integer> redirmap = new HashMap<Integer,Integer>();
    
    String gmap;
    //requires nodesInGroup to be full
    void genMap(int maxnum)
    {
    	MAPSIZE=maxnum;
    	String result="";
    	
    	redirmap.clear();
    	
    	
    	for(int i=0;i<maxnum;i++)
    	{
    		if(i<nodesInGroup.size())
    		{
    			redirmap.put(Integer.valueOf(nodesInGroup.get(i).index),Integer.valueOf(i));
    		}
    		else
    		{
    			
    		}
    	}
    	
    	
    	//find max distance
    	double maxdist=0;
    	for(int i=0;i<nodesInGroup.size();i++)
    	{
    		for(int k=0;k<nodesInGroup.get(i).gconnum;k++)
    		{
    			if(maxdist < nodesInGroup.get(i).glen[k])
    			{
    				maxdist = nodesInGroup.get(i).glen[k];
    			}
    		}
    	}
    	
    	
    	//scale to 100
    	double scalefactor;
    	
    	scalefactor = 100/maxdist;
    	System.out.println("scale: "+scalefactor + " md: "+maxdist);
    	
    	for(int i=0;i<maxnum;i++)
    	{
    		if(nodesInGroup.size()>i)
    		{
    			for(int k=0;k<4;k++)
    			{
    				if(nodesInGroup.get(i).gconnum > k)
    				{
    					int tempsize = (int) (nodesInGroup.get(i).glen[k] * scalefactor);
    					System.out.println(tempsize+" "+nodesInGroup.get(i).glen[k]+ " "+nodesInGroup.get(i).index + " "+nodesInGroup.get(i).gconn[k] +" "+ vectString(nodesInGroup.get(i).connsList[k]));
    					//System.out.println(tempsize+" "+scalefactor+" "+nodesInGroup.get(i).glen[k]);
    					result+=convertToHex(tempsize,4);
    				}
    				else
    				{
    					result+="FFFF";
    				}
    			}
    		}
    		else
    		{
    			result+="AAAABBBBCCCCDDDD";
    		}
    	}
    	
    	for(int i=0;i<maxnum;i++)
    	{
    		if(nodesInGroup.size()>i)
    		{
    			for(int k=0;k<4;k++)
    			{
    				if(nodesInGroup.get(i).gconnum > k)
    				{
    					int tempsize = redirmap.get(Integer.valueOf((nodesInGroup.get(i).gconn[k] )));
    					
    					result+=convertToHex(tempsize,2);
    				}
    				else
    				{
    					result+="FF";
    				}
    			}
    			System.out.println(convertToHex(i,2)+" "+nodesInGroup.get(i).index);
    			for(int j=0;j<nodesInGroup.get(i).names.length;j++)
    			{
    				System.out.println(nodesInGroup.get(i).names[j]);
    			}
    			System.out.println();
    		}
    		else
    		{
    			result+="CCDDEEFF";
    		}
    	}
    	
    	System.out.println(result);
    	gmap=result;
    }
    
    String vectString(ArrayList<Node> n)
    {
    	String tr;
    	
    	tr="";
    	
    	for(int i=0;i<n.size();i++)
    		tr+=n.get(i).index+" ";
    	
    	return tr;
    }
    
    String convertToHex(int num, int hexNums)
    {
    	//System.out.println(num);
    	
    	String result="";
    	
    	String hexVal=(new BigInteger(""+num)).toString(16);
    	
    	if(hexVal.length()%2 == 1)
    	{
    		hexVal="0"+hexVal;
    	}
    	
    	
    	for(int i=0;i<hexVal.length();i+=2)
    	{
    		result+=""+hexVal.charAt(i)+""+hexVal.charAt(i+1);
    	}
    	
    	for(int i=hexVal.length();i<hexNums;i++)
    	{
    		result=result+"0";
    	}
    	
    	return result;
    	
    }
    
	/*private static Polyline createNodeLine(int index1, int index2) {
		List<GeoPoint> geoPoints = Arrays.asList(new Integer(index1), new Integer(index2));
		PolygonalChain polygonalChain = new PolygonalChain(geoPoints);

		Paint paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintStroke.setStyle(Paint.Style.STROKE);
		paintStroke.setColor(Color.MAGENTA);
		paintStroke.setAlpha(128);
		paintStroke.setStrokeWidth(7);
		paintStroke.setPathEffect(new DashPathEffect(new float[] { 25, 15 }, 0));

		return new Polyline(polygonalChain, paintStroke);
	}*/
    
    HashMap<Long,Node> map = new HashMap<Long,Node>();
    Node[] nodes;
    MapView mapView;
    ArrayItemizedOverlay itemizedOverlay,itemizedOverlayStart,itemizedOverlayEnd;
    ArrayList<Node> nodesInGroup = new ArrayList<Node>();
    ArrayWayOverlay wayOverlays, wayOverlaysMarked, borderOverlay;
    
    //http://www.movable-type.co.uk/scripts/latlong.html
    public static double distance(Node n1, Node n2)
    {
        double R = 6371; // km
        double dLat = Math.toRadians((n1.getLatitude()-n2.getLatitude()));
        double dLon = Math.toRadians((n1.getLongitude()-n2.getLongitude()));
        double lat1 = Math.toRadians(n1.getLatitude());
        double lat2 = Math.toRadians(n2.getLatitude());
        
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        double d = R * c;
        
        
        //System.out.println(d);
        return d;
    }
    
    class Node implements Comparable
    {
    	int value;
    	int backvalue;
    	double log,lad;
    	double[] lengths;
    	int connections;
    	int[] Nodes;
    	String [] names;
    	
    	int [] gconn = new int[4];
    	double [] glen = new double[4];
    	ArrayList<Node> [] connsList = new ArrayList[4];
    	int gconnum=0;
    	
    	int index;
    	
    	OverlayItem p;
    	
    	double getLatitude(){return lad;}
    	double getLongitude(){return log;}
		@Override
		public int compareTo(Object arg0)
		{
			Node s2 = (Node)arg0;
			
	  		 // public int compare(Node s1, Node s2) {
	    		    if(value > s2.value)
	    		    	return -1;
	    		    if(s2.value > value)
	    		    	return 1;
	    		    return 0;
	    		  //}
			
			
			//return 0;
		}
		
		public String toString()
		{
			String toReturn=gconnum+" "+index+" "+super.toString()+"\n";
			for(int i=0;i<names.length;i++)
			{
				toReturn+="\n"+names[i];
			}
			for(int i=0;i<gconnum;i++)
			{
				toReturn+="\n"+gconn[i];
			}
			
			return toReturn;
		}
		
		public void transativeConnections()
		{
			int mynum = index;//redirmap.get(Integer.valueOf(index));
			
			nextNode:
			for(int i=0;i<gconnum;i++)
			{
				Node n = nodes[gconn[i]];
				
				
				for(int j=0;j<n.gconnum;j++)
				{
					if(n.gconn[j]==mynum)
						continue nextNode;
				}
				
				System.out.println(n+"\n****does not contain****\n"+this+"\n*\n");
				
				
			}
			
			nextNode:
			for(int i=0;i<gconnum;i++)
			{
				for(int j=i+1;j<gconnum;j++)
				{
					if(gconn[i]==gconn[j])
						System.out.println("same: "+i+" "+j);
				}
			}
			

		}
		
		public void testsame()
		{
			nextNode:
				for(int i=0;i<gconnum;i++)
				{
					for(int j=i+1;j<gconnum;j++)
					{
						if(gconn[i]==gconn[j])
							System.out.println("same: "+i+" "+j);
					}
				}				
		}
		

		
    }
	public void send(int num, DataOutputStream output )
	{
		try
		{
			if(redirmap.size()!=nodesInGroup.size())
				System.out.println("sizes of redirmap and nodesingroup do not match");
			
			output.writeInt(num);
			output.writeInt(nodesInGroup.size());
			//System.out.println(num);
			//System.out.println(nodesInGroup.size());
			
			//ArrayList<Node> recievedNodes = new ArrayList<Node>();
			
			for(int i=0;i<nodesInGroup.size();i++)
			{
				Node n = nodesInGroup.get(i);
				output.writeInt(n.gconnum);
				output.writeInt(n.index);
				//n.gconn = new int[n.gconnum];
				//n.glen = new double[n.gconnum];
				for(int k=0;k<n.gconnum;k++)
				{
					output.writeInt(n.gconn[k]);
					output.writeDouble(n.glen[k]);
					
					/*output.writeInt(n.connsList[k].size());
					for(int j=0;j<n.connsList[k].size();j++)
					{
						output.writeInt(n.connsList[k].get(j).index);
					}*/
				} 
				System.out.println("send: "+n.gconnum+" "+i);
			}
			for(int i=0;i<nodesInGroup.size();i++)
			{
				Node n = nodesInGroup.get(i);

				for(int k=0;k<n.gconnum;k++)
				{
					output.writeInt(n.connsList[k].size());
					System.out.println(n.index+" to "+n.gconn[k]);
					for(int j=0;j<n.connsList[k].size();j++)
					{
						output.writeInt(n.connsList[k].get(j).index);
						System.out.print(" "+n.connsList[k].get(j).index);
					}
					System.out.println();
				}
				//System.out.println("send: "+n.gconnum+" "+i);
			}			
			
			//HashMap<Integer,Integer> remapping = new HashMap<Integer,Integer>();
			
	    	redirmap.clear();
	    	
	    	
	    	for(int i=0;i<nodesInGroup.size();i++)
	    	{
	    		if(i<nodesInGroup.size())
	    		{
	    			redirmap.put(Integer.valueOf(nodesInGroup.get(i).index),Integer.valueOf(i));
	    		}
	    		else
	    		{
	    			
	    		}
	    	}
			
			for(int i=0;i<nodesInGroup.size();i++)
			{
				//System.out.println(redirmap.get(Integer.valueOf(i)));
				
				int x = redirmap.get(Integer.valueOf(nodesInGroup.get(i).index)).intValue();
				
				//int groupnum = input.readInt();
				output.writeInt(nodesInGroup.get(i).index);
				output.writeInt(x);
				//int nodeid = input.readInt();
				//remapping.put(Integer.valueOf(groupnum),Integer.valueOf(nodeid));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void recieve(DataInputStream input)
	{
		try
		{
			int amount = input.readInt();
			int ingroupamount = input.readInt();
			//double scale = input.readDouble();
			
			
			ArrayList<Node> recievedNodes = new ArrayList<Node>();
			
			for(int i=0;i<ingroupamount;i++)
			{
				Node n = new Node();
				n.gconnum = input.readInt();
				n.gconn = new int[n.gconnum];
				n.glen = new double[n.gconnum];
				for(int k=0;k<n.gconnum;k++)
				{
					n.gconn[k] = input.readInt();
					n.glen[k] = input.readDouble();
					
					int size = input.readInt();
					n.connsList[k] = new ArrayList<Node>();
					for(int j=0;j<size;j++)
					{
						n.connsList[k].add(nodes[input.readInt()]);
					}
				}
			}
			
			for(int i=0;i<ingroupamount;i++)
			{
			
			}
			
			
			HashMap<Integer,Integer> remapping = new HashMap<Integer,Integer>();
			
			for(int i=0;i<ingroupamount;i++)
			{
				int groupnum = input.readInt();
				int nodeid = input.readInt();
				remapping.put(Integer.valueOf(groupnum),Integer.valueOf(nodeid));
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}		
	}
	
	void checkDuplications(Node [] array)
	{
		for(int i=0;i<array.length;i++)
		{
			for(int j=0;j<array.length;j++)
			{
				if(j==i)
					continue;
				
				if(array[i]==array[j] && array[i]!=null)
				{
					printStack();
					return;
				}
			}
		}
		
	}
	void printStack()
	{
		try
		{
			throw new Exception();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	BigInteger decr;
	
	String addEncryption(String s)
	{
		BigInteger x = new BigInteger(MAPSIZE*8,rand);
		decr= x;
		s = s+x.toString(16);
		
		System.out.println("encrypt: "+s);
		return s;
	}
	
	String decrypt(String s)
	{
		BigInteger q = new BigInteger(s,16);
		q = q.xor(decr);
		
		String res = q.toString(16);
		
		if(res.length()%8!=0)
		{
			res="0"+res;
		}
		
		System.out.println("decrpyt: "+res);
		return ""+res;
	}
}

