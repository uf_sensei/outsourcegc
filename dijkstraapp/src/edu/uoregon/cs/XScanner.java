package edu.uoregon.cs;

import java.io.InputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

public class XScanner
{
	StringTokenizer ts;
	
	String lastString;
	
	boolean hitCond = false,inIf=false;
	
	
	public XScanner(InputStream is)
	{
		String s = new Scanner(is).useDelimiter("\\A").next();
		
		ts = new StringTokenizer(s);
	}
	
	public String next()
	{
		lastString = ts.nextToken();
		
		return lastString;
	}
	
	public int nextInt()
	{
		lastString = ts.nextToken();
		
		return Integer.parseInt( lastString);
	}	
	
	public float nextFloat()
	{
		lastString = ts.nextToken();
		
		return Float.parseFloat( lastString);
	}
	
	public long nextLong()
	{
		lastString = ts.nextToken();
		
		return Long.parseLong( lastString);		
	}
	public double nextDouble()
	{
		lastString = ts.nextToken();
		
		return Double.parseDouble( lastString);		
	}	
	
	
	
	public void skiptoEnd()
	{
		while(!lastString.equals("$ENDIF$")) lastString = ts.nextToken();
		hitCond=false;
	}
	
	public void skipIfElseEnd()
	{
		while(!lastString.equals("$ENDIF$")&!lastString.equals("$IF$")&!lastString.equals("$ELSE$")) lastString = ts.nextToken();
		
		if(lastString.equals("$ENDIF$"))
			hitCond=false;
	}
	
	
	
	public boolean hasNext()
	{
		return ts.hasMoreElements();
	}
}