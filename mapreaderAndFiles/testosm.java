import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.*;
import org.openstreetmap.osmosis.core.task.v0_6.*;
import org.openstreetmap.osmosis.xml.common.CompressionMethod;
import org.openstreetmap.osmosis.xml.v0_6.XmlReader;
import java.io.*;
import java.util.*;

public class testosm
{
    static int c=0;
    static Map<Long,Node> map = new HashMap<Long,Node>(1000000);
    //static HashMap<Long,WayNode> map = new HashMap<Long,Node>();
    static Vector<Way> ways = new Vector<Way>();


    public static void main(String[] args) throws Exception
    {


        File file = new File("district-of-columbia.osm.pbf"); // the input file

        Sink sinkImplementation = new Sink() 
        {
                
            public void process(EntityContainer entityContainer) 
            {
                Entity entity = entityContainer.getEntity();
                if (entity instanceof Node) 
                {
                    map.put(new Long(entity.getId()),(Node)entity);
                } 
                else if (entity instanceof Way)
                {

                    Collection<Tag> ls = entity.getTags();
                    Tag[] tags = (Tag[]) ls.toArray(new Tag[0]);
                    boolean found = false;
                    boolean name = false;
                    for(int i=0;i<ls.size();i++)
                    {
                        if(tags[i].getKey().equals("highway"))
                        {


                        }	
                        if(tags[i].getKey().equals("highway") && !( tags[i].getValue().equals("footway")) && !( tags[i].getValue().equals("cycleway"))  && !( tags[i].getValue().equals("service"))  && !( tags[i].getValue().equals("path"))  && !( tags[i].getValue().equals("steps"))    )
                        {
                            found=true;
                        }
                        if(tags[i].getKey().equals("name"))
                            name=true;

                    }

                    if(found)
                    {
                        for(int i=0;i<ls.size();i++)
                        {
                                if(tags[i].getKey().equals("name"))
                                {
                                    //System.out.println(tags[i]); 
                                    
                                    /*if(tags[i].getValue().equals("Pennsylvania Avenue Northwest") || tags[i].getValue().equals("10th St NW") )
                                    {
                                        Way x = (Way)entity;
                                        List<WayNode> wnx = x.getWayNodes();
                                        Iterator itx = wnx.iterator();
                                        
                                        System.out.println();
                                        while(itx.hasNext())
                                        {
                                            WayNode nx = (WayNode) itx.next();
                                            Node c = map.get( new Long(nx.getNodeId()));
                                            System.out.println(tags[i].getValue()+" "+c.getLatitude() +" "+c.getLongitude());
                                            
                                        }
                                    }*/
                                    
                                    
                                    /*Way x = (Way)entity;
                                    List<WayNode> wnx = x.getWayNodes();
                                    if(wnx.size()==2)
                                    {
                                        System.out.println(tags[i].getValue());
                                    }*/
                                    
                                }	
                            
                                
                        }
                        
                        
                        ways.add(((Way)entity));

                        testosm.c++;
                        //System.out.println();
                    }
                
                
                    //do something with the way
                } 
                else if (entity instanceof Relation) 
                {
                //System.out.println("relation");

                    //do something with the relation
                }
                else
                {
                    System.out.println(entity);
                }
            }
            public void release() { }
            public void complete() { }
            public void initialize(java.util.Map<String,Object> map) {}
        };
        
        

        boolean pbf = false;
        CompressionMethod compression = CompressionMethod.None;

        if (file.getName().endsWith(".pbf")) 
        {
            pbf = true;
        } 
        else if (file.getName().endsWith(".gz")) 
        {
            compression = CompressionMethod.GZip;
        } 
        else if (file.getName().endsWith(".bz2")) 
        {
            compression = CompressionMethod.BZip2;
        }

        RunnableSource reader;

        if (pbf) 
        {
            reader = new crosby.binary.osmosis.OsmosisReader(
                    new FileInputStream(file));
        } 
        else 
        {
            reader = new XmlReader(file, false, compression);
        }

        reader.setSink(sinkImplementation);

        Thread readerThread = new Thread(reader);
        readerThread.start();

        while (readerThread.isAlive()) 
        {
            try 
            {
                readerThread.join();
            } 
            catch (InterruptedException e) 
            {
                /* do nothing */
            }
        }

        System.out.println("map size: "+map.size());
        System.out.println("Done parsing");
        //System.out.println(c);
        
        int cc=0;


        HashMap<Long,Intersection> inter = new HashMap<Long,Intersection>(10000);
        Vector<NWay> newways = new Vector<NWay>();

        for(int i=0;i<ways.size();i++)
        {
            Way x = ways.get(i);
            List<WayNode> wnx = x.getWayNodes();
            
            NWay currentnway = new NWay(x);
            newways.add(currentnway);
            

            //for(int k=0;k<wnx.size();k++)
            int k=0;
            Iterator itx = wnx.iterator();
            while(itx.hasNext())
            {
                
                WayNode nx = (WayNode) itx.next();

                //lid = nx.getNodeId();
                    
                for(int j=0;j<ways.size();j++)
                {
                    if(i==j)
                        continue;
                    
                    Way y = ways.get(j);
                    List<WayNode> wny = y.getWayNodes();
                    
                    

                    Iterator ity = wny.iterator();
                    //for(int l=0;l<wny.size();l++)
                    while(ity.hasNext())
                    {
                        WayNode ny = (WayNode) ity.next();
                        if(nx.compareTo(ny)==0 && x != y )
                        {
                            //System.out.println("same "+ x +" "+y);
                            Node c = map.get( new Long(nx.getNodeId()));
                            
                            
                            
                            if(inter.get(new Long(nx.getNodeId()))==null)
                            {
                                inter.put(new Long(nx.getNodeId()),new Intersection(c.getLatitude(),c.getLongitude(),x,y,nx.getNodeId(),i,j));
                            }
                            else
                            {
                                Intersection ii = inter.get(new Long(nx.getNodeId()));
                                ii.addWay(x,i);
                            }
                            
                            Intersection sect=inter.get(new Long(nx.getNodeId()));
                            //System.out.println(sect);
                            currentnway.addIntersection(sect,k);
                            
                        }
                    }
                   
                }
                k++;
            }
        }
        
        
        WayNode [] nodes1 = new WayNode[1];
        WayNode [] nodes2 = new WayNode[2];
        
        System.out.println("finished initial nodes\nstarting extra nodes");
        int found =0;
        /*for(int i=0;i<ways.size()/100;i++)
        {
            Way x = ways.get(i);
            List<WayNode> wnx = x.getWayNodes();
            
            NWay currentnway = newways.get(i);//new NWay(x)
            
            //NWay currentnway = new NWay(x);
            //newways.add(currentnway);
            
            System.out.println(i);
    
            nodes1 = wnx.toArray(nodes1);
            for(int k=0;k<nodes1.length;k++)
            //while(itx.hasNext())
            {
                if(nodes1[k]==null)
                    break;
                
                WayNode nx = nodes1[k];//(WayNode) itx.next();
                
                    
                    for(int j=0;j<ways.size();j++)
                    {
                        if(i==j)
                            continue;
                        
                        Way y = ways.get(j);
                        List<WayNode> wny = y.getWayNodes();
                        
                        //Iterator itx = wnx.iterator();
                        //Iterator ity = wny.iterator();
                        
                        
                        
                        
                    //lid = nx.getNodeId();
                    
                    //ity = wny.iterator();
                    nodes2 = wny.toArray(nodes2);
                    for(int l=0;l<nodes2.length;l++)
                    //while(ity.hasNext())
                    {
                        if(nodes2[l]==null)
                            break;
                        WayNode ny = nodes2[l];//(WayNode) ity.next();
                        
                        
                        if(x != y  && inter.get(new Long(nx.getNodeId()))==null && inter.get(new Long(ny.getNodeId()))!=null &&  ( NWay.distance(map.get( new Long(nx.getNodeId())),map.get( new Long(ny.getNodeId()))) < 0.002))
                        {
                            
                            //nx.setNodeId(ny.getNodeId());
                            
                            map.put(new Long(nx.getNodeId()),  map.get( new Long(ny.getNodeId()))   );
                            inter.put(new Long(nx.getNodeId()),  inter.get( new Long(ny.getNodeId()))   );
                            
                            Intersection ii = inter.get(new Long(nx.getNodeId()));
                            ii.addWay(x);
                            currentnway.addIntersectionMid(ii,k);
                            
                            
                            
                            //System.out.println("found");
                            found++;
                            //System.out.println("same "+ x +" "+y);
                            
                        }
                    }
                }
            }
        }*/
        
        System.out.println("found: "+found);

        
        
        
        ///adding first and last nodes for each way if the way did not have an intersection on those nodes
        for(int i=0;i<ways.size();i++)
        {
            Way x = ways.get(i);
            List<WayNode> wnx = x.getWayNodes();
            
            NWay currentnway = newways.get(i);//new NWay(x)
            
            WayNode nx = wnx.get(0);
            Node c = map.get( new Long(nx.getNodeId()));
            
            if(inter.get(new Long(nx.getNodeId()))==null)
            {
                inter.put(new Long(nx.getNodeId()),new Intersection(c.getLatitude(),c.getLongitude(),x,nx.getNodeId(),i));
                
                Intersection sect=inter.get(new Long(nx.getNodeId()));
                //System.out.println(sect);
                currentnway.addIntersectionFirst(sect);
            }
            
            nx = wnx.get(wnx.size()-1);
            c = map.get( new Long(nx.getNodeId()));
            
            if(inter.get(new Long(nx.getNodeId()))==null)
            {
                inter.put(new Long(nx.getNodeId()),new Intersection(c.getLatitude(),c.getLongitude(),x,nx.getNodeId(),i));
                Intersection sect=inter.get(new Long(nx.getNodeId()));
                //System.out.println(sect);
                currentnway.addIntersectionLast(sect,i);
            }       
            
            
            /*if(currentnway.waydata.size()>1 & currentnway.waydata.get(0).inter.nodeid==currentnway.waydata.get(currentnway.waydata.size()-1).inter.nodeid)
                System.out.println("circle!");*/
            
        }


        Iterator it = inter.values().iterator();
        
        System.out.println("Done creating nways - first last");
        
        /*while(it.hasNext())
        {
            System.out.println(it.next());
        }

        System.out.println(inter.size());*/
        
        
        HashMap<Long,NNode> newnodes = new HashMap<Long,NNode>();
        
        for(int i=0;i<newways.size();i++)
        {
            Vector<StructNWayNode> waydata = newways.get(i).waydata;
            
            
            
            for(int j=0;j<waydata.size();j++)
            {
                StructNWayNode n = waydata.get(j);
                
                if(n.inter!=null)
                {
                    if(newnodes.get(new Long(n.inter.nodeid))==null)
                    {
                        newnodes.put(new Long(n.inter.nodeid), new NNode(inter.get(new Long(n.inter.nodeid))));
                    }
                    inter.get(new Long(n.inter.nodeid)).setNNode(newnodes.get(new Long(n.inter.nodeid)));
                    
                    //System.out.println(inter.get(new Long(n.inter.nodeid)).newnode);
                }
                else
                {
                    
                }
            }
            
            for(int j=0;j<waydata.size();j++)
            {
                StructNWayNode n = waydata.get(j);
                
                if(n.inter!=null)
                {
                    NNode cnode = newnodes.get(new Long(n.inter.nodeid));
                    //System.out.println(cnode);
                    
                    if(!(j-2<0))
                    {
                        StructNWayNode l1 = waydata.get(j-1);
                        cnode.conns[cnode.numcon] = waydata.get(j-2).inter.newnode;
                        cnode.lengths[cnode.numcon]= l1.length;
                        cnode.numcon++;
                    }
                    
                    
                    if(!(j+2 >= waydata.size()))
                    {
                        StructNWayNode l2 = waydata.get(j+1);
                        cnode.conns[cnode.numcon] = waydata.get(j+2).inter.newnode;
                        cnode.lengths[cnode.numcon]=l2.length;
                        cnode.numcon++;
                    }
                }
                else
                {
                    
                }
            }
            
        }


        //System.out.println(c);

        it = newnodes.values().iterator();
        
        //System.out.println("Done creating nways");
        
        while(it.hasNext())
        {
            //System.out.println(it.next());
            ((NNode)it.next()).removeDuplicates();
        }
        
        /*
        it = newnodes.values().iterator();
        
        //System.out.println("Done creating nways");
        
        while(it.hasNext())
        {
            //System.out.println(it.next());
            System.out.println(((NNode)it.next()).numcon);
        }        */
         /*System.out.println(inter.size());*/        
        
        try
        {
            FileWriter fstream = new FileWriter("nodes.txt");
            BufferedWriter out = new BufferedWriter(fstream);
            it = newnodes.values().iterator();
            
            //System.out.println("Done creating nways");
            
            out.write(newnodes.size()+"\n");
            while(it.hasNext())
            {
                //System.out.println(it.next());
               out.write(((NNode)it.next())+"\n");
            }   
            out.close();
            
        }
        catch(Exception E)
        {
            
        }

        
    }

    static class Intersection
    {
        NNode newnode;
        double l1,l2;
        Vector<Way> ways = new Vector<Way>();
        Vector<String> names = new Vector<String>();
        long nodeid;
        int value=0;
        public Intersection(double l, double ll, Way wi, Way wii, long nodeid_in, int wiid, int wiiid)
        {
            l1=l;
            l2=ll;
            //ways.add(wi);
            //ways.add(wii);
            addWay(wi,wiid);
            addWay(wii,wiiid);
            nodeid=nodeid_in;
            //w1 = wi;
            //w2 = wii;
        }
        public Intersection(double l, double ll, Way wi,  long nodeid_in, int wiid)
        {
            l1=l;
            l2=ll;
            addWay(wi,wiid);
            //ways.add(wi);
            nodeid=nodeid_in;
            //w1 = wi;
            //w2 = wii;
        }
        
        public void addWay(Way x, int id)
        {
            if(!ways.contains(x))
            {
                ways.add(x);
                
                Collection<Tag> ls = x.getTags();
                Tag[] tags = (Tag[]) ls.toArray(new Tag[0]);
                for(int i=0;i<ls.size();i++)
                {
                    if(tags[i].getKey().equals("highway"))
                    {
                        
                        if(tags[i].getValue().equals("motorway"))
                        {
                            value+=25;
                        }
                        else if(tags[i].getValue().equals("motorway_link"))
                        {
                            value+=25;
                        }
                        else if(tags[i].getValue().equals("trunk"))
                        {
                            value+=20;
                        }
                        else if(tags[i].getValue().equals("trunk_link"))
                        {
                            value+=20;
                        }
                        else if(tags[i].getValue().equals("primary"))
                        {
                            value+=15;
                        }
                        else if(tags[i].getValue().equals("primary_link"))
                        {
                            value+=15;
                        }
                        else if(tags[i].getValue().equals("secondary"))
                        {
                            value+=12;
                        }
                        else if(tags[i].getValue().equals("secondary_link"))
                        {
                            value+=12;
                        }
                        else if(tags[i].getValue().equals("tertiary"))
                        {
                            value+=10;
                        }
                        else if(tags[i].getValue().equals("tertiary_link"))
                        {
                            value+=10;
                        }
                        else if(tags[i].getValue().equals("residential"))
                        {
                            value+=5;
                        }
                        else if(tags[i].getValue().equals("unclassified"))
                        {
                            value+=5;
                        }
                        else
                        {
                            System.out.println(tags[i].getValue());
                        }
                    }	
                    if(tags[i].getKey().equals("name"))
                    {
                        String nametemp = tags[i].getValue();
                        
                        for(int k=0;k<ls.size();k++)
                        {
                            if(tags[k].getKey().equals("oneway"))
                            {
                                nametemp+="__"+tags[k].getValue();
                            }
                        }
                        
                        
                        if(!names.contains(nametemp))
                        {
                            names.add(""+nametemp+id);
                        }
                    }
                }
            }
            
        }

        public String toString()
        {
            return ""+l1+"\t "+l2 + "\t"+value;
        }
        
        public void setNNode(NNode x)
        {
            newnode = x;
        }
    }

    
    static class StructNWayNode
    {
        double length;
        Intersection inter;
        int place;
        
        public StructNWayNode(double l, Intersection inter, int place_in)
        {
            length = l;
            this.inter=inter;
            place = place_in;
        }
        
    }

    static class NWay
    {
        Way way;

        Vector<StructNWayNode> waydata = new Vector<StructNWayNode>();

        public NWay(Way w)
        {
            way = w;
        }

        public void addIntersection(Intersection x, int place)
        {
            
            if(waydata.size()==0)
            {
                waydata.add( new StructNWayNode(0,x,place));
            }
            else
            {
                //add length then....
                double length = getDistance(waydata.get(waydata.size()-1).inter.nodeid,x.nodeid, way);
                
                waydata.add( new StructNWayNode(length,null,-1));
                
                waydata.add( new StructNWayNode(0,x,place));
            }
        }
        
        public void addIntersectionMid(Intersection x, int place)
        {
            if(waydata.size()==0)
            {
                waydata.add( new StructNWayNode(0,x,place));
            }
            else
            {
                //add length then....
                
                int i;
                for(i=0;i<waydata.size();i++)
                {
                    StructNWayNode temp = waydata.get(i);
                    
                    if(temp.inter!=null && temp.place < place)
                        break;
                }
 

                if(i==waydata.size())
                {
                    double length = getDistance(waydata.get(waydata.size()-1).inter.nodeid,x.nodeid, way);
                    
                    waydata.add( new StructNWayNode(length,null,-1));
                    
                    waydata.add( new StructNWayNode(0,x,place));
                    return;
                }
                
                
                
                waydata.add(i, new StructNWayNode(0,x,place));
                
                if(i>0)
                {
                    double lengthpre = getDistance(waydata.get(i-2).inter.nodeid,x.nodeid, way);
                    waydata.get(i-1).length = lengthpre;
                }
                
                if(i<waydata.size())
                {
                    double lengthpost = getDistance(waydata.get(i+1).inter.nodeid,x.nodeid, way);
                    waydata.add(i+1, new StructNWayNode(lengthpost,null,-1));
                }
                
                
                /*double length1 = getDistance(waydata.get(waydata.size()-1).inter.nodeid,x.nodeid, way);
                
                waydata.add( new StructNWayNode(length,null));
                
                waydata.add( new StructNWayNode(0,x));*/
            }            
        }
        
        public void addIntersectionFirst(Intersection x)
        {
            
            if(waydata.size()==0)
            {
                waydata.add( new StructNWayNode(0,x,0));
            }
            else
            {
                //add length then....
                double length = getDistance(waydata.get(0).inter.nodeid,x.nodeid, way);
                
                waydata.add(0, new StructNWayNode(length,null,0));
                
                waydata.add(0, new StructNWayNode(0,x,-1));
            }
        }
        
        public void addIntersectionLast(Intersection x, int place)
        {
            {
                //add length then....
                double length = getDistance(waydata.get(waydata.size()-1).inter.nodeid,x.nodeid, way);
                
                waydata.add( new StructNWayNode(length,null,-1));
                
                waydata.add( new StructNWayNode(0,x,place));
            }
        }
        
        
                
        /*public void addBeginEndIntersections()
        {
            
        }*/

        //http://www.movable-type.co.uk/scripts/latlong.html
        public static double distance(Node n1, Node n2)
        {
            double R = 6371; // km
            double dLat = Math.toRadians((n1.getLatitude()-n2.getLatitude()));
            double dLon = Math.toRadians((n1.getLongitude()-n2.getLongitude()));
            double lat1 = Math.toRadians(n1.getLatitude());
            double lat2 = Math.toRadians(n2.getLatitude());
            
            double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            double d = R * c;
            
            
            //System.out.println(d);
            return d;
        }
        
        public double getDistance(long id1, long id2, Way w )
        {
            int i1=-1,i2=-1;
            double length=0;
            
            //Way x = ways.get(i);
            List<WayNode> wnx = w.getWayNodes();
            WayNode[] nodes = new WayNode[wnx.size()];
            nodes = wnx.toArray(nodes);
            
            
            for(int i=0;i<nodes.length;i++)
            {
                if(nodes[i].getNodeId()==id1 || nodes[i].getNodeId()==id2)
                {
                    if(i1==-1)
                        i1=i;
                    else
                        i2=i;
                }
            }
            //System.out.println(i1+" "+i2);

            
            for(int i=i1;i<i2;i++)
            {
                length+=distance(map.get( new Long(nodes[i].getNodeId())), map.get( new Long(nodes[i+1].getNodeId())));
            }
            
            
            
            return length;
        }
        
        
    }


    static class NNode
    {
        static long IDCOUNT=0;

        Intersection inter;
        double log,lat;
        long id=IDCOUNT++;
        NNode[] conns = new NNode[400];
        int numcon;
        double [] lengths = new double[400];
        //NWay[] ways = new NWay[2];
        
        public String toString()
        {
            String toReturn =""+id+" "+inter+"\n"+numcon;
            for(int i=0;i<numcon;i++)
            {
                toReturn+="\n"+conns[i].id+" "+lengths[i];
            }
            
            toReturn+="\n"+inter.names.size();
            for(int i=0;i<inter.names.size();i++)
            {
                toReturn+="\n"+inter.names.get(i).replace(' ','_');
            }
            
            return toReturn;
        }
        
        public NNode(Intersection i)
        {
            inter=i;
        }
        
        public void compareNodes()
        {
            for(int i=0;i<numcon;i++)
                for(int j=0;j<numcon;j++)
                {
                    if(i==j)
                        continue;
                    
                    if(conns[i]==conns[j])
                        System.out.println("duplicate "+i+" "+j);
                }
        }
        
        public void removeDuplicates()
        {
            
            //remove self
            for(int i=0;i<numcon;i++)
            {
                System.out.println("START");
                for(int k=0;k<numcon;k++)
                {
                    System.out.println(k+" "+conns[k].id);
                }
                System.out.println("BEGIN");
                
                if(conns[i].id==id)
                {
                    //System.out.println("dup");
                }
                
                if(conns[i].id==id)
                {
                    /*for(int k=i;k<numcon-1;k++)
                    {
                        conns[i]=conns[i+1];
                        lengths[i]=lengths[i+1];
                    }*/
                    
                    conns[i]=conns[numcon-1];
                    lengths[i]=lengths[numcon-1];
                    
                    i--;
                    numcon--;
                }
                for(int k=0;k<numcon;k++)
                {
                    System.out.println(k+" "+conns[k].id);
                }
                System.out.println("END");
            }
            
            
            /*for(int i=0;i<numcon;i++)
            {
                for(int j=i+1;j<numcon;j++)
                {
                    if(conns[i]==conns[j])
                    {
                        //System.out.println("duplicate "+i+" "+j);
                        for(int k=j;k<numcon-1;k++)
                        {
                            //if((!=j)
                            {
                                conns[k]=conns[k+1];
                                lengths[k]=lengths[k+1];
                            }
                            
                        }
                        numcon--;
                        j=0;
                        i=1;
                    }
                }   
            }*/
        }
    }
}
