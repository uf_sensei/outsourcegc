#!/bin/bash

set -e

LIB_DIR=/home/terabyte/local/lib
SERVER_IP_ADDR=69.30.63.212

if [ -z $LD_LIBRARY_PATH ]
then
  export LD_LIBRARY_PATH=$LIB_DIR
else
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIB_DIR
fi


# Circuit files
CIRCUIT_FAST_AES=./circuits/fast-aes.circuit.bin.hash-free
CIRCUIT_RSA_256=./circuits/rsa.256.bin.hash-free

function time_execution {
    echo "TIMING: $*"
    /usr/bin/time --format="\n-- BENCH RESULTS --\nreal_time:   %e s\nuser_time:   %U s\nsystem_time: %S s\nmax_mem:     %M Kb\navg_mem:     %t Kb\nFS_reads:    %I\nFS_writes:   %O\nsock_recvs:  %r\nsock_sends:  %s\n" $@
}


#time_execution mpirun -n 1 ./evl 80 256 $CIRCUIT_FAST_AES inp.txt $SERVER_IP_ADDR 5000 1
mpirun -n 1 ./evl 80 256 $CIRCUIT_FAST_AES inp.txt $SERVER_IP_ADDR 5000 1
mpirun -n 2 ./evl 80 256 $CIRCUIT_FAST_AES inp.txt $SERVER_IP_ADDR 5000 1
# mpirun -n 4 ./evl 80 256 $CIRCUIT_FAST_AES inp.txt $SERVER_IP_ADDR 5000 1
# mpirun -n 1 ./evl 80 256 $CIRCUIT_FAST_AES inp.txt $SERVER_IP_ADDR 5000 2
# mpirun -n 2 ./evl 80 256 $CIRCUIT_FAST_AES inp.txt $SERVER_IP_ADDR 5000 2
# mpirun -n 4 ./evl 80 256 $CIRCUIT_FAST_AES inp.txt $SERVER_IP_ADDR 5000 2
mpirun -n 1 ./evl 80 1   $CIRCUIT_RSA_256  inp.txt $SERVER_IP_ADDR 5000 0
